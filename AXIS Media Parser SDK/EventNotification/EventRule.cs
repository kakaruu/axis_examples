﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EventNotification
{
	/// <summary>
	/// Simple event rule
	/// </summary>
	internal class EventRule
	{
		private string _name;
		private string _eventTopic;
		private Condition _sourceCondition;
		private Condition _dataCondition;

		public EventRule(string name, string eventTopic,
			Condition sourceCondition, Condition dataCondition)
		{
			this._name = name;
			this._eventTopic = eventTopic;
			this._sourceCondition = sourceCondition;
			this._dataCondition = dataCondition;
		}

		public bool Match(SimpleEvent simpleEvent)
		{
			if (simpleEvent.PropertyOperation != "Initialized") // do not match initial state
			{
				if (simpleEvent.Topic == _eventTopic)
				{
					if (_sourceCondition == null || _sourceCondition.MatchAny(simpleEvent.Source))
					{
						if (_dataCondition == null || _dataCondition.MatchAny(simpleEvent.Data))
						{
							return true;
						}
					}
				}
			}

			return false;
		}

		public override string ToString()
		{
			return _name;
		}
	}

	internal class Condition
	{
		private string _name;
		private string _value;

		public Condition(string name, string value)
		{
			this._name = name;
			this._value = value;
		}

		public bool MatchAny(SimpleItemList itemHolder)
		{
			if (itemHolder != null)
			{
				foreach (SimpleItem simpleItem in itemHolder.Items)
				{
					if (simpleItem.Name == _name && simpleItem.Value == _value)
					{
						return true;
					}
				}
			}

			return false;
		}
	}
}
