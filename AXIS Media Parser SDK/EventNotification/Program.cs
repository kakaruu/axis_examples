using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Xml.Linq;
using System.Xml.Serialization;
using AxisMediaParserLib;

namespace EventNotification
{
	/// <summary>
	/// This sample program shows how to use the AXIS Media Parser component to subscribe to 
	/// the metadata stream of an Axis device to get notifications about different device events.
	/// </summary>
	class Program
	{
		// Base URL - change IP and credentials
		private const string BaseUrl = "axrtsp://10.85.11.8/axis-media/media.amp?video=0&audio=0"; 
		private const string UserName = "root";
		private const string Password = "pass";

		// Get all events
		string Url = BaseUrl + "&event=on";

		// Filter on topic (VMD 2)
		//string Url = BaseUrl + "&event=on&eventtopic=onvif:RuleEngine/axis:VideoMotionDetection//.";

		// Filter on event content (window=0)
		//string Url = BaseUrl + "&event=on&eventcontent=boolean(//SimpleItem[@Name=%22window%22and@Value=%220%22])";



		/// <summary>
		/// Set of rule on what this application should detect
		/// </summary>
		private IList<EventRule> eventRules =
			new List<EventRule>()
		{
			new EventRule(
				"Motion Detection",
				"tns1:VideoAnalytics/tnsaxis:MotionDetection",
				null,
				new Condition("motion", "1")),
				
			new EventRule(
				"Motion Detection (AXIS VMD 2 application)",
				"tns1:RuleEngine/tnsaxis:VideoMotionDetection/motion",
				null,
				new Condition("active", "1")),
				
			new EventRule(
				"Motion Detection on video channel 1 (AXIS VMD 3 application)",
				"tns1:RuleEngine/tnsaxis:VMD3/vmd3_video_1",
				null,
				new Condition("active", "1")),
				
			new EventRule(
				"Audio Detection",
				"tns1:AudioSource/tnsaxis:TriggerLevel",
				null,
				new Condition("triggered", "1")),
				
			new EventRule(
				"Tampering Detection on video channel 1",
				"tns1:VideoSource/tnsaxis:Tampering",
				new Condition("channel", "1"),
				new Condition("tampering", "1")),
				
			new EventRule(
				"SD-card Disruption (SD-card ejected)",
				"tnsaxis:Storage/Disruption",
				new Condition("disk_id", "SD_DISK"),
				new Condition("disruption", "1"))
		};

		// Event to signal main thread to abort parsing process
		private AutoResetEvent abortEvent = new AutoResetEvent(false);

		static void Main(string[] args)
		{
			new Program().Run();
		}


		private void Run()
		{
			Console.CancelKeyPress += new ConsoleCancelEventHandler(Console_CancelKeyPress);

			// Create the AXIS Media Parser COM object
			AxisMediaParser parser = new AxisMediaParser();
			try
			{
				// Register for events
				parser.OnError += OnError;
				parser.OnMetaDataSample += OnMetaDataSample;

				// Set connection and media properties
				parser.MediaURL = Url;
				parser.MediaUsername = UserName;
				parser.MediaPassword = Password;
				parser.NetworkTimeout = 120000;

				// Get absolute time from Axis device
				parser.TimeFormat = "ntp";

				// Connect to the device
				Console.WriteLine("Connecting to {0}", parser.MediaURL);
				int cookieID;
				int numberOfStreams;
				object buffer;
				parser.Connect(out cookieID, out numberOfStreams, out buffer);

				// Start the media stream and registered event handlers will be called
				parser.Start();

				Console.WriteLine("Capturing... (Press Ctrl + C to abort)\n");

				// Wait until abort is called
				abortEvent.WaitOne();

				// Stop the stream
				parser.Stop();

				// Unregister event handlers
				parser.OnError -= OnError;
				parser.OnMetaDataSample -= OnMetaDataSample;

			}
			catch (COMException e)
			{
				Console.WriteLine("Exception for {0}, {1}", parser.MediaURL, e.Message);
			}

			// Inform the GC that COM object no longer will be used
			Marshal.FinalReleaseComObject(parser);
			parser = null;
			Console.WriteLine("Stream stopped");
		}

		void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
		{
			Console.WriteLine("Canceled");
			abortEvent.Set();
			e.Cancel = true;
		}

		// Event handler callback from AxisMediaParser in case of errors during stream parsing
		private void OnError(int errorCode)
		{
			if (errorCode == (int)AMP_ERROR.AMP_E_END_OF_STREAM)
			{
				Console.WriteLine("End of stream");
				abortEvent.Set();
			}
			else
			{
				AMP_ERROR ampError = (AMP_ERROR)errorCode;
				Console.WriteLine("OnError - Code 0x{0:X} ({1})", errorCode, ampError.ToString());
				Console.ReadKey();
			}
		}

		// Event handler callback for Metadata
		void OnMetaDataSample(int CookieID, int sampleType, int sampleFlags,
			ulong startTime, ulong stopTime, string MetaData)
		{
			XNamespace wsntNamespace = "http://docs.oasis-open.org/wsn/b-2";
			XNamespace onvifNamespace = "http://www.onvif.org/ver10/schema";
			XElement root = XElement.Load(new StringReader(MetaData));

			// Find notifications
			IEnumerable<XElement> notifications =
				from el in root.Descendants(wsntNamespace + "NotificationMessage")
				select el;

			foreach (XElement notification in notifications)
			{
				string topic = notification.Descendants(wsntNamespace + "Topic").First().Value;
				XElement message = notification.Descendants(onvifNamespace + "Message").First();

				XmlSerializer serializer = new XmlSerializer(typeof(SimpleEvent));
				SimpleEvent simpleEvent = (SimpleEvent)serializer.Deserialize(message.CreateReader());
				simpleEvent.Topic = topic;

				// Find matching rules
				foreach (EventRule rule in this.eventRules)
				{
					if (rule.Match(simpleEvent))
					{
						Console.WriteLine("{0} --------------------------------------", simpleEvent.Timestamp);
						Console.WriteLine("RULE TRIGGER: {0}\nEVENT: {1}\n", rule, simpleEvent);
					}
				}
			}
		}
	}
}