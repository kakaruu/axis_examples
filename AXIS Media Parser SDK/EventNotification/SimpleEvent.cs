﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Xml.Serialization;


namespace EventNotification
{
	/// <summary>
	/// Represent the <tt:Message> element of the ONVIF metadata stream
	/// </summary>
	[XmlRootAttribute(ElementName = "Message", Namespace = "http://www.onvif.org/ver10/schema")]
	public class SimpleEvent
	{
		[XmlElementAttribute("Source")]
		public SimpleItemList Source { get; set; }

		[XmlElementAttribute("Key")]
		public SimpleItemList Key { get; set; }

		[XmlElementAttribute("Data")]
		public SimpleItemList Data { get; set; }

		[XmlIgnore()]
		public string Topic { get; set; }

		[XmlAttribute("UtcTime")]
		public DateTime Timestamp { get; set; }

		[XmlAttribute("PropertyOperation")]
		public string PropertyOperation { get; set; }

		public override string ToString()
		{
			// remove namespace prefix from topic tree
			string friendlyTopic =  Regex.Replace(Topic, @"[^/:]+:", String.Empty);
			string text = friendlyTopic + "\n";

			if (this.Source != null)
			{
				foreach (SimpleItem item in this.Source.Items)
				{
					text += item + "; ";
				}
			}

			if (this.Key != null)
			{
				foreach (SimpleItem item in this.Key.Items)
				{
					text += item + "; ";
				}
			}

			if (this.Data != null)
			{
				foreach (SimpleItem item in this.Data.Items)
				{
					text += item + "; ";
				}
			}

			return text;
		}
	}

	public class SimpleItemList
	{
		[XmlElementAttribute("SimpleItem", typeof(SimpleItem))]
		public List<SimpleItem> Items { get; set; }
	}

	public class SimpleItem
	{
		[XmlAttribute("Name")]
		public string Name { get; set; }

		[XmlAttribute("Value")]
		public string Value { get; set; }

		public override string ToString()
		{
			if (Value.Length > 20)
			{
				return Name + "=" + Value.Substring(0, 19) + "...";
			}
			else
			{
				return Name + "=" + Value;
			}
		}
	}
}
