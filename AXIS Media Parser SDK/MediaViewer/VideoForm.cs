using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;

using AxisMediaViewerLib;
using AxisMediaParserLib;
using System.Threading.Tasks;

namespace MediaViewer
{
	/// <summary>
	/// This sample shows how to use the AXIS Media Viewer component, where a thread reads
	/// media frames from a file (e.g. created in the MediaParser sample) and lets the viewer component
	/// render them. Please see the documentation for possible values and error codes.
	/// </summary>
	class VideoForm : Form
	{
		// Delegate to notify end-of-stream from the background render thread.
		private delegate void EndOfStreamDelegate();

		// NTP time-stamps start from this date
		private readonly DateTime NtpTimeEpoch = new DateTime(1900, 1, 1);

		enum State
		{
			Stopped,
			Paused,
			Playing
		}

        private AxisMediaParser parser;
        private AxisMediaViewer viewer;

        private long m_videoViewHandle;

		// Location of recording (.bin) generated with MediaParser sample.
		// Sample file included with SDK copied to target directory during post-build event.

		private State state = State.Stopped;
		private decimal playbackRate = 1;
		private long lastCurrentPosition = -1;

        private object seekingLock = new object();

        private Button fwdStepButton;
		private Button backStepButton;
		private TextBox currentTimeTextBox;
		private Button stopButton;
		private Button pauseButton;
		private Button playButton;
		private TrackBar trackBar;
		private ProgressBar progressBar;
		private NumericUpDown playbackRateControl;
		private Label playbackRateLabel;
		private CheckBox videoCheckBox;
		private CheckBox audioCheckBox;
		private Panel videoPanel;
		private Label timeLabel;


		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new VideoForm());
		}


		public VideoForm()
		{
			InitializeComponent();

            m_videoViewHandle = this.videoPanel.Handle.ToInt64();

            this.CreateViewer();

			videoCheckBox.Enabled = true;
			audioCheckBox.Enabled = true;
		}

		private void CreateViewer()
		{
            Task.Factory.StartNew(() =>
            {
                // Create the AXIS Media Viewer COM object in a MTA apartment to allow multiple
                // threads (UI and render thread) to access COM object (by default tasks use MTA threads).
                parser = new AxisMediaParser();
                viewer = new AxisMediaViewer();

                viewer.VideoRenderer = (int)AMV_RENDER_OPTIONS.AMV_VIDEO_RENDERER_EVR;
                viewer.EnableOnDecodedImage = false;
                viewer.PlayOptions = (int)AMV_PLAY_OPTIONS.AMV_PO_VIDEO;
                viewer.LiveMode = false;
                viewer.PlayOptions = GetPlayOptions();
                viewer.PlaybackRate = (double)playbackRate;

                parser.OnVideoSample += OnVideoSample;
                parser.OnAudioSample += OnAudioSample;

                parser.ShowLoginDialog = true;
                // 1�� ��ȭ
                parser.MediaURL = "axrtsphttp://192.168.0.247/axis-media/media.amp?recordingid=20190729_111100_3A5E_ACCC8EBE5348";
                // 3�� ��ȭ
                //parser.MediaURL = "axrtsphttp://192.168.0.247/axis-media/media.amp?recordingid=20190729_105000_BE24_ACCC8EBE5348";
                //parser.MediaURL = "axrtsphttp://192.168.0.247/axis-media/media.amp?videocodec=h264&events=on&eventtopic=//.";
                parser.MediaUsername = "root";
                parser.MediaPassword = "!Q2w3e4r5t6y";

                parser.StartPosition = 6000;

                viewer.OnMediaPosition +=
                        new IAxisMediaViewerEvents_OnMediaPositionEventHandler(OnMediaPosition);
            }).Wait();
        }

		private void DestroyViewer()
		{
			if (viewer != null)
			{
				if (state != State.Stopped)
				{
					Stop();
				}

				viewer.OnMediaPosition -=
					new IAxisMediaViewerEvents_OnMediaPositionEventHandler(OnMediaPosition);


				// Inform the GC that COM object no longer will be used
				Marshal.FinalReleaseComObject(viewer);
				viewer = null;
			}
		}

		private void Play()
		{
            if(this.state == State.Stopped)
            {
                Task.Factory.StartNew(() =>
                {
                    int cookieID;
                    int numberOfStreams;
                    object mediaTypeBuffer;
                    parser.Connect(out cookieID, out numberOfStreams, out mediaTypeBuffer);

                    // Init the Viewer with media type buffer and a win32 hWnd handle to the window. 
                    viewer.Init(numberOfStreams, mediaTypeBuffer, m_videoViewHandle);
                }).Wait();
            }

            if (parser.Status == (int)AxisMediaParserLib.AMP_STATUS.AMP_STATUS_IDLE)
            {
                Task.Factory.StartNew(() =>
                {
                    parser.Start();
                }).Wait();
            }

            if (viewer.PlaybackRate != (double)playbackRate)
            {
                Seek(lastCurrentPosition, (double)playbackRate, true);
            }
            Task.Factory.StartNew(() =>
            {
                Thread.Sleep(1000);
                viewer.Start();
            });

            SetState(State.Playing);
        }

        private void Pause()
        {
            if (this.state == State.Playing)
            {
                Task.Factory.StartNew(() =>
                {
                    viewer.Pause();
                }).Wait();
                SetState(State.Paused);
            }
        }

        private void Stop()
		{
			viewer.Stop();
            parser.Disconnect();

            SetState(State.Stopped);
		}

        private void Seek(long mediaPosition, double playbackRate, bool seekToSyncPoint)
        {
            viewer.Stop();

            lock (seekingLock)
            {
                Task.Factory.StartNew(() =>
                {
                    parser.Disconnect();
                    parser.StartPosition = mediaPosition;
                    int cookieID;
                    int numberOfStreams;
                    object mediaTypeBuffer;
                    parser.Connect(out cookieID, out numberOfStreams, out mediaTypeBuffer);

                    // Init the Viewer with media type buffer and a win32 hWnd handle to the window. 
                    viewer.Init(numberOfStreams, mediaTypeBuffer, m_videoViewHandle);

                    parser.Start();

                    if (!seekToSyncPoint)
                    {
                        // make viewer preroll all samples before this position
                        //viewer.SetStartTime((long)mediaPosition);
                    }

                    if (playbackRate != viewer.PlaybackRate)
                    {
                        viewer.PlaybackRate = playbackRate;
                    }

                    if (state == State.Paused)
                    {
                        viewer.Pause();
                    }
                    else
                    {
                        viewer.Start();
                    }
                });
            }
        }

        private void SetState(State state)
        {
            this.state = state;

            switch (state)
            {
                case State.Stopped:
                playButton.Enabled = true;
                pauseButton.Enabled = true;
                stopButton.Enabled = false;
                trackBar.Value = 0;
                break;
                case State.Paused:
                playButton.Enabled = true;
                pauseButton.Enabled = false;
                stopButton.Enabled = true;
                break;
                case State.Playing:
                playButton.Enabled = false;
                pauseButton.Enabled = true;
                stopButton.Enabled = true;
                break;
            }
        }

        private int GetPlayOptions()
        {
            int playOptions = 0;

            if (videoCheckBox.Checked)
            {
                playOptions |= (int)AMV_PLAY_OPTIONS.AMV_PO_VIDEO;
            }

            if (audioCheckBox.Checked)
            {
                playOptions |= (int)AMV_PLAY_OPTIONS.AMV_PO_AUDIO;
            }

            // Enable metadata overlay
            // playOptions |= (int)AMV_PLAY_OPTIONS.AMV_PO_META;

            return playOptions;
        }

        void OnMediaPosition(long currentPosition)
        {
            if (InvokeRequired)
            {
                // If called from a non UI thread, let the UI thread perform the call 
                BeginInvoke(new IAxisMediaViewerEvents_OnMediaPositionEventHandler(OnMediaPosition),
                    new object[] { currentPosition });
                return;
            }

            long duration = parser.Duration * 10000;

            lastCurrentPosition = currentPosition;

            // If current position represent absolute time we present the current date and time in the GUI
            DateTime absoluteTime = new DateTime(currentPosition + NtpTimeEpoch.Ticks);
            if (absoluteTime.Year >= 1970)
            {
                timeLabel.Text = absoluteTime.ToString("yyyy-MM-dd hh:mm:ss.ff UTC");
                timeLabel.Invalidate();
            }

            long relativePosition = currentPosition - parser.StartPosition * 10000;
            if (relativePosition < 0)
            {
                relativePosition = 0;
            }
            else if (relativePosition > duration)
            {
                relativePosition = duration;
            }

            // Update progress bar
            if (duration > 0)
            {
                int scale = (progressBar.Maximum - progressBar.Minimum);
                progressBar.Value = (int)(((relativePosition * scale) / duration));
            }
            else
            {
                progressBar.Value = 0;
            }

            // Update time text box
            TimeSpan currentTime = new TimeSpan(relativePosition); // ms -> 100-nanosecond
            TimeSpan currentDuration = new TimeSpan(duration); // ms -> 100-nanosecond

            string timeFormat = (currentTime.Days > 0 || currentDuration.Days > 0) ?
                "({6}) {0:D2}:{1:D2}:{2:D2} / ({7}) {3:D2}:{4:D2}:{5:D2}" :
                "{0:D2}:{1:D2}:{2:D2}:{8:D3} / {3:D2}:{4:D2}:{5:D2}:{9:D3}";

            string timeinfo = String.Format(timeFormat,
                currentTime.Hours, currentTime.Minutes, currentTime.Seconds,
                currentDuration.Hours, currentDuration.Minutes, currentDuration.Seconds,
                currentTime.Days, currentDuration.Days,
                currentTime.Milliseconds, currentDuration.Milliseconds);

            currentTimeTextBox.Text = timeinfo;
        }

        private void OnVideoSample(int cookieID, int sampleType,
        int sampleFlags, ulong startTime, ulong stopTime, object SampleArray)
        {
            // Let viewer render video sample

            // Add LiveTimeOffset to original timestamp for optimal latency when rendering
            long renderStartTime = 0;
            long renderStopTime = 1;
            if ((long)startTime + parser.LiveTimeOffset > 0)
            {
                renderStartTime = (long)startTime + parser.LiveTimeOffset;
                renderStopTime = (long)stopTime + parser.LiveTimeOffset;
            }

            viewer.RenderVideoSample(sampleFlags, (ulong)renderStartTime, (ulong)renderStopTime, SampleArray);
        }

        private void OnAudioSample(int cookieID, int sampleType,
        int sampleFlags, ulong startTime, ulong stopTime, object SampleArray)
        {
            // Let viewer render audio sample

            // Add LiveTimeOffset to original timestamp for optimal latency when rendering
            long renderStartTime = 0;
            long renderStopTime = 1;
            if ((long)startTime + parser.LiveTimeOffset > 0)
            {
                renderStartTime = (long)startTime + parser.LiveTimeOffset;
                renderStopTime = (long)stopTime + parser.LiveTimeOffset;
            }

            viewer.RenderAudioSample(sampleFlags, (ulong)renderStartTime, (ulong)renderStopTime, SampleArray);
        }

		private void OnEndOfStream()
		{
			if (state != State.Stopped)
			{
				Stop();
			}
		}

		/// <summary>
		/// This event handler is called after the dialog is shown, since we need the
		/// win32 window handle as a parameter to the Viewer
		/// </summary>
		private void VideoForm_Load(object sender, EventArgs e)
		{
			// Auto start
			Play();
		}

		private void VideoForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			DestroyViewer();
            //parser.Stop();
		}

		/// <summary>
		/// Event called when the dialog is resized, the viewer is informed about the new size
		/// </summary>
		private void VideoForm_Resize(object sender, EventArgs e)
		{
			if (viewer != null)
			{
				viewer.SetVideoPosition(0, 0, videoPanel.Width, videoPanel.Height);
			}
		}

		private void playButton_Click(object sender, EventArgs e)
		{
			Play();
		}

		private void stopButton_Click(object sender, EventArgs e)
		{
			Stop();
		}

		private void pauseButton_Click(object sender, EventArgs e)
		{
			Pause();
		}

		private void backStepButton_Click(object sender, EventArgs e)
		{
			if (state == State.Paused)
			{
				Seek(lastCurrentPosition - 1, (double)playbackRate, false);
			}
			else
			{
				Pause();
			}
		}

		private void fwdStepButton_Click(object sender, EventArgs e)
		{
			if (state == State.Stopped)
			{
				Pause();
			}
			else
			{
				viewer.FrameStep(1);
				SetState(State.Paused);
			}
		}

		private void trackBar_Scroll(object sender, EventArgs e)
		{
            if (parser.Duration <= 0)
			{
				return;
			}

			if (state == State.Stopped)
			{
				return;
			}

			double newPosition = trackBar.Value / (double)(trackBar.Maximum - trackBar.Minimum);
			long newMediaPosition = (long)(newPosition * (parser.Duration));

			// We only seek to video sync-point to improve performance
			Seek(newMediaPosition, (double)playbackRate, true);

			// Sleep to avoid too many seek events (better visual feedback)
			System.Threading.Thread.Sleep(20);
		}

		private void mediaTypeCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if (state != State.Stopped)
			{
				viewer.Stop();
			}

			// Reinitialize Viewer with updated play options			
			viewer.PlayOptions = GetPlayOptions();
			//viewer.Init(0, fileParser.MediaTypeBuffer, videoPanel.Handle.ToInt64());

			Play();
		}

		private void playbackRateControl_ValueChanged(object sender, EventArgs e)
		{
            // Enforce logarithmic scale
            decimal newRate;
            if (playbackRate < playbackRateControl.Value && playbackRateControl.Value <= 2 * playbackRate)
            {
                newRate = playbackRate * 2;
            }
            else if (playbackRate / 2 <= playbackRateControl.Value && playbackRateControl.Value < playbackRate)
            {
                newRate = playbackRate / 2;
            }
            else if (playbackRate != playbackRateControl.Value)
            {
                // user entered value manual - stick to power-two values
                int test = (int)Math.Log((double)playbackRateControl.Value, 2);
                newRate = (decimal)Math.Pow(2.0, (double)test);
            }
            else
            {
                return;
            }

            if (newRate < playbackRateControl.Minimum)
            {
                newRate = playbackRateControl.Minimum;
            }

            if (newRate > playbackRateControl.Maximum)
            {
                newRate = playbackRateControl.Maximum;
            }

            playbackRate = newRate;
            playbackRateControl.Value = playbackRate;
            playbackRateControl.DecimalPlaces = (playbackRateControl.Value < 1) ? 2 : 0;

            if (state != State.Stopped)
            {
                Seek(lastCurrentPosition + 1, (double)playbackRate, false);
            }
        }

		private void videoPanel_Paint(object sender, PaintEventArgs e)
		{
			if (viewer != null)
			{
				viewer.RepaintVideo();
			}
		}

		#region Windows Form Designer generated code

		// Required designer variable.
		private System.ComponentModel.IContainer components = null;

		// Clean up any resources being used.
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}


		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.fwdStepButton = new System.Windows.Forms.Button();
			this.backStepButton = new System.Windows.Forms.Button();
			this.currentTimeTextBox = new System.Windows.Forms.TextBox();
			this.stopButton = new System.Windows.Forms.Button();
			this.pauseButton = new System.Windows.Forms.Button();
			this.playButton = new System.Windows.Forms.Button();
			this.trackBar = new System.Windows.Forms.TrackBar();
			this.progressBar = new System.Windows.Forms.ProgressBar();
			this.playbackRateControl = new System.Windows.Forms.NumericUpDown();
			this.playbackRateLabel = new System.Windows.Forms.Label();
			this.videoCheckBox = new System.Windows.Forms.CheckBox();
			this.audioCheckBox = new System.Windows.Forms.CheckBox();
			this.videoPanel = new System.Windows.Forms.Panel();
			this.timeLabel = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.trackBar)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.playbackRateControl)).BeginInit();
			this.SuspendLayout();
			// 
			// fwdStepButton
			// 
			this.fwdStepButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.fwdStepButton.Location = new System.Drawing.Point(450, 536);
			this.fwdStepButton.Name = "fwdStepButton";
			this.fwdStepButton.Size = new System.Drawing.Size(75, 23);
			this.fwdStepButton.TabIndex = 4;
			this.fwdStepButton.Text = "Step ->";
			this.fwdStepButton.UseVisualStyleBackColor = true;
			this.fwdStepButton.Click += new System.EventHandler(this.fwdStepButton_Click);
			// 
			// backStepButton
			// 
			this.backStepButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.backStepButton.Location = new System.Drawing.Point(288, 536);
			this.backStepButton.Name = "backStepButton";
			this.backStepButton.Size = new System.Drawing.Size(75, 23);
			this.backStepButton.TabIndex = 3;
			this.backStepButton.Text = "<- Step";
			this.backStepButton.UseVisualStyleBackColor = true;
			this.backStepButton.Click += new System.EventHandler(this.backStepButton_Click);
			// 
			// currentTimeTextBox
			// 
			this.currentTimeTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.currentTimeTextBox.Location = new System.Drawing.Point(636, 507);
			this.currentTimeTextBox.Name = "currentTimeTextBox";
			this.currentTimeTextBox.ReadOnly = true;
			this.currentTimeTextBox.Size = new System.Drawing.Size(151, 20);
			this.currentTimeTextBox.TabIndex = 15;
			this.currentTimeTextBox.TabStop = false;
			this.currentTimeTextBox.Text = "__:__:__ / __:__:__";
			this.currentTimeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// stopButton
			// 
			this.stopButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.stopButton.Enabled = false;
			this.stopButton.Location = new System.Drawing.Point(410, 507);
			this.stopButton.Name = "stopButton";
			this.stopButton.Size = new System.Drawing.Size(75, 23);
			this.stopButton.TabIndex = 1;
			this.stopButton.Text = "Stop";
			this.stopButton.UseVisualStyleBackColor = true;
			this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
			// 
			// pauseButton
			// 
			this.pauseButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.pauseButton.Location = new System.Drawing.Point(369, 536);
			this.pauseButton.Name = "pauseButton";
			this.pauseButton.Size = new System.Drawing.Size(75, 23);
			this.pauseButton.TabIndex = 2;
			this.pauseButton.Text = "Pause";
			this.pauseButton.UseVisualStyleBackColor = true;
			this.pauseButton.Click += new System.EventHandler(this.pauseButton_Click);
			// 
			// playButton
			// 
			this.playButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.playButton.Location = new System.Drawing.Point(328, 507);
			this.playButton.Name = "playButton";
			this.playButton.Size = new System.Drawing.Size(75, 23);
			this.playButton.TabIndex = 0;
			this.playButton.Text = "Play";
			this.playButton.UseVisualStyleBackColor = true;
			this.playButton.Click += new System.EventHandler(this.playButton_Click);
			// 
			// trackBar
			// 
			this.trackBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.trackBar.LargeChange = 100;
			this.trackBar.Location = new System.Drawing.Point(12, 459);
			this.trackBar.Maximum = 1000;
			this.trackBar.Name = "trackBar";
			this.trackBar.Size = new System.Drawing.Size(789, 45);
			this.trackBar.TabIndex = 6;
			this.trackBar.TickFrequency = 0;
			this.trackBar.Scroll += new System.EventHandler(this.trackBar_Scroll);
			// 
			// progressBar
			// 
			this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.progressBar.Location = new System.Drawing.Point(24, 446);
			this.progressBar.Maximum = 1000;
			this.progressBar.Name = "progressBar";
			this.progressBar.Size = new System.Drawing.Size(763, 11);
			this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
			this.progressBar.TabIndex = 10;
			// 
			// playbackRateControl
			// 
			this.playbackRateControl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.playbackRateControl.Increment = new decimal(new int[] {
            1,
            0,
            0,
            196608});
			this.playbackRateControl.Location = new System.Drawing.Point(742, 539);
			this.playbackRateControl.Maximum = new decimal(new int[] {
            64,
            0,
            0,
            0});
			this.playbackRateControl.Minimum = new decimal(new int[] {
            15625,
            0,
            0,
            393216});
			this.playbackRateControl.Name = "playbackRateControl";
			this.playbackRateControl.Size = new System.Drawing.Size(45, 20);
			this.playbackRateControl.TabIndex = 5;
			this.playbackRateControl.ThousandsSeparator = true;
			this.playbackRateControl.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.playbackRateControl.ValueChanged += new System.EventHandler(this.playbackRateControl_ValueChanged);
			// 
			// playbackRateLabel
			// 
			this.playbackRateLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.playbackRateLabel.AutoSize = true;
			this.playbackRateLabel.Location = new System.Drawing.Point(664, 541);
			this.playbackRateLabel.Name = "playbackRateLabel";
			this.playbackRateLabel.Size = new System.Drawing.Size(75, 13);
			this.playbackRateLabel.TabIndex = 19;
			this.playbackRateLabel.Text = "Playback rate:";
			// 
			// videoCheckBox
			// 
			this.videoCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.videoCheckBox.AutoSize = true;
			this.videoCheckBox.Checked = true;
			this.videoCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.videoCheckBox.Enabled = false;
			this.videoCheckBox.Location = new System.Drawing.Point(12, 506);
			this.videoCheckBox.Name = "videoCheckBox";
			this.videoCheckBox.Size = new System.Drawing.Size(53, 17);
			this.videoCheckBox.TabIndex = 7;
			this.videoCheckBox.Text = "Video";
			this.videoCheckBox.UseVisualStyleBackColor = true;
			this.videoCheckBox.CheckedChanged += new System.EventHandler(this.mediaTypeCheckBox_CheckedChanged);
			// 
			// audioCheckBox
			// 
			this.audioCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.audioCheckBox.AutoSize = true;
			this.audioCheckBox.Checked = true;
			this.audioCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.audioCheckBox.Enabled = false;
			this.audioCheckBox.Location = new System.Drawing.Point(12, 527);
			this.audioCheckBox.Name = "audioCheckBox";
			this.audioCheckBox.Size = new System.Drawing.Size(53, 17);
			this.audioCheckBox.TabIndex = 8;
			this.audioCheckBox.Text = "Audio";
			this.audioCheckBox.UseVisualStyleBackColor = true;
			this.audioCheckBox.CheckedChanged += new System.EventHandler(this.mediaTypeCheckBox_CheckedChanged);
			// 
			// videoPanel
			// 
			this.videoPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.videoPanel.Location = new System.Drawing.Point(0, 0);
			this.videoPanel.Name = "videoPanel";
			this.videoPanel.Size = new System.Drawing.Size(813, 440);
			this.videoPanel.TabIndex = 23;
			this.videoPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.videoPanel_Paint);
			// 
			// timeLabel
			// 
			this.timeLabel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			this.timeLabel.Location = new System.Drawing.Point(328, 482);
			this.timeLabel.Name = "timeLabel";
			this.timeLabel.Size = new System.Drawing.Size(157, 22);
			this.timeLabel.TabIndex = 0;
			this.timeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// VideoForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(813, 570);
			this.Controls.Add(this.timeLabel);
			this.Controls.Add(this.videoPanel);
			this.Controls.Add(this.audioCheckBox);
			this.Controls.Add(this.videoCheckBox);
			this.Controls.Add(this.playbackRateLabel);
			this.Controls.Add(this.playbackRateControl);
			this.Controls.Add(this.fwdStepButton);
			this.Controls.Add(this.backStepButton);
			this.Controls.Add(this.currentTimeTextBox);
			this.Controls.Add(this.stopButton);
			this.Controls.Add(this.pauseButton);
			this.Controls.Add(this.playButton);
			this.Controls.Add(this.trackBar);
			this.Controls.Add(this.progressBar);
			this.Name = "VideoForm";
			this.Text = "VideoForm";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VideoForm_FormClosing);
			this.Load += new System.EventHandler(this.VideoForm_Load);
			this.Resize += new System.EventHandler(this.VideoForm_Resize);
			((System.ComponentModel.ISupportInitialize)(this.trackBar)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.playbackRateControl)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion
	}
}