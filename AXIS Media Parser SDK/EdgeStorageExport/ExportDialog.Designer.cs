﻿namespace EdgeStorageExport
{
	partial class ExportDialog
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.progressBar = new System.Windows.Forms.ProgressBar();
			this.openButton = new System.Windows.Forms.Button();
			this.stopButton = new System.Windows.Forms.Button();
			this.exportSpeedLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// progressBar
			// 
			this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.progressBar.Location = new System.Drawing.Point(12, 12);
			this.progressBar.Maximum = 1000;
			this.progressBar.Name = "progressBar";
			this.progressBar.Size = new System.Drawing.Size(435, 23);
			this.progressBar.TabIndex = 0;
			// 
			// openButton
			// 
			this.openButton.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.openButton.Enabled = false;
			this.openButton.Location = new System.Drawing.Point(232, 44);
			this.openButton.Name = "openButton";
			this.openButton.Size = new System.Drawing.Size(75, 23);
			this.openButton.TabIndex = 1;
			this.openButton.Text = "Open File";
			this.openButton.UseVisualStyleBackColor = true;
			this.openButton.Click += new System.EventHandler(this.openButton_Click);
			// 
			// stopButton
			// 
			this.stopButton.Anchor = System.Windows.Forms.AnchorStyles.None;
			this.stopButton.Enabled = false;
			this.stopButton.Location = new System.Drawing.Point(151, 44);
			this.stopButton.Name = "stopButton";
			this.stopButton.Size = new System.Drawing.Size(75, 23);
			this.stopButton.TabIndex = 2;
			this.stopButton.Text = "Stop";
			this.stopButton.UseVisualStyleBackColor = true;
			this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
			// 
			// exportSpeedLabel
			// 
			this.exportSpeedLabel.AutoSize = true;
			this.exportSpeedLabel.Location = new System.Drawing.Point(13, 54);
			this.exportSpeedLabel.Name = "exportSpeedLabel";
			this.exportSpeedLabel.Size = new System.Drawing.Size(18, 13);
			this.exportSpeedLabel.TabIndex = 3;
			this.exportSpeedLabel.Text = "0x";
			// 
			// ExportDialog
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(459, 79);
			this.Controls.Add(this.exportSpeedLabel);
			this.Controls.Add(this.stopButton);
			this.Controls.Add(this.openButton);
			this.Controls.Add(this.progressBar);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ExportDialog";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Export";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ExportDialog_FormClosing);
			this.Load += new System.EventHandler(this.ExportDialog_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ProgressBar progressBar;
		private System.Windows.Forms.Button openButton;
		private System.Windows.Forms.Button stopButton;
		private System.Windows.Forms.Label exportSpeedLabel;
	}
}