﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Xml.Linq;
using System.Text.RegularExpressions;

namespace EdgeStorageExport
{
	/// <summary>
	/// Window to load recording list from Edge storage on Axis device
	/// </summary>
	public partial class RecordingListForm : Form
	{

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new RecordingListForm());
		}

		public RecordingListForm()
		{
			InitializeComponent();
		}

		private void connectButton_Click(object sender, EventArgs e)
		{
			dataGridView1.DataSource = null;
			playButton.Enabled = exportButton.Enabled = false;
			exportRangeGroupBox.Enabled = exportRangeCheckBox.Enabled = false;
			startTimeLabel.Enabled = startTimeTextBox.Enabled = false;
			endTimeLabel.Enabled = endTimeTextBox.Enabled = false;

			DownloadRecordingList();
		}

		private void DownloadRecordingList()
		{
			try
			{
				// Comprehensive information about the Edge Storage API can be found on the partner pages:
				// http://www.axis.com/partner_pages/vapix3.php
				UriBuilder uriBuilder = new UriBuilder();
				uriBuilder.Scheme = "http://";
				uriBuilder.Host = ipBox.Text;
				uriBuilder.Path = "/axis-cgi/record/list.cgi";
				uriBuilder.Query = "recordingid=all";

				WebClient webClient = new WebClient();
				if (!String.IsNullOrEmpty(userNameBox.Text))
				{
					webClient.Credentials = new NetworkCredential(userNameBox.Text, passwordBox.Text);
				}
				string result = webClient.DownloadString(uriBuilder.ToString());

				XDocument xmlDoc = XDocument.Load(new System.IO.StringReader(result));
				var query = from r in xmlDoc.Descendants("recording")
										select new Recording
										{
											RecordingId = (string)r.Attribute("recordingid").Value,
											StartTime = IntParseDateTime(r.Attribute("starttime").Value),
											StopTime = IntParseDateTime(r.Attribute("stoptime").Value),
											RecordingStatus = (string)r.Attribute("recordingstatus").Value,
											MimeType = r.Descendants("video").Count() > 0 ?
												(string)r.Descendants("video").FirstOrDefault().Attribute("mimetype").Value : "",
											FrameRate = r.Descendants("video").Count() > 0 ?
												(string)r.Descendants("video").FirstOrDefault().Attribute("framerate").Value : "",
											Audio = (string)((r.Descendants("audio").Count() > 0) ? "yes" : "no"),
											Host = ipBox.Text,
											Credentials = new NetworkCredential(userNameBox.Text, passwordBox.Text)
										};

				dataGridView1.DataSource = query.ToList();

				if (query.Count() > 0)
				{
					playButton.Enabled = exportButton.Enabled = true;
					exportRangeGroupBox.Enabled = exportRangeCheckBox.Enabled = true;
					startTimeLabel.Enabled = startTimeTextBox.Enabled = exportRangeCheckBox.Checked;
					endTimeLabel.Enabled = endTimeTextBox.Enabled = exportRangeCheckBox.Checked;
				}
				else
				{
					MessageBox.Show("No recordings are available on " + ipBox.Text);
				}
			}
			catch (System.Exception ex)
			{
				MessageBox.Show("Failed to download recording list. " + ex.Message +
					"\n\n(Note that the Edge Storage API is supported from firmware 5.40)");
			}
		}

		private DateTime IntParseDateTime(string dateTimeString)
		{
			DateTime parsedDateTime;
			if (DateTime.TryParse(dateTimeString, out parsedDateTime))
			{
				return parsedDateTime;
			}
			else
			{
				return new DateTime();
			}
		}


		private void playButton_Click(object sender, EventArgs e)
		{
			MessageBox.Show("The AXIS Media Control SDK include sample code " +
				"for implementation of Edge storage playback.");
		}

		private void dataGridView1_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
		{
			exportButton.PerformClick();
		}

		private void exportButton_Click(object sender, EventArgs e)
		{
			if (dataGridView1.SelectedRows.Count > 0)
			{
				Recording recording = (Recording)dataGridView1.SelectedRows[0].DataBoundItem;

				UriBuilder uriBuilder = new UriBuilder();
				uriBuilder.Scheme = "axrtsphttp://";
				uriBuilder.Host = recording.Host;
				uriBuilder.Path = "/axis-media/media.amp";
				// Enable pull-mode to force server to send data as fast as possible
				uriBuilder.Query = "recordingid=" + recording.RecordingId + "&pull=1";

				ExportDialog exportDialog = new ExportDialog(
					uriBuilder.ToString(),
					recording.Credentials.UserName,
					recording.Credentials.Password);

				if (exportRangeCheckBox.Checked)
				{
					try
					{
						DateTime startTime = DateTime.Parse(startTimeTextBox.Text);
						exportDialog.ExportRangeStart = (startTime - recording.StartTime).TotalSeconds;
					}
					catch (System.FormatException)
					{
						MessageBox.Show("Bad start time format");
						return;
					}

					try
					{
						DateTime endTime = DateTime.Parse(endTimeTextBox.Text);
						exportDialog.ExportRangeEnd = (endTime - recording.StartTime).TotalSeconds;
					}
					catch (System.FormatException)
					{
						MessageBox.Show("Bad end time format");
						return;
					}
				}

				exportDialog.ShowDialog();
			}
		}

		private void dataGridView1_SelectionChanged(object sender, EventArgs e)
		{
			if (dataGridView1.SelectedRows.Count > 0)
			{
				Recording recording = (Recording)dataGridView1.SelectedRows[0].DataBoundItem;
				startTimeTextBox.Text = recording.StartTime.ToString();
				endTimeTextBox.Text = recording.StopTime.ToString();
			}
		}

		private void exportRangeCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			startTimeLabel.Enabled = startTimeTextBox.Enabled = exportRangeCheckBox.Checked;
			endTimeLabel.Enabled = endTimeTextBox.Enabled = exportRangeCheckBox.Checked;
		}
	}
}
