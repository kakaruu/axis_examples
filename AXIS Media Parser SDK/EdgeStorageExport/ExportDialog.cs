﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AxisMediaParserLib;
using AXISFILEWRITERLib;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace EdgeStorageExport
{
	/// <summary>
	/// This class demonstrate how the Media Parser can be used to export recordings
	/// from the Edge storage of an Axis camera or video encoder. This particular example uses
	/// the AXIS File Writer to export the recordings as ASF files.
	/// 
	/// The sample code also demonstrates how to export a specific range of a recording.
	/// 
	/// </summary>
	public partial class ExportDialog : Form
	{
		private delegate void ProgressUpdatedDelegate(double value);
		private delegate void ExportSpeedUpdatedDelegate(double value);
		private delegate void EndOfRecordingDelegate();

		private AxisMediaParser parser;
		private AxisFileWriter writer;

		private string exportFileName = "";

		private long lastCaptureTime = 0;
		private long lastTimestamp = 0;

		public ExportDialog(string recordingUrl, string userName, string password)
		{
			InitializeComponent();

			// To avoid dead-lock when aborting export the Parser/Writer must be created in
			// Multi-Threaded Apartment (by default tasks use MTA threads)
			Task.Factory.StartNew(() =>
			{				
				parser = new AxisMediaParser();
				writer = new AxisFileWriter();
			}).Wait();

			// Register for events like OnVideoSample, OnAudioSample, OnTriggerData and OnError
			parser.OnVideoSample += new IAxisMediaParserEvents_OnVideoSampleEventHandler(OnVideoSample);
			parser.OnAudioSample += new IAxisMediaParserEvents_OnAudioSampleEventHandler(OnAudioSample);
			parser.OnError += new IAxisMediaParserEvents_OnErrorEventHandler(OnError);


			// Set connection and media properties
			parser.MediaURL = recordingUrl;
			parser.MediaUsername = userName;
			parser.MediaPassword = password;
		}

		/// <summary>
		/// Set export range start offset (seconds)
		/// </summary>
		public double ExportRangeStart
		{
			set
			{
				parser.StartPosition = (long)(1000 * value); // ms
			}
		}

		/// <summary>
		/// Set export range end offset (seconds)
		/// </summary>
		public double ExportRangeEnd
		{
			set
			{
				parser.StopPosition = (long)(1000 * value); // ms
			}
		}

		private void CloseExport()
		{
			if (parser != null)
			{
				parser.Disconnect();

				// Inform the GC that COM object no longer will be used
				Marshal.FinalReleaseComObject(parser);
				parser = null;
			}

			if (writer != null)
			{
				writer.EndRecording();

				// Inform the GC that COM object no longer will be used
				Marshal.FinalReleaseComObject(writer);
				writer = null;
			}
		}

		void OnVideoSample(int CookieID, int SampleType, int SampleFlags, ulong StartTime, ulong StopTime, object SampleArray)
		{
			writer.WriteStreamSample((ushort)AFW_STREAM_TYPE.AFW_ST_VIDEO, StartTime, (uint)SampleFlags, SampleArray);

			// Get export range duration
			long exportDuration = parser.StopPosition - parser.StartPosition;
			if (parser.Duration > 0 && parser.StopPosition > parser.Duration)
			{
				exportDuration -= (parser.StopPosition - parser.Duration);
			}

			// Calculate export progress
			if (exportDuration > 0)
			{
				double progress = (long)StartTime / exportDuration / 10000.0;
				this.BeginInvoke(new ProgressUpdatedDelegate(OnProgressUpdated), new object[] { progress });
			}

			// Calculate export speed
			long captureTime = DateTime.Now.Ticks;
			if (captureTime - lastCaptureTime > 10000000)
			{
				if (lastCaptureTime != 0)
				{
					double exportSpeed = ((long)StartTime - lastTimestamp) / (captureTime - lastCaptureTime);
					this.BeginInvoke(new ProgressUpdatedDelegate(OnExportSpeedUpdated),
						new object[] { exportSpeed });
				}

				lastCaptureTime = captureTime;
				lastTimestamp = (long)StartTime;
			}
		}

		void OnAudioSample(int CookieID, int SampleType, int SampleFlags, ulong StartTime, ulong StopTime, object SampleArray)
		{
			writer.WriteStreamSample((ushort)AFW_STREAM_TYPE.AFW_ST_AUDIO, StartTime, (uint)SampleFlags, SampleArray);
		}

		void OnError(int ErrorCode)
		{
			if (ErrorCode == (int)AMP_ERROR.AMP_E_END_OF_STREAM)
			{
				this.BeginInvoke(new EndOfRecordingDelegate(OnEndOfRecording));
			}
			else
			{
				AMP_ERROR ampError = (AMP_ERROR)ErrorCode;
				MessageBox.Show(String.Format("OnError - {1} (0x{0:X})", ErrorCode, ampError.ToString()));
			}
		}

		private void ExportDialog_Load(object sender, EventArgs e)
		{
			SaveFileDialog fileDialog = new SaveFileDialog();
			fileDialog.Title = "Export ASF file";
			fileDialog.Filter = "ASF files (*.asf)|*.asf";
			if (DialogResult.OK != fileDialog.ShowDialog())
			{
				this.Close();
				return;
			}

			exportFileName = fileDialog.FileName;

			// Connect to the device and start export
			try
			{
				int cookieID;
				int numberOfStreams;
				object mediaTypeBuffer;
				parser.Connect(out cookieID, out numberOfStreams, out mediaTypeBuffer);
				
				try
				{
					writer.BeginRecord(exportFileName, mediaTypeBuffer);
				}
				catch (COMException ex)
				{
					MessageBox.Show(string.Format("Could not write to file. {0}", ex.Message));
					return;
				}

				parser.Start();
				stopButton.Enabled = true;
			}
			catch (COMException ex)
			{
				MessageBox.Show(string.Format("Error when initiating export for {0}, {1}",
					parser.MediaURL, ex.Message));
			}
		}

		private void OnProgressUpdated(double value)
		{
			int progress = (int)(value * (progressBar.Maximum - progressBar.Minimum));
			progress = Math.Max(progress, progressBar.Minimum);
			progress = Math.Min(progress, progressBar.Maximum);
			progressBar.Value = progress;
		}

		private void OnExportSpeedUpdated(double value)
		{
			exportSpeedLabel.Text = String.Format("x{0}", value);
		}

		private void OnEndOfRecording()
		{
			CloseExport();
			progressBar.Value = progressBar.Maximum;

			openButton.Enabled = true;
			stopButton.Enabled = false;
		}

		private void ExportDialog_FormClosing(object sender, FormClosingEventArgs e)
		{
			CloseExport();
		}

		private void stopButton_Click(object sender, EventArgs e)
		{
			CloseExport();

			openButton.Enabled = true;
			stopButton.Enabled = false;
		}

		private void openButton_Click(object sender, EventArgs e)
		{
			System.Diagnostics.Process.Start(exportFileName);
		}
	}
}
