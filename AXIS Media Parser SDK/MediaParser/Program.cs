using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using AxisMediaParserLib;


/// <summary>
/// This sample shows how to use the AXIS Media Parser component, which parses a media stream and
/// writes the media frames to a file. This file can then be used in the MediaViewer or
/// ASFFileExport samples.
/// Please see the documentation for possible values and error codes.
/// </summary>
class Program
{
	// Connection presets for MJPEG
	// private const string Url = "axmphttp://10.85.158.214/axis-cgi/mjpg/video.cgi";

	// Connection preset for MPEG-4, (HTTP API 2.0)
	// private const string Url = "axrtsphttp://10.85.158.214/mpeg4/1/media.amp";

	// Connection preset for H.264 (HTTP API 3.0)
	private const string Url = "axrtsp://192.168.0.194/axis-media/media.amp";

	// Connection preset with Metadata enabled (not supported by first generation Meta data firmwares)
	// private const string Url = "axrtsphttp://10.85.11.8/axis-media/media.amp?events=on&eventtopic=//.";

	// AMP is also able to parse compatible file containers with supported codec, e.g ASF + H.264
	// private const string Url = @"C:\MyVideo.H264.AAC.asf";


	private const string UserName = "root";
	private const string Password = "!Q2w3e4r5t6y";

	// Target file for recording (written to current directory, e.g.
	// "{SDK}\Samples\Visual C#\MediaParser\bin\x86\Debug")
	private const string FilePath = "media_parser_recording.bin";

	// Time to parse media stream into file
	private const int Duration = 10 * 1000;

	// Event to signal main thread to abort parsing process
	private AutoResetEvent abortEvent = new AutoResetEvent(false);


	// Output file created in Run() and used in OnVideoSample() 
	private BinaryWriter outFile;
	private object fileLock = new object();

	static void Main(string[] args)
	{
		new Program().Run();
	}

	private void Run()
	{
		Console.CancelKeyPress += new ConsoleCancelEventHandler(Console_CancelKeyPress);

		// Open binary output file to write parsed video frames into
		using (FileStream outFileStream = new FileStream(FilePath, FileMode.Create))
		using (outFile = new BinaryWriter(outFileStream))
		{
			// Create the AXIS Media Parser COM object
			AxisMediaParser parser = new AxisMediaParser();
			try
			{
				// Register for events like OnVideoSample, OnAudioSample, OnTriggerData and OnError
				parser.OnVideoSample += OnVideoSample;
				parser.OnAudioSample += OnAudioSample;
				parser.OnError += OnError;
				parser.OnTriggerData += OnTrigger;
				parser.OnMetaDataSample += OnMetaDataSample;

				// Set connection and media properties
				parser.MediaURL = Url;
				parser.MediaUsername = UserName;
				parser.MediaPassword = Password;
				parser.NetworkTimeout = 120000;

				// Get absolute time from Axis device
				parser.TimeFormat = "ntp";

				// Connect to the device
				Console.WriteLine("Connecting to {0}", parser.MediaURL);
				int cookieID;
				int numberOfStreams;
				object buffer;
				parser.Connect(out cookieID, out numberOfStreams, out buffer);

				// Write media type information to out file (buffer is an array of bytes)
				byte[] mediaTypeBuffer = (byte[])buffer;
				outFile.Write(mediaTypeBuffer.Length);
				outFile.Write(mediaTypeBuffer, 0, mediaTypeBuffer.Length);

				// Start the media stream and registered event handlers will be called
				parser.Start();

				// Wait until abort is called or after the specified duration
				abortEvent.WaitOne(Duration);

				// Stop the stream
				parser.Stop();

				// Unregister event handlers
				parser.OnVideoSample -= OnVideoSample;
				parser.OnAudioSample -= OnAudioSample;
				parser.OnError -= OnError;
				parser.OnTriggerData -= OnTrigger;
				parser.OnMetaDataSample -= OnMetaDataSample;

			}
			catch (COMException e)
			{
				Console.WriteLine("Exception for {0}, {1}", parser.MediaURL, e.Message);
			}
			// Inform the GC that COM object no longer will be used
			Marshal.FinalReleaseComObject(parser);
			parser = null;
			Console.WriteLine("Stream stopped");
		}
	}

	void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
	{
		Console.WriteLine("Canceled");
		abortEvent.Set();
	}

	// Event handler callback for video samples buffers
	private void OnVideoSample(int cookieID, int sampleType,
		int sampleFlags, ulong startTime, ulong stopTime, object SampleArray)
	{
		// Cast the buffer object to a byte array
		byte[] bufferBytes = (byte[])SampleArray;
		Console.WriteLine("OnVideoSample - {0} bytes {1}", bufferBytes.Length, startTime);

		// Write the data to out file
		lock (fileLock)
		{
			outFile.Write(sampleType);
			outFile.Write(sampleFlags);
			outFile.Write(startTime);
			outFile.Write(stopTime);
			outFile.Write(bufferBytes.Length);
			outFile.Write(bufferBytes, 0, bufferBytes.Length);
		}
	}


	// Event handler callback for audio samples buffers
	private void OnAudioSample(int cookieID, int sampleType,
		int sampleFlags, ulong startTime, ulong stopTime, object SampleArray)
	{
		// Cast the buffer object to a byte array
		byte[] bufferBytes = (byte[])SampleArray;
		Console.WriteLine("OnAudioSample - {0} bytes {1}", bufferBytes.Length, startTime);

		// Write the data to out file
		lock (fileLock)
		{
			outFile.Write(sampleType);
			outFile.Write(sampleFlags);
			outFile.Write(startTime);
			outFile.Write(stopTime);
			outFile.Write(bufferBytes.Length);
			outFile.Write(bufferBytes, 0, bufferBytes.Length);
		}
	}


	// Event handler callback from AxisMediaParser in case of errors during stream parsing
	private void OnError(int errorCode)
	{
		if (errorCode == (int)AMP_ERROR.AMP_E_END_OF_STREAM)
		{
			Console.WriteLine("End of stream");
			abortEvent.Set();
		}
		else
		{
			AMP_ERROR ampError = (AMP_ERROR)errorCode;
			Console.WriteLine("OnError - {1} (0x{0:X})", errorCode, ampError.ToString());
			Console.ReadKey();
		}
	}


	// Event handler callback for video trigger data
	void OnTrigger(int CookieID, ulong StartTime, uint UserTime, short UserTimeFract,
	 uint UnitTime, short UnitTimeFract, bool UnitTimeInvalid, string TriggerData)
	{
		Console.WriteLine("OnTriggerData - {0}", TriggerData);
	}

	// Event handler callback for Metadata
	void OnMetaDataSample(int CookieID, int sampleType, int sampleFlags,
		ulong startTime, ulong stopTime, string MetaData)
	{
		// Cast the buffer object to a byte array (UTF-8 encoding)
		byte[] bufferBytes = System.Text.Encoding.UTF8.GetBytes(MetaData);

		Console.WriteLine("OnMetaDataSample - {0} bytes", bufferBytes.Length);

		// Write the data to out file
		lock (fileLock)
		{
			outFile.Write(sampleType);
			outFile.Write(sampleFlags);
			outFile.Write(startTime);
			outFile.Write(stopTime);
			outFile.Write(bufferBytes.Length);
			outFile.Write(bufferBytes, 0, bufferBytes.Length);
		}
	}
}


