using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using AXISFILEWRITERLib;
using AxisMediaParserLib;


/// <summary>
/// This sample shows how to use the AXIS File Writer component, which creates an
/// ASF file based on video and audio samples generated by the AXIS Media Parser.
/// Please see the documentation for possible values and error codes.
/// </summary>
class Program
{
	// Source binary file generated with MediaParser sample.
	// Sample file included with SDK copied to target directory during post-build event.
	private static string InputFilePath = "sample.H264.G711.bin";
	//private static string InputFilePath = "flower.H264.AAC.bin"; // MP4 export

	// Target format (MP4 requires H.264 or AAC, use "flower.H264.AAC.bin")
	private static AFW_FORMAT OutputFileFormat = AFW_FORMAT.AFW_FORMAT_ASF;
	//private static AFW_FORMAT OutputFileFormat = AFW_FORMAT.AFW_FORMAT_MP4;

	// Target file (exported to current directory, e.g.
	// "{SDK}\Samples\Visual C#\ASFFileExport\bin\x86\Debug")
	private static string OutputFilePath = "export"; // extension is added dynamically

	private AxisFileWriter writer;

	static void Main(string[] args)
	{
		new Program().Run();
	}

	private void Run()
	{
		Console.CancelKeyPress += new ConsoleCancelEventHandler(Console_CancelKeyPress);

		switch (OutputFileFormat)
		{
			case AFW_FORMAT.AFW_FORMAT_ASF:
				OutputFilePath += ".asf";
				break;
			case AFW_FORMAT.AFW_FORMAT_MP4:
				OutputFilePath += ".mp4";
				break;
		}
		
		// Create the AXIS File Writer COM object
		writer = new AxisFileWriter();
		writer.FileFormat = (int)OutputFileFormat;

		if (!writer.CheckRecordingCapability())
		{
			Console.WriteLine("Error: Required features not available on this system.");
			Console.ReadKey();
			return;
		}

		// Exclusive features for ASF file writing
		if (writer.FileFormat == (int)AFW_FORMAT.AFW_FORMAT_ASF)
		{
			// Add Digital Signature (verification is made using AxisFileAuthenticator class)
			writer.AddSignature = true;
			writer.SignaturePassword = "secret";

			// Add predefined or custom attributes.
			// https://msdn.microsoft.com/en-us/library/windows/desktop/dd562627.aspx
			writer.AttributeStore.Set("WM/ToolName", "ASFFileExport sample");
			writer.AttributeStore.Set("Axis/MagicNumber", (uint)1984);
		}

		this.WriteDataToFile();

		// Inform the GC that COM object no longer will be used
		Marshal.FinalReleaseComObject(writer);
		writer = null;
	}

	private void WriteDataToFile()
	{
		try
		{
			// Open binary input file to read parsed video frames from
			using (FileStream inFileStream = new FileStream(InputFilePath, FileMode.Open))
			using (BinaryReader inFile = new BinaryReader(inFileStream))
			{
				Console.WriteLine("Opened source file: \n{0}\n", inFileStream.Name);

				// Read media type information from file
				int mediaTypeSize = inFile.ReadInt32();
				byte[] mediaTypeBuffer = inFile.ReadBytes(mediaTypeSize);

				// Init the Writer with media type buffer and output file path. 
				writer.BeginRecord(OutputFilePath, mediaTypeBuffer);
				Console.WriteLine("Opened target file {0}", OutputFilePath);

				// While there are frames in the file, read each frame and let the writer render it
				bool isDiscontinity = true;
				while (true)
				{
					try
					{
						// Read frame data
						int sampleType = inFile.ReadInt32();
						int sampleFlags = inFile.ReadInt32();
						ulong startTime = inFile.ReadUInt64();
						ulong stopTime = inFile.ReadUInt64();
						int bufferSize = inFile.ReadInt32();
						byte[] bufferBytes = inFile.ReadBytes(bufferSize);

						Console.WriteLine("type {0}, flags {1}, start {2}, size {3}",
							sampleType, sampleFlags, startTime, bufferSize);

						AFW_STREAM_TYPE streamType;
						uint flags = 0;
						switch (sampleType)
						{
							case (int)AMP_VIDEO_SAMPLE_TYPE.AMP_VST_MPEG4_VIDEO_CONFIG:
							case (int)AMP_VIDEO_SAMPLE_TYPE.AMP_VST_MPEG4_VIDEO_IVOP:
							case (int)AMP_VIDEO_SAMPLE_TYPE.AMP_VST_MJPEG_VIDEO:
							case (int)AMP_VIDEO_SAMPLE_TYPE.AMP_VST_H264_VIDEO_CONFIG:
							case (int)AMP_VIDEO_SAMPLE_TYPE.AMP_VST_H264_VIDEO_IDR:
							case (int)AMP_VIDEO_SAMPLE_TYPE.AMP_VST_H264_VIDEO_NON_IDR:
							case (int)AMP_VIDEO_SAMPLE_TYPE.AMP_VST_H264_VIDEO_SEI:
							case (int)AMP_VIDEO_SAMPLE_TYPE.AMP_VST_H265_VIDEO_CONFIG:
							case (int)AMP_VIDEO_SAMPLE_TYPE.AMP_VST_H265_VIDEO_IDR:
							case (int)AMP_VIDEO_SAMPLE_TYPE.AMP_VST_H265_VIDEO_NON_IDR:
								streamType = AFW_STREAM_TYPE.AFW_ST_VIDEO;
								flags |= (uint)AFW_WRITE_STREAM_SAMPLE_FLAGS.AFW_WSSF_CLEANPOINT;
								break;
							case (int)AMP_VIDEO_SAMPLE_TYPE.AMP_VST_MPEG4_VIDEO_PVOP:
							case (int)AMP_VIDEO_SAMPLE_TYPE.AMP_VST_MPEG4_VIDEO_BVOP:
							case (int)AMP_VIDEO_SAMPLE_TYPE.AMP_VST_MPEG4_VIDEO_FRAGMENT:
								streamType = AFW_STREAM_TYPE.AFW_ST_VIDEO;
								break;
							case (int)AMP_VIDEO_SAMPLE_TYPE.AMP_VST_MPEG4_AUDIO:
								streamType = AFW_STREAM_TYPE.AFW_ST_AUDIO;
								flags |= (uint)AFW_WRITE_STREAM_SAMPLE_FLAGS.AFW_WSSF_CLEANPOINT;
								break;
							case (int)AMP_VIDEO_SAMPLE_TYPE.AMP_VST_METADATA_ONVIF:
								// Metadata streams are not supported by AXIS File Writer
								continue;
							default:
								throw new Exception("Unknown sample type");
						}


						if (isDiscontinity)
						{
							flags |= (uint)AFW_WRITE_STREAM_SAMPLE_FLAGS.AFW_WSSF_DISCONTINUITY;
						}

						writer.WriteStreamSample((ushort)streamType, startTime, flags, bufferBytes);
						isDiscontinity = false;
					}
					catch (EndOfStreamException)
					{
						// end-of-stream reached
						break;
					}
				}

				writer.EndRecording();
				Console.WriteLine("Export complete.");
				Console.WriteLine("Output file: " + OutputFilePath);
				Console.ReadKey();
			}
		}
		catch (COMException e)
		{
			if (e.ErrorCode == (int)AFW_ERROR.AFW_E_UNSUPPORTED_MEDIA_CODEC)
			{
				Console.WriteLine(
					"Error: The video or audio codec is not supported for this file format.\n" +
					"Note that MP4 export requires H.264 and/or AAC.");
				Console.ReadKey();
			}
			else
			{
				Console.WriteLine("Error: Exception for {0}, {1}", InputFilePath, e.Message);
				Console.ReadKey();
			}
		}
	}

	void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
	{
		try
		{
			writer.EndRecording();
		}
		catch (COMException ex)
		{
			Console.WriteLine("Error: Exception for {0}, {1}", InputFilePath, ex.Message);
			Console.ReadKey();
		}
	}
}

