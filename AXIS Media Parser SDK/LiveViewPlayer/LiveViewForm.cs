using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AxisMediaParserLib;      
using AxisMediaViewerLib;
using System.Threading;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.IO;

/// <summary>
/// This sample shows how to use the AXIS Media Parser and the AXIS Media Viewer components,
/// where the parser parses a live media stream and each frame is rendered by the viewer.
/// Please see the documentation for possible values and error codes.
/// </summary>
public class LiveViewForm : Form
{
	// NTP time-stamps start from this date
	private readonly DateTime NtpTimeEpoch = new DateTime(1900, 1, 1);

	// Connection presets for MJPEG
  // private const string Url = "axmphttp://10.85.11.4/axis-cgi/mjpg/video.cgi";

	// Connection preset for MPEG-4, (HTTP API 2.0)
	// private const string Url = "axrtsphttp://10.85.11.2/mpeg4/1/media.amp";

  // Connection preset for H.264 (HTTP API 3.0)
  private const string Url = "axrtsphttp://10.85.11.8/axis-media/media.amp?videocodec=h264";

	// Connection preset for H.264 + metadata stream (HTTP API 3.0)
	// private const string Url = "axrtsphttp://10.85.11.8/axis-media/media.amp?videocodec=h264&events=on&eventtopic=//.";

	private const string UserName = "root";
	private const string Password = "pass";

	private AxisMediaParser parser;
	private AxisMediaViewer viewer;
	private int viewerWidth;
	private int viewerHeight;
	private Button stopButton;
	private Button startButton;
	private Panel videoFrame;
	private Label timeLabel;
	// Enable to save the first decoded image to disk.
	private bool saveFirstImageToDisk = true;


	static void Main()
	{
		Application.EnableVisualStyles();
		Application.SetCompatibleTextRenderingDefault(false);
		Application.Run(new LiveViewForm());
	}


	public LiveViewForm()
	{
		InitializeComponent();
	}
	
	private void VideoForm_Load(object sender, EventArgs e)
	{
		// Create the AXIS Media Parser/Viewer COM objects in a MTA apartment to allow multiple
		// threads (UI and render thread) to access COM object (by default tasks use MTA threads).
		Task.Factory.StartNew(() =>
		{
			// Create AXIS Media Parser and AXIS Media Viewer components
			parser = new AxisMediaParser();
			viewer = new AxisMediaViewer();
		}).Wait();

		// Register for callback events like OnVideoSample, OnAudioSample and OnError
		parser.OnVideoSample += OnVideoSample;
		parser.OnAudioSample += OnAudioSample;
		parser.OnMetaDataSample += OnMetaDataSample;
		parser.OnError += OnError;

		// Set parser properties
		parser.ShowLoginDialog = true;
		parser.MediaURL = Url;
		parser.MediaUsername = UserName;
		parser.MediaPassword = Password;

		// Set viewer properties
		viewer.VideoRenderer = (int)AMV_RENDER_OPTIONS.AMV_VIDEO_RENDERER_EVR;
		if (saveFirstImageToDisk)
		{
			viewer.OnDecodedImage += OnDecodedImage;
			viewer.ColorSpace = (short)AMV_COLOR_SPACE.AMV_CS_RGB24;
			viewer.EnableOnDecodedImage = true;
		}
		else
		{
			viewer.ColorSpace = (short)AMV_COLOR_SPACE.AMV_CS_YUY2;
			viewer.EnableOnDecodedImage = false;
		}

		viewer.LiveMode = true;

		// Parser connects to the device
		Console.WriteLine("Connecting to {0}", parser.MediaURL);
		int cookieID;
		int numberOfStreams;
		object mediaTypeBuffer;
		parser.Connect(out cookieID, out numberOfStreams, out mediaTypeBuffer);

		// Init the Viewer with media type buffer and a win32 hWnd handle to the window. 
		viewer.Init(0, mediaTypeBuffer, this.videoFrame.Handle.ToInt64());

		// Get the video frame size and resize the form to fit the video stream 
		viewer.GetVideoSize(out viewerWidth, out viewerHeight);
		ResizeFormToFitVideoSize(viewerWidth, viewerHeight);

		// Start the viewer before rendering the first frame
		viewer.Start();

		// Start the media stream, OnVideoSample will be called for each frame
		parser.Start();
	}

	// Event handler from the parser for each video frame buffer
	private void OnVideoSample(int cookieID, int sampleType,
		int sampleFlags, ulong startTime, ulong stopTime, object SampleArray)
	{
		// Let viewer render video sample

		// Add LiveTimeOffset to original timestamp for optimal latency when rendering
		long renderStartTime = 0;
		long renderStopTime = 1;
		if ((long)startTime + parser.LiveTimeOffset > 0)
		{
			renderStartTime = (long)startTime + parser.LiveTimeOffset;
			renderStopTime = (long)stopTime + parser.LiveTimeOffset;
		}

		viewer.RenderVideoSample(sampleFlags, (ulong)renderStartTime, (ulong)renderStopTime, SampleArray);

		// When NTPTimeOffset is not zero it can be used to convert to camera clock time
		if (parser.NTPTimeOffset > 0)
		{
			ulong dateTimeTicks = startTime + parser.NTPTimeOffset + (ulong)NtpTimeEpoch.Ticks;
			DateTime cameraTime = new DateTime((long)dateTimeTicks);

			// Note that we display the time when the frame was received not rendered
			UpdateTimeDisplay(cameraTime.ToLocalTime());
		}
	}

	// Event handler from the parser for each audio frame buffer
	private void OnAudioSample(int cookieID, int sampleType,
		int sampleFlags, ulong startTime, ulong stopTime, object SampleArray)
	{
		// Let viewer render audio sample

		// Add LiveTimeOffset to original timestamp for optimal latency when rendering
		long renderStartTime = 0;
		long renderStopTime = 1;
		if ((long)startTime + parser.LiveTimeOffset > 0)
		{
			renderStartTime = (long)startTime + parser.LiveTimeOffset;
			renderStopTime = (long)stopTime + parser.LiveTimeOffset;
		}

		viewer.RenderAudioSample(sampleFlags, (ulong)renderStartTime, (ulong)renderStopTime, SampleArray);
	}

	private void OnMetaDataSample(int cookieID, int sampleType,
		int sampleFlags, ulong startTime, ulong stopTime, string metaData)
	{
		// Process metadata samples here

		// Let viewer overlay metadata sample
		//
		// //Add LiveTimeOffset to original timestamp for optimal latency when rendering
		//long renderStartTime = 0;
		//long renderStopTime = 1;
		//if ((long)startTime + parser.LiveTimeOffset > 0)
		//{
		//  renderStartTime = (long)startTime + parser.LiveTimeOffset;
		//  renderStopTime = (long)stopTime + parser.LiveTimeOffset;
		//}
		//
		// viewer.RenderMetadataSample(sampleFlags, renderStartTime, renderStopTime, metaData);
	}

	// Resizes the form to fit the contents of the video stream
	private void ResizeFormToFitVideoSize(int width, int height)
	{
		int formWidthChange = width - videoFrame.ClientSize.Width;
		int formHeightChange = height - videoFrame.ClientSize.Height;
		ClientSize = new Size(ClientSize.Width + formWidthChange,
			ClientSize.Height + formHeightChange);
	}

	void UpdateTimeDisplay(DateTime time)
	{
		if (InvokeRequired)
		{
			// If called from a non UI thread, let the UI thread perform the call 
			BeginInvoke(new Action<DateTime>(UpdateTimeDisplay), new object[] { time });
			return;
		}

		// Update the time label
		timeLabel.Text = time.ToString("yyyy-MM-dd HH:mm:ss.fff");
	}
	 
	// Event handler from AxisMediaParser in case of errors during stream parsing
	private static void OnError(int errorCode)
	{
		AMP_ERROR ampError = (AMP_ERROR)errorCode;
		MessageBox.Show(string.Format("Parser OnErrorEventHandler {1} (0x{0:X})",
			errorCode, ampError.ToString()));
	}


	// Event called when the dialog is resized, the viewer is informed about the new size
	private void videoFrame_Resize(object sender, EventArgs e)
	{
		if (viewer == null)
		{
			return;
		}

		if (videoFrame.ClientSize.Width != viewerWidth ||
			videoFrame.ClientSize.Height != viewerHeight)
		{
			// The video frame area has changed, so the viewer needs to be informed
			viewerWidth = videoFrame.ClientSize.Width;
			viewerHeight = videoFrame.ClientSize.Height;
			viewer.SetVideoPosition(0, 0, viewerWidth, viewerHeight);
		}
	}

	// Event handler for Stop button click
	private void Stop_Click(object sender, EventArgs e)
	{
		if (parser.Status == (int)AMP_STATUS.AMP_STATUS_RUNNING)
		{
			parser.Stop();
			viewer.Stop();
		}
	}

	// Event handler for Start button click
	private void Start_Click(object sender, EventArgs e)
	{
		if (parser.Status == (int)AMP_STATUS.AMP_STATUS_IDLE)
		{
			parser.Start();
			viewer.Start();
		}
	}

	private void videoFrame_Paint(object sender, PaintEventArgs e)
	{
		if (viewer != null)
		{
			viewer.RepaintVideo();
		}
	}

	// Event handler for form closing, stopping parser and viewer
	private void VideoForm_FormClosing(object sender, FormClosingEventArgs e)
	{
		if (parser.Status == (int)AMP_STATUS.AMP_STATUS_RUNNING)
		{
			parser.Stop();
			viewer.Stop();
		}

		// Unregister event handlers
		parser.OnVideoSample -= OnVideoSample;
		parser.OnAudioSample -= OnAudioSample;
		parser.OnMetaDataSample -= OnMetaDataSample;
		parser.OnError -= OnError;

		viewer.OnDecodedImage -= OnDecodedImage;

		// Inform the GC that COM object no longer will be used
		Marshal.FinalReleaseComObject(viewer);
		viewer = null;
    
		Marshal.FinalReleaseComObject(parser);
		parser = null;
	}
	private void OnDecodedImage(ulong StartTime, short ColorSpace, object SampleArray)
	{
		if (saveFirstImageToDisk)
		{
			byte[] decodedData = (byte[])SampleArray;
			Bitmap bm = new Bitmap(CreateBitmap(decodedData));
			bm.Save("firstImage.jpeg", System.Drawing.Imaging.ImageFormat.Jpeg);
			saveFirstImageToDisk = false;
		}
	}
	private static Bitmap CreateBitmap(byte[] data)
	{
		using (MemoryStream memoryStream = new MemoryStream())
		using (BinaryWriter binaryWriter = new BinaryWriter(memoryStream))
		{
			int bitmapFileHeaderSize = 14;
			binaryWriter.Write('B');
			binaryWriter.Write('M');
			binaryWriter.Write(bitmapFileHeaderSize + data.Length); // 4 bytes
			binaryWriter.Write((short)0);
			binaryWriter.Write((short)0);
			int bitmapInfoHeaderLength = BitConverter.ToInt32(data, 0);
			binaryWriter.Write(bitmapFileHeaderSize + bitmapInfoHeaderLength);
			binaryWriter.Write(data);

			return (Bitmap)Image.FromStream(memoryStream);
		}
	}



	#region Windows Form Designer generated code

	// Required designer variable.
	private System.ComponentModel.IContainer components = null;

	// Clean up any resources being used.
	protected override void Dispose(bool disposing)
	{
		if (disposing && (components != null))
		{
			components.Dispose();
		}
		base.Dispose(disposing);
	}


	// Required method for Designer support - do not modify
	// the contents of this method with the code editor.
	private void InitializeComponent()
	{
			this.stopButton = new System.Windows.Forms.Button();
			this.startButton = new System.Windows.Forms.Button();
			this.videoFrame = new System.Windows.Forms.Panel();
			this.timeLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// stopButton
			// 
			this.stopButton.Location = new System.Drawing.Point(93, 12);
			this.stopButton.Name = "stopButton";
			this.stopButton.Size = new System.Drawing.Size(75, 23);
			this.stopButton.TabIndex = 1;
			this.stopButton.Text = "Stop";
			this.stopButton.UseVisualStyleBackColor = true;
			this.stopButton.Click += new System.EventHandler(this.Stop_Click);
			// 
			// startButton
			// 
			this.startButton.Location = new System.Drawing.Point(12, 12);
			this.startButton.Name = "startButton";
			this.startButton.Size = new System.Drawing.Size(75, 23);
			this.startButton.TabIndex = 2;
			this.startButton.Text = "Start";
			this.startButton.UseVisualStyleBackColor = true;
			this.startButton.Click += new System.EventHandler(this.Start_Click);
			// 
			// videoFrame
			// 
			this.videoFrame.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.videoFrame.Location = new System.Drawing.Point(12, 41);
			this.videoFrame.Name = "videoFrame";
			this.videoFrame.Size = new System.Drawing.Size(313, 222);
			this.videoFrame.TabIndex = 3;
			this.videoFrame.Paint += new System.Windows.Forms.PaintEventHandler(this.videoFrame_Paint);
			this.videoFrame.Resize += new System.EventHandler(this.videoFrame_Resize);
			// 
			// timeLabel
			// 
			this.timeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.timeLabel.AutoSize = true;
			this.timeLabel.Location = new System.Drawing.Point(190, 17);
			this.timeLabel.Name = "timeLabel";
			this.timeLabel.Size = new System.Drawing.Size(0, 13);
			this.timeLabel.TabIndex = 4;
			// 
			// LiveViewForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(337, 275);
			this.Controls.Add(this.startButton);
			this.Controls.Add(this.stopButton);
			this.Controls.Add(this.videoFrame);
			this.Controls.Add(this.timeLabel);
			this.Name = "LiveViewForm";
			this.Text = "Live view";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VideoForm_FormClosing);
			this.Load += new System.EventHandler(this.VideoForm_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

	}

	#endregion
}
