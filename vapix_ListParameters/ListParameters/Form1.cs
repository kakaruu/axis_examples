﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;


namespace ListParameters
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Clear the rich text box every time the user clicks the Get Param button.
            
            richTextBox1.Text = "";
           

            //Create a string. The string will be used to pass the VAPIX(R) command to the Axis product.
            string ParamList;

            try
            {
                
                //Enter the VAPIX command. Here we use param.cgi with the list action. 192.168.0.90 is the IP address of the Axis product.
                ParamList = "http://192.168.0.247/axis-cgi/param.cgi?action=list&responseformat=rfc";

                // Enter the user name and password for the Axis product.

                NetworkCredential networkCredential = new NetworkCredential("root","!Q2w3e4r5t6y");

                
                //Associate the 'NetworkCredential' object with the 'WebRequest' object.
                //Read more about WebRequest at
                //http://msdn.microsoft.com/en-us/library/system.net.webrequest.credentials(v=vs.71).aspx



                WebRequest request = WebRequest.Create(ParamList);
                request.Credentials = networkCredential;  
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
               

                //Read Parameter List and display the response in the rich text box.
                //Read more about the HaveResponse property at
                //http://msdn.microsoft.com/en-us/library/system.net.httpwebrequest.haveresponse(v=vs.110).aspx

              
                Stream streamResponse = response.GetResponseStream();
                StreamReader streamRead = new StreamReader(streamResponse);
                Char[] readBuff = new Char[256000];
                int count = streamRead.Read(readBuff, 0, 256000);
                richTextBox1.Text += String.Format("\nAXIS PARAMETER LIST:\n");
                while (count > 0)
                {
                    String outputData = new String(readBuff, 0, count);
                    richTextBox1.Text += String.Format("{0}", outputData);
                    count = streamRead.Read(readBuff, 0, 256000);
                }


                // Close and release the response.
                streamRead.Close();
                streamResponse.Close();
                response.Close();

            }

            catch (Exception es)
            {
                MessageBox.Show(es.ToString(), "\nError Message");


            }

        }
    }
}
