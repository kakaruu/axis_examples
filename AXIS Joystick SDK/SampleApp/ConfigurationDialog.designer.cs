namespace SampleApp
{
  partial class ConfigurationDialog
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.okButton = new System.Windows.Forms.Button();
      this.cancelButton = new System.Windows.Forms.Button();
      this.valueBoxPan = new System.Windows.Forms.TextBox();
      this.applyButton = new System.Windows.Forms.Button();
      this.label2 = new System.Windows.Forms.Label();
      this.valueBoxTilt = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.valueBoxZoom = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.JoystickData = new System.Data.DataSet();
      this.AxisConfigurations = new System.Data.DataTable();
      this.dataColumn1 = new System.Data.DataColumn();
      this.dataColumn2 = new System.Data.DataColumn();
      this.dataColumn3 = new System.Data.DataColumn();
      this.dataColumn4 = new System.Data.DataColumn();
      this.dataColumn5 = new System.Data.DataColumn();
      this.dataColumn6 = new System.Data.DataColumn();
      this.dataColumn7 = new System.Data.DataColumn();
      this.invertCheckPan = new System.Windows.Forms.CheckBox();
      this.invertCheckTilt = new System.Windows.Forms.CheckBox();
      this.invertCheckZoom = new System.Windows.Forms.CheckBox();
      ((System.ComponentModel.ISupportInitialize)(this.JoystickData)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.AxisConfigurations)).BeginInit();
      this.SuspendLayout();
      // 
      // okButton
      // 
      this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.okButton.Location = new System.Drawing.Point(22, 97);
      this.okButton.Name = "okButton";
      this.okButton.Size = new System.Drawing.Size(75, 23);
      this.okButton.TabIndex = 0;
      this.okButton.Text = "Ok";
      this.okButton.UseVisualStyleBackColor = true;
      this.okButton.Click += new System.EventHandler(this.okButton_Click);
      // 
      // cancelButton
      // 
      this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.cancelButton.Location = new System.Drawing.Point(103, 97);
      this.cancelButton.Name = "cancelButton";
      this.cancelButton.Size = new System.Drawing.Size(75, 23);
      this.cancelButton.TabIndex = 1;
      this.cancelButton.Text = "Cancel";
      this.cancelButton.UseVisualStyleBackColor = true;
      this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
      // 
      // valueBoxPan
      // 
      this.valueBoxPan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.valueBoxPan.Location = new System.Drawing.Point(103, 9);
      this.valueBoxPan.MaxLength = 8;
      this.valueBoxPan.Name = "valueBoxPan";
      this.valueBoxPan.Size = new System.Drawing.Size(75, 20);
      this.valueBoxPan.TabIndex = 2;
      this.valueBoxPan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.valueBoxPan.WordWrap = false;
      this.valueBoxPan.TextChanged += new System.EventHandler(this.valueBox_TextChanged);
      // 
      // applyButton
      // 
      this.applyButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.applyButton.Enabled = false;
      this.applyButton.Location = new System.Drawing.Point(184, 97);
      this.applyButton.Name = "applyButton";
      this.applyButton.Size = new System.Drawing.Size(75, 23);
      this.applyButton.TabIndex = 1;
      this.applyButton.Text = "Apply";
      this.applyButton.UseVisualStyleBackColor = true;
      this.applyButton.Click += new System.EventHandler(this.applyButton_Click);
      // 
      // label2
      // 
      this.label2.AutoEllipsis = true;
      this.label2.Location = new System.Drawing.Point(12, 9);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(88, 20);
      this.label2.TabIndex = 4;
      this.label2.Text = "Pan Sensitivity";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // valueBoxTilt
      // 
      this.valueBoxTilt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.valueBoxTilt.Location = new System.Drawing.Point(103, 35);
      this.valueBoxTilt.MaxLength = 8;
      this.valueBoxTilt.Name = "valueBoxTilt";
      this.valueBoxTilt.Size = new System.Drawing.Size(75, 20);
      this.valueBoxTilt.TabIndex = 2;
      this.valueBoxTilt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.valueBoxTilt.WordWrap = false;
      this.valueBoxTilt.TextChanged += new System.EventHandler(this.valueBox_TextChanged);
      // 
      // label1
      // 
      this.label1.AutoEllipsis = true;
      this.label1.Location = new System.Drawing.Point(12, 35);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(88, 20);
      this.label1.TabIndex = 4;
      this.label1.Text = "Tilt Sensitivity";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // valueBoxZoom
      // 
      this.valueBoxZoom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.valueBoxZoom.Location = new System.Drawing.Point(103, 61);
      this.valueBoxZoom.MaxLength = 8;
      this.valueBoxZoom.Name = "valueBoxZoom";
      this.valueBoxZoom.Size = new System.Drawing.Size(75, 20);
      this.valueBoxZoom.TabIndex = 2;
      this.valueBoxZoom.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      this.valueBoxZoom.WordWrap = false;
      this.valueBoxZoom.TextChanged += new System.EventHandler(this.valueBox_TextChanged);
      // 
      // label3
      // 
      this.label3.AutoEllipsis = true;
      this.label3.Location = new System.Drawing.Point(12, 61);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(88, 20);
      this.label3.TabIndex = 4;
      this.label3.Text = "Zoom Sensitivity";
      this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // JoystickData
      // 
      this.JoystickData.DataSetName = "NewDataSet";
      this.JoystickData.Tables.AddRange(new System.Data.DataTable[] {
            this.AxisConfigurations});
      // 
      // AxisConfigurations
      // 
      this.AxisConfigurations.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7});
      this.AxisConfigurations.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Axis"}, false)});
      this.AxisConfigurations.Locale = new System.Globalization.CultureInfo("");
      this.AxisConfigurations.TableName = "AxisConfigurations";
      // 
      // dataColumn1
      // 
      this.dataColumn1.ColumnName = "Axis";
      this.dataColumn1.ReadOnly = true;
      // 
      // dataColumn2
      // 
      this.dataColumn2.ColumnName = "DeadZone";
      this.dataColumn2.DataType = typeof(int);
      // 
      // dataColumn3
      // 
      this.dataColumn3.ColumnName = "Threshold";
      this.dataColumn3.DataType = typeof(int);
      // 
      // dataColumn4
      // 
      this.dataColumn4.ColumnName = "Overlap";
      this.dataColumn4.DataType = typeof(int);
      // 
      // dataColumn5
      // 
      this.dataColumn5.ColumnName = "Sensitivity";
      this.dataColumn5.DataType = typeof(decimal);
      // 
      // dataColumn6
      // 
      this.dataColumn6.ColumnName = "CurveCoeff";
      this.dataColumn6.DataType = typeof(decimal);
      // 
      // dataColumn7
      // 
      this.dataColumn7.ColumnName = "Inverted";
      this.dataColumn7.DataType = typeof(bool);
      // 
      // invertCheckPan
      // 
      this.invertCheckPan.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.invertCheckPan.Location = new System.Drawing.Point(184, 9);
      this.invertCheckPan.Name = "invertCheckPan";
      this.invertCheckPan.Size = new System.Drawing.Size(75, 20);
      this.invertCheckPan.TabIndex = 5;
      this.invertCheckPan.Text = "invert axis";
      this.invertCheckPan.UseVisualStyleBackColor = true;
      this.invertCheckPan.CheckedChanged += new System.EventHandler(this.invertCheck_CheckedChanged);
      // 
      // invertCheckTilt
      // 
      this.invertCheckTilt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.invertCheckTilt.Location = new System.Drawing.Point(184, 35);
      this.invertCheckTilt.Name = "invertCheckTilt";
      this.invertCheckTilt.Size = new System.Drawing.Size(75, 20);
      this.invertCheckTilt.TabIndex = 5;
      this.invertCheckTilt.Text = "invert axis";
      this.invertCheckTilt.UseVisualStyleBackColor = true;
      this.invertCheckTilt.CheckedChanged += new System.EventHandler(this.invertCheck_CheckedChanged);
      // 
      // invertCheckZoom
      // 
      this.invertCheckZoom.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.invertCheckZoom.Location = new System.Drawing.Point(184, 61);
      this.invertCheckZoom.Name = "invertCheckZoom";
      this.invertCheckZoom.Size = new System.Drawing.Size(75, 20);
      this.invertCheckZoom.TabIndex = 5;
      this.invertCheckZoom.Text = "invert axis";
      this.invertCheckZoom.UseVisualStyleBackColor = true;
      this.invertCheckZoom.CheckedChanged += new System.EventHandler(this.invertCheck_CheckedChanged);
      // 
      // ConfigurationDialog
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(271, 132);
      this.ControlBox = false;
      this.Controls.Add(this.invertCheckZoom);
      this.Controls.Add(this.invertCheckTilt);
      this.Controls.Add(this.invertCheckPan);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.valueBoxZoom);
      this.Controls.Add(this.valueBoxTilt);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.valueBoxPan);
      this.Controls.Add(this.applyButton);
      this.Controls.Add(this.cancelButton);
      this.Controls.Add(this.okButton);
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.MinimumSize = new System.Drawing.Size(279, 166);
      this.Name = "ConfigurationDialog";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "ConfigurationDialog";
      this.TopMost = true;
      ((System.ComponentModel.ISupportInitialize)(this.JoystickData)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.AxisConfigurations)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button okButton;
    private System.Windows.Forms.Button cancelButton;
    private System.Windows.Forms.TextBox valueBoxPan;
    private System.Windows.Forms.Button applyButton;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox valueBoxTilt;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox valueBoxZoom;
    private System.Windows.Forms.Label label3;
    private System.Data.DataSet JoystickData;
    private System.Data.DataTable AxisConfigurations;
    private System.Data.DataColumn dataColumn1;
    private System.Data.DataColumn dataColumn2;
    private System.Data.DataColumn dataColumn3;
    private System.Data.DataColumn dataColumn4;
    private System.Data.DataColumn dataColumn5;
    private System.Data.DataColumn dataColumn6;
    private System.Data.DataColumn dataColumn7;
    private System.Windows.Forms.CheckBox invertCheckPan;
    private System.Windows.Forms.CheckBox invertCheckTilt;
    private System.Windows.Forms.CheckBox invertCheckZoom;
  }
}