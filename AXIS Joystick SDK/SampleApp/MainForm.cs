using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AxisJoystickModuleLib;

namespace SampleApp
{
  // This application demonstrates the useage of the AxisJoystickModule. You can view
  // a list of attached devices, activate and deactivate them separately and change
  // some settings. All events supported by the AxisJoystick are handled and displayed
  // in various manners.
  public partial class MainForm : Form
  {
    AxisJoystickHandlerClass myHandler = new AxisJoystickHandlerClass();
    System.Collections.Generic.List<AxisJoystick> myJoysticks = new List<AxisJoystick>();
    System.Collections.Generic.List<Label> myButtons = new List<Label>();
    System.Collections.Generic.List<Color> myJoystickColors = new List<Color>();
    System.Collections.Generic.List<Panel[]> myStatusPanels = new List<Panel[]>();
    Color colorLight;
    Color colorGrey;
    Color colorDark;
    int myNbrJoysticks;

    public MainForm()
    {
      InitializeComponent();
      colorLight = panelLeft.BackColor;
      colorDark = panelRing.BackColor;
      colorGrey = panelJoystick.BackColor;

      myJoystickColors.Add(Color.LightBlue);
      myJoystickColors.Add(Color.LightGreen);
      myJoystickColors.Add(Color.Pink); 
      myJoystickColors.Add(Color.LightYellow);      
    }

    private void Activate(AxisJoystick theJoystick)
    {
      theJoystick.PTZControlURL = urlBox.Text;
      theJoystick.Username = userBox.Text;
      theJoystick.Password = passBox.Text;
      theJoystick.Activate();
    }

    private void CreateListPanel(int nbrJoysticks)
    {
      listPanel.SuspendLayout();
      listPanel.Controls.Clear();
      myStatusPanels.Clear();

      int x = 0;
      int y = 0;
      int width = listPanel.Width;
      int height = 24;

      EventHandler aCheckHandler = new EventHandler(CheckBox_Click);
      EventHandler aJoystickLabelHandler = new EventHandler(JoystickLabel_Click);

      for (int i = 0; i < nbrJoysticks; i++)
      {
        Color aColor = myJoystickColors[(i % myJoystickColors.Count)];
        
        
        CheckBox aCheckBox = new CheckBox();
        aCheckBox.AutoSize = false;
        aCheckBox.Location = new Point(x, y);
        aCheckBox.Size = new Size(width - 80, height);
        aCheckBox.Text = myJoysticks[i].Name;
        aCheckBox.TextAlign = ContentAlignment.MiddleLeft;
        aCheckBox.Checked = (myJoysticks[i].Status & 4) != 0;
        aCheckBox.Tag = myJoysticks[i];
        aCheckBox.Click += aCheckHandler;
        aCheckBox.AutoEllipsis = true;
        aCheckBox.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
        aCheckBox.CheckAlign = ContentAlignment.MiddleLeft;
        aCheckBox.TabIndex = 6+i;
        string aText = myJoysticks[i].Name + "\n" + myJoysticks[i].Guid + "\nAxes: " 
            + myJoysticks[i].NbrAxes.ToString() +
            "\nButtons: " + myJoysticks[i].NbrButtons.ToString();
        toolTip.SetToolTip(aCheckBox, aText);
        listPanel.Controls.Add(aCheckBox);


        Label aLabel = new Label();
        aLabel.Location = new Point(x + width - 60, y);
        aLabel.Size = new Size(37, height);
        aLabel.ForeColor = Color.Blue;
        aLabel.Text = "conf";
        aLabel.TextAlign = ContentAlignment.MiddleLeft;
        aLabel.AutoEllipsis = false;
        aLabel.Anchor = AnchorStyles.Right | AnchorStyles.Top;
        aLabel.Tag = myJoysticks[i];
        aLabel.Cursor = Cursors.Hand;
        aLabel.Click += aJoystickLabelHandler;
        listPanel.Controls.Add(aLabel);


        Panel aFrame = new Panel();
        aFrame.Location = new Point(x + width - 22, y + height / 4 - 1);
        aFrame.Size = new Size(22, height / 2 + 2);
        aFrame.BackColor = colorGrey;
        aFrame.Anchor = AnchorStyles.Right | AnchorStyles.Top;
        listPanel.Controls.Add(aFrame);

        Panel[] somePanels = new Panel[3];
        Panel aPanel = new Panel();
        aPanel.Location = new Point(1, 1);
        aPanel.Size = new Size(6, height / 2);
        aPanel.BackColor = (myJoysticks[i].Status & 1) != 0 ? aColor : colorDark;
        aPanel.Anchor = AnchorStyles.Left | AnchorStyles.Top;
        somePanels[0] = aPanel;
        aFrame.Controls.Add(aPanel);

        aPanel = new Panel();
        aPanel.Location = new Point(8, 1);
        aPanel.Size = new Size(6, height/2);
        aPanel.BackColor = (myJoysticks[i].Status & 2) != 0 ? aColor : colorDark;
        aPanel.Anchor = AnchorStyles.Left | AnchorStyles.Top;
        somePanels[1] = aPanel;
        aFrame.Controls.Add(aPanel);

        aPanel = new Panel();
        aPanel.Location = new Point(15, 1);
        aPanel.Size = new Size(6, height / 2);
        aPanel.BackColor = (myJoysticks[i].Status & 4) != 0 ? aColor : colorDark;
        aPanel.Anchor = AnchorStyles.Left | AnchorStyles.Top;
        somePanels[2] = aPanel;
        aFrame.Controls.Add(aPanel);

        myStatusPanels.Add(somePanels);

        y += height;
      }

      listPanel.ResumeLayout();
    }

    void CheckBox_Click(object sender, EventArgs e)
    {
      CheckBox aBox = (CheckBox)sender;
      AxisJoystick aJoystick = (AxisJoystick) (aBox.Tag);

      if (aBox.Checked)
      {
        Activate(aJoystick);
      }
      else
      {
        aJoystick.DeActivate();
      }
    }

    private void enumerateButton_Click(object sender, EventArgs e)
    {
      int aNbrJoysticks = myHandler.EnumerateJoysticks();
      nbrLabel.Text = aNbrJoysticks.ToString() + " joysticks found.";

      if (aNbrJoysticks > myNbrJoysticks)
      {
        for (int i = myNbrJoysticks; i < aNbrJoysticks; i++)
        {
          myJoysticks.Add(myHandler.GetJoystick(i));
          myJoysticks[i].Id = i;
          myJoysticks[i].ButtonDown += new _IAxisJoystickEvents_ButtonDownEventHandler(MainForm_ButtonDown);
          myJoysticks[i].ButtonUp += new _IAxisJoystickEvents_ButtonUpEventHandler(MainForm_ButtonUp);
          myJoysticks[i].JoystickMove += new _IAxisJoystickEvents_JoystickMoveEventHandler(MainForm_JoystickMove);
          myJoysticks[i].StatusChange += new _IAxisJoystickEvents_StatusChangeEventHandler(MainForm_StatusChange);
          myJoysticks[i].Error += new _IAxisJoystickEvents_ErrorEventHandler(MainForm_Error);
        }
      }
      myNbrJoysticks = aNbrJoysticks;
      CreateListPanel(myNbrJoysticks);
      Text = "AxisJoystick Test";
      if (myNbrJoysticks > 0)
      {
        configureButton.Enabled = true;
      }
      else
      {
        configureButton.Enabled = false;
      }
    }

    void MainForm_Error(int joystickId, int errorCode, string errorInfo)
    {
      messageBox.AppendText("Error " + errorCode.ToString("X") + ": '" + errorInfo + "'\r\n");
    }

    void MainForm_StatusChange(int joystickId, int oldStatus, int newStatus)
    {
      Color aColor = myJoystickColors[joystickId % myJoystickColors.Count];
      string aString = "";
      aString += (newStatus & (int)JOYSTICK_STATUS.JOYSTICK_CONNECTED) != 0 ? "connected, " : "";
      aString += (newStatus & (int)JOYSTICK_STATUS.JOYSTICK_ACQUIRED) != 0 ? "acquired, " : "";
      aString += (newStatus & (int)JOYSTICK_STATUS.JOYSTICK_ACTIVE) != 0 ? "active" : "";
      aString.TrimEnd(' ', ',');
      if (joystickId < myStatusPanels.Count)
      {
        Panel[] somePanels = myStatusPanels[joystickId];
        somePanels[0].BackColor = (newStatus & (int)JOYSTICK_STATUS.JOYSTICK_CONNECTED) != 0 ?
          aColor : colorDark;
        somePanels[1].BackColor = (newStatus & (int)JOYSTICK_STATUS.JOYSTICK_ACQUIRED) != 0 ?
          aColor : colorDark;
        somePanels[2].BackColor = (newStatus & (int)JOYSTICK_STATUS.JOYSTICK_ACTIVE) != 0 ?
          aColor : colorDark;
        toolTip.SetToolTip(somePanels[0], aString);
        toolTip.SetToolTip(somePanels[1], aString);
        toolTip.SetToolTip(somePanels[2], aString);
      }
    }

    void MainForm_JoystickMove(int joystickId, int x, int y, int z)
    {
      int aDeadzone = 0;
      Color aColor = myJoystickColors[(joystickId % myJoystickColors.Count)];
      if (x < -aDeadzone)
      {
        panelLeft.BackColor = aColor;
        panelRight.BackColor = colorLight;
      }
      else if (x > aDeadzone)
      {
        panelLeft.BackColor = colorLight;
        panelRight.BackColor = aColor;
      }
      else
      {
        panelLeft.BackColor = colorLight;
        panelRight.BackColor = colorLight;
      }

      if (y < -aDeadzone)
      {
        panelUp.BackColor = aColor;
        panelDown.BackColor = colorLight;
      }
      else if (y > aDeadzone)
      {
        panelUp.BackColor = colorLight;
        panelDown.BackColor = aColor;
      }
      else
      {
        panelUp.BackColor = colorLight;
        panelDown.BackColor = colorLight;
      }

      if (z < -aDeadzone)
      {
        panelMiddle.BackColor = colorLight;
        panelRing.BackColor = aColor;
      }
      else if (z > aDeadzone)
      {
        panelMiddle.BackColor = aColor;
        panelRing.BackColor = colorDark;
      } 
      else
      {
        panelMiddle.BackColor = colorLight;
        panelRing.BackColor = colorDark;
      }
    }

    void MainForm_ButtonUp(int joystickId, int buttonIndex)
    {
      if (buttonIndex >= 0 && buttonIndex < myButtons.Count)
      {
        myButtons[buttonIndex].BackColor = colorLight;
      }

    }

    void MainForm_ButtonDown(int joystickId, int buttonIndex)
    {
      if (buttonIndex >= 0 && buttonIndex < myButtons.Count)
      {
        myButtons[buttonIndex].BackColor = myJoystickColors[(joystickId % myJoystickColors.Count)];
      }
    }

    private void focusCheck_CheckedChanged(object sender, EventArgs e)
    {
      foreach (AxisJoystick j in this.myJoysticks)
      {
        j.SetParent(Handle.ToInt64(), focusCheck.Checked);
      }
    }

    private void configureButton_Click(object sender, EventArgs e)
    {
      if (myJoysticks.Count > 0)
      {
        myJoysticks[0].RunControlPanel(this.Handle.ToInt64());
      }
    }

    private void JoystickLabel_Click(object sender, EventArgs e)
    {
      Label aLabel = (Label)sender;
      AxisJoystick aJoystick = (AxisJoystick)aLabel.Tag;
      ConfigurationDialog aDialog = new ConfigurationDialog(aJoystick);
      aDialog.ShowDialog(this);
    }

    private void MainForm_Load(object sender, EventArgs e)
    {
      myButtons.Add(button0);
      myButtons.Add(button1);
      myButtons.Add(button2);
      myButtons.Add(button3);
      myButtons.Add(button4);
      myButtons.Add(button5);
      myButtons.Add(button6);
      myButtons.Add(button7);
      myButtons.Add(button8);
      myButtons.Add(button9);
      myButtons.Add(button10);
      myButtons.Add(button11);
    }
  }
}