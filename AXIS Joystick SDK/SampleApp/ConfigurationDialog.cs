using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AxisJoystickModuleLib;

namespace SampleApp
{
  public partial class ConfigurationDialog : Form
  {
    AxisJoystick myJoystick = null;

    public ConfigurationDialog(AxisJoystick theJoystick)
    {
      myJoystick = theJoystick;
      Text = theJoystick.Name;

      InitializeComponent();
      GetConfiguration();
    }

    public ConfigurationDialog()
    {
      InitializeComponent();
    }

    private void valueBox_TextChanged(object sender, EventArgs e)
    {
      applyButton.Enabled = true;
    }

    private void invertCheck_CheckedChanged(object sender, EventArgs e)
    {
      applyButton.Enabled = true;
    }

    private void GetConfiguration()
    {
      int deadzone;
      int overlap;
      int threshold;
      float sensitivity;
      float curveCoeff;
      bool inverted;

      if (myJoystick == null)
      {
        return;
      }

      JOYSTICK_AXIS anAxis = JOYSTICK_AXIS.JOYSTICK_AXIS_X;
      myJoystick.GetConfiguration((int) anAxis, out deadzone, out overlap, out threshold,
        out sensitivity, out curveCoeff, out inverted);
      valueBoxPan.Text = sensitivity.ToString("N2");
      invertCheckPan.Checked = inverted;

      anAxis = JOYSTICK_AXIS.JOYSTICK_AXIS_Y;
      myJoystick.GetConfiguration((int) anAxis, out deadzone, out overlap, out threshold,
        out sensitivity, out curveCoeff, out inverted);
      valueBoxTilt.Text = sensitivity.ToString("N2");
      invertCheckTilt.Checked = inverted;

      anAxis = JOYSTICK_AXIS.JOYSTICK_AXIS_Z;
      myJoystick.GetConfiguration((int) anAxis, out deadzone, out overlap, out threshold,
        out sensitivity, out curveCoeff, out inverted);
      valueBoxZoom.Text = sensitivity.ToString("N2");
      invertCheckZoom.Checked = inverted;

      applyButton.Enabled = false;
    }

    private void SetConfiguration(int theAxis, float theSensitivity, bool isInverted)
    {
      int deadzone;
      int overlap;
      int threshold;
      float sensitivity;
      float curveCoeff;
      bool inverted;

      if (myJoystick == null)
      {
        return;
      }

      myJoystick.GetConfiguration(theAxis, out deadzone, out overlap, out threshold,
        out sensitivity, out curveCoeff, out inverted);
      myJoystick.SetConfiguration(theAxis, deadzone, overlap, threshold, 
        theSensitivity, curveCoeff, isInverted);
      applyButton.Enabled = false;
    }

    private void okButton_Click(object sender, EventArgs e)
    {
      SetConfiguration((int) JOYSTICK_AXIS.JOYSTICK_AXIS_X, Single.Parse(valueBoxPan.Text), invertCheckPan.Checked);
      SetConfiguration((int) JOYSTICK_AXIS.JOYSTICK_AXIS_Y, Single.Parse(valueBoxTilt.Text), invertCheckTilt.Checked);
      SetConfiguration((int) JOYSTICK_AXIS.JOYSTICK_AXIS_Z, Single.Parse(valueBoxZoom.Text), invertCheckZoom.Checked);
    }

    private void cancelButton_Click(object sender, EventArgs e)
    {

    }

    private void applyButton_Click(object sender, EventArgs e)
    {
      SetConfiguration((int)JOYSTICK_AXIS.JOYSTICK_AXIS_X, Single.Parse(valueBoxPan.Text), invertCheckPan.Checked);
      SetConfiguration((int)JOYSTICK_AXIS.JOYSTICK_AXIS_Y, Single.Parse(valueBoxTilt.Text), invertCheckTilt.Checked);
      SetConfiguration((int)JOYSTICK_AXIS.JOYSTICK_AXIS_Z, Single.Parse(valueBoxZoom.Text), invertCheckZoom.Checked);
    }


  }
}