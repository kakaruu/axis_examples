namespace SampleApp
{
  partial class MainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.enumerateButton = new System.Windows.Forms.Button();
      this.listPanel = new System.Windows.Forms.Panel();
      this.nbrLabel = new System.Windows.Forms.Label();
      this.toolTip = new System.Windows.Forms.ToolTip(this.components);
      this.urlBox = new System.Windows.Forms.TextBox();
      this.userBox = new System.Windows.Forms.TextBox();
      this.passBox = new System.Windows.Forms.TextBox();
      this.focusCheck = new System.Windows.Forms.CheckBox();
      this.configureButton = new System.Windows.Forms.Button();
      this.panelJoystick = new System.Windows.Forms.Panel();
      this.panelRing = new System.Windows.Forms.Panel();
      this.panelMiddle = new System.Windows.Forms.Panel();
      this.panelDown = new System.Windows.Forms.Panel();
      this.panelUp = new System.Windows.Forms.Panel();
      this.panelRight = new System.Windows.Forms.Panel();
      this.panelLeft = new System.Windows.Forms.Panel();
      this.button0 = new System.Windows.Forms.Label();
      this.button1 = new System.Windows.Forms.Label();
      this.button2 = new System.Windows.Forms.Label();
      this.button3 = new System.Windows.Forms.Label();
      this.button4 = new System.Windows.Forms.Label();
      this.button5 = new System.Windows.Forms.Label();
      this.button6 = new System.Windows.Forms.Label();
      this.button7 = new System.Windows.Forms.Label();
      this.button8 = new System.Windows.Forms.Label();
      this.button9 = new System.Windows.Forms.Label();
      this.button10 = new System.Windows.Forms.Label();
      this.button11 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.panel1 = new System.Windows.Forms.Panel();
      this.messageBox = new System.Windows.Forms.TextBox();
      this.panelJoystick.SuspendLayout();
      this.panelRing.SuspendLayout();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // enumerateButton
      // 
      this.enumerateButton.Location = new System.Drawing.Point(12, 12);
      this.enumerateButton.Name = "enumerateButton";
      this.enumerateButton.Size = new System.Drawing.Size(91, 23);
      this.enumerateButton.TabIndex = 0;
      this.enumerateButton.Text = "Enumerate";
      this.toolTip.SetToolTip(this.enumerateButton, "Press this button to register connected devices.");
      this.enumerateButton.UseVisualStyleBackColor = true;
      this.enumerateButton.Click += new System.EventHandler(this.enumerateButton_Click);
      // 
      // listPanel
      // 
      this.listPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.listPanel.Location = new System.Drawing.Point(12, 127);
      this.listPanel.Name = "listPanel";
      this.listPanel.Size = new System.Drawing.Size(397, 281);
      this.listPanel.TabIndex = 1;
      // 
      // nbrLabel
      // 
      this.nbrLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.nbrLabel.Location = new System.Drawing.Point(109, 12);
      this.nbrLabel.Name = "nbrLabel";
      this.nbrLabel.Size = new System.Drawing.Size(300, 23);
      this.nbrLabel.TabIndex = 2;
      this.nbrLabel.Text = "No joysticks enumerated.";
      this.nbrLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // toolTip
      // 
      this.toolTip.AutomaticDelay = 100;
      this.toolTip.AutoPopDelay = 100000;
      this.toolTip.InitialDelay = 500;
      this.toolTip.IsBalloon = true;
      this.toolTip.ReshowDelay = 20;
      // 
      // urlBox
      // 
      this.urlBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.urlBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
      this.urlBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.AllUrl;
      this.urlBox.Location = new System.Drawing.Point(112, 41);
      this.urlBox.Name = "urlBox";
      this.urlBox.Size = new System.Drawing.Size(297, 20);
      this.urlBox.TabIndex = 1;
      this.urlBox.Text = "http://<ip-address>/axis-cgi/com/ptz.cgi";
      this.toolTip.SetToolTip(this.urlBox, "The URL of the ptz.cgi on the camera that will receive the commands.");
      // 
      // userBox
      // 
      this.userBox.Location = new System.Drawing.Point(112, 67);
      this.userBox.Name = "userBox";
      this.userBox.Size = new System.Drawing.Size(144, 20);
      this.userBox.TabIndex = 2;
      this.toolTip.SetToolTip(this.userBox, "Username used when sending commands to the camera.");
      // 
      // passBox
      // 
      this.passBox.Location = new System.Drawing.Point(112, 93);
      this.passBox.Name = "passBox";
      this.passBox.PasswordChar = '*';
      this.passBox.Size = new System.Drawing.Size(144, 20);
      this.passBox.TabIndex = 3;
      this.toolTip.SetToolTip(this.passBox, "Password used when sending commands to the camera.");
      // 
      // focusCheck
      // 
      this.focusCheck.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.focusCheck.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
      this.focusCheck.Location = new System.Drawing.Point(308, 68);
      this.focusCheck.Name = "focusCheck";
      this.focusCheck.Size = new System.Drawing.Size(101, 20);
      this.focusCheck.TabIndex = 4;
      this.focusCheck.Text = "Focus Required";
      this.toolTip.SetToolTip(this.focusCheck, "Check this box to only send ptz commands to the camera when the application windo" +
              "w has focus.");
      this.focusCheck.UseVisualStyleBackColor = true;
      this.focusCheck.CheckedChanged += new System.EventHandler(this.focusCheck_CheckedChanged);
      // 
      // configureButton
      // 
      this.configureButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
      this.configureButton.BackColor = System.Drawing.SystemColors.Control;
      this.configureButton.Enabled = false;
      this.configureButton.Location = new System.Drawing.Point(308, 90);
      this.configureButton.Name = "configureButton";
      this.configureButton.Size = new System.Drawing.Size(101, 23);
      this.configureButton.TabIndex = 5;
      this.configureButton.Text = "Calibrate";
      this.toolTip.SetToolTip(this.configureButton, "This button will open the windows joystick calibration dialog.");
      this.configureButton.UseVisualStyleBackColor = false;
      this.configureButton.Click += new System.EventHandler(this.configureButton_Click);
      // 
      // panelJoystick
      // 
      this.panelJoystick.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.panelJoystick.BackColor = System.Drawing.SystemColors.ControlDark;
      this.panelJoystick.Controls.Add(this.panelRing);
      this.panelJoystick.Controls.Add(this.panelDown);
      this.panelJoystick.Controls.Add(this.panelUp);
      this.panelJoystick.Controls.Add(this.panelRight);
      this.panelJoystick.Controls.Add(this.panelLeft);
      this.panelJoystick.Location = new System.Drawing.Point(12, 414);
      this.panelJoystick.Name = "panelJoystick";
      this.panelJoystick.Size = new System.Drawing.Size(75, 75);
      this.panelJoystick.TabIndex = 7;
      this.toolTip.SetToolTip(this.panelJoystick, "Joystick movement");
      // 
      // panelRing
      // 
      this.panelRing.BackColor = System.Drawing.SystemColors.ControlDarkDark;
      this.panelRing.Controls.Add(this.panelMiddle);
      this.panelRing.Location = new System.Drawing.Point(19, 19);
      this.panelRing.Name = "panelRing";
      this.panelRing.Size = new System.Drawing.Size(36, 36);
      this.panelRing.TabIndex = 9;
      this.toolTip.SetToolTip(this.panelRing, "Indicates zoom out");
      // 
      // panelMiddle
      // 
      this.panelMiddle.BackColor = System.Drawing.SystemColors.ControlLight;
      this.panelMiddle.Location = new System.Drawing.Point(11, 11);
      this.panelMiddle.Name = "panelMiddle";
      this.panelMiddle.Size = new System.Drawing.Size(15, 15);
      this.panelMiddle.TabIndex = 8;
      this.toolTip.SetToolTip(this.panelMiddle, "Indicates zoom in");
      // 
      // panelDown
      // 
      this.panelDown.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.panelDown.BackColor = System.Drawing.SystemColors.ControlLight;
      this.panelDown.Location = new System.Drawing.Point(8, 67);
      this.panelDown.Name = "panelDown";
      this.panelDown.Size = new System.Drawing.Size(59, 7);
      this.panelDown.TabIndex = 1;
      this.toolTip.SetToolTip(this.panelDown, "Joystick movement");
      // 
      // panelUp
      // 
      this.panelUp.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.panelUp.BackColor = System.Drawing.SystemColors.ControlLight;
      this.panelUp.Location = new System.Drawing.Point(8, 1);
      this.panelUp.Name = "panelUp";
      this.panelUp.Size = new System.Drawing.Size(59, 7);
      this.panelUp.TabIndex = 1;
      this.toolTip.SetToolTip(this.panelUp, "Joystick movement");
      // 
      // panelRight
      // 
      this.panelRight.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.panelRight.BackColor = System.Drawing.SystemColors.ControlLight;
      this.panelRight.Location = new System.Drawing.Point(67, 8);
      this.panelRight.Name = "panelRight";
      this.panelRight.Size = new System.Drawing.Size(7, 59);
      this.panelRight.TabIndex = 0;
      this.toolTip.SetToolTip(this.panelRight, "Joystick movement");
      // 
      // panelLeft
      // 
      this.panelLeft.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                  | System.Windows.Forms.AnchorStyles.Left)));
      this.panelLeft.BackColor = System.Drawing.SystemColors.ControlLight;
      this.panelLeft.Location = new System.Drawing.Point(1, 8);
      this.panelLeft.Name = "panelLeft";
      this.panelLeft.Size = new System.Drawing.Size(7, 59);
      this.panelLeft.TabIndex = 0;
      this.toolTip.SetToolTip(this.panelLeft, "Joystick movement");
      // 
      // button0
      // 
      this.button0.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.button0.BackColor = System.Drawing.SystemColors.ControlLight;
      this.button0.ForeColor = System.Drawing.SystemColors.ControlText;
      this.button0.Location = new System.Drawing.Point(4, 1);
      this.button0.Name = "button0";
      this.button0.Size = new System.Drawing.Size(22, 22);
      this.button0.TabIndex = 8;
      this.button0.Text = "0";
      this.button0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.toolTip.SetToolTip(this.button0, "Joystick button");
      // 
      // button1
      // 
      this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.button1.BackColor = System.Drawing.SystemColors.ControlLight;
      this.button1.ForeColor = System.Drawing.SystemColors.ControlText;
      this.button1.Location = new System.Drawing.Point(30, 1);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(22, 22);
      this.button1.TabIndex = 8;
      this.button1.Text = "1";
      this.button1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.toolTip.SetToolTip(this.button1, "Joystick button");
      // 
      // button2
      // 
      this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.button2.BackColor = System.Drawing.SystemColors.ControlLight;
      this.button2.ForeColor = System.Drawing.SystemColors.ControlText;
      this.button2.Location = new System.Drawing.Point(56, 1);
      this.button2.Name = "button2";
      this.button2.Size = new System.Drawing.Size(22, 22);
      this.button2.TabIndex = 8;
      this.button2.Text = "2";
      this.button2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.toolTip.SetToolTip(this.button2, "Joystick button");
      // 
      // button3
      // 
      this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.button3.BackColor = System.Drawing.SystemColors.ControlLight;
      this.button3.ForeColor = System.Drawing.SystemColors.ControlText;
      this.button3.Location = new System.Drawing.Point(82, 1);
      this.button3.Name = "button3";
      this.button3.Size = new System.Drawing.Size(22, 22);
      this.button3.TabIndex = 8;
      this.button3.Text = "3";
      this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.toolTip.SetToolTip(this.button3, "Joystick button");
      // 
      // button4
      // 
      this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.button4.BackColor = System.Drawing.SystemColors.ControlLight;
      this.button4.ForeColor = System.Drawing.SystemColors.ControlText;
      this.button4.Location = new System.Drawing.Point(108, 1);
      this.button4.Name = "button4";
      this.button4.Size = new System.Drawing.Size(22, 22);
      this.button4.TabIndex = 8;
      this.button4.Text = "4";
      this.button4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.toolTip.SetToolTip(this.button4, "Joystick button");
      // 
      // button5
      // 
      this.button5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.button5.BackColor = System.Drawing.SystemColors.ControlLight;
      this.button5.ForeColor = System.Drawing.SystemColors.ControlText;
      this.button5.Location = new System.Drawing.Point(134, 1);
      this.button5.Name = "button5";
      this.button5.Size = new System.Drawing.Size(22, 22);
      this.button5.TabIndex = 8;
      this.button5.Text = "5";
      this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.toolTip.SetToolTip(this.button5, "Joystick button");
      // 
      // button6
      // 
      this.button6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.button6.BackColor = System.Drawing.SystemColors.ControlLight;
      this.button6.ForeColor = System.Drawing.SystemColors.ControlText;
      this.button6.Location = new System.Drawing.Point(160, 1);
      this.button6.Name = "button6";
      this.button6.Size = new System.Drawing.Size(22, 22);
      this.button6.TabIndex = 8;
      this.button6.Text = "6";
      this.button6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.toolTip.SetToolTip(this.button6, "Joystick button");
      // 
      // button7
      // 
      this.button7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.button7.BackColor = System.Drawing.SystemColors.ControlLight;
      this.button7.ForeColor = System.Drawing.SystemColors.ControlText;
      this.button7.Location = new System.Drawing.Point(186, 1);
      this.button7.Name = "button7";
      this.button7.Size = new System.Drawing.Size(22, 22);
      this.button7.TabIndex = 8;
      this.button7.Text = "7";
      this.button7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.toolTip.SetToolTip(this.button7, "Joystick button");
      // 
      // button8
      // 
      this.button8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.button8.BackColor = System.Drawing.SystemColors.ControlLight;
      this.button8.ForeColor = System.Drawing.SystemColors.ControlText;
      this.button8.Location = new System.Drawing.Point(212, 1);
      this.button8.Name = "button8";
      this.button8.Size = new System.Drawing.Size(22, 22);
      this.button8.TabIndex = 8;
      this.button8.Text = "8";
      this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.toolTip.SetToolTip(this.button8, "Joystick button");
      // 
      // button9
      // 
      this.button9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.button9.BackColor = System.Drawing.SystemColors.ControlLight;
      this.button9.ForeColor = System.Drawing.SystemColors.ControlText;
      this.button9.Location = new System.Drawing.Point(238, 1);
      this.button9.Name = "button9";
      this.button9.Size = new System.Drawing.Size(22, 22);
      this.button9.TabIndex = 8;
      this.button9.Text = "9";
      this.button9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.toolTip.SetToolTip(this.button9, "Joystick button");
      // 
      // button10
      // 
      this.button10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.button10.BackColor = System.Drawing.SystemColors.ControlLight;
      this.button10.ForeColor = System.Drawing.SystemColors.ControlText;
      this.button10.Location = new System.Drawing.Point(264, 1);
      this.button10.Name = "button10";
      this.button10.Size = new System.Drawing.Size(22, 22);
      this.button10.TabIndex = 8;
      this.button10.Text = "10";
      this.button10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.toolTip.SetToolTip(this.button10, "Joystick button");
      // 
      // button11
      // 
      this.button11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.button11.BackColor = System.Drawing.SystemColors.ControlLight;
      this.button11.ForeColor = System.Drawing.SystemColors.ControlText;
      this.button11.Location = new System.Drawing.Point(290, 1);
      this.button11.Name = "button11";
      this.button11.Size = new System.Drawing.Size(22, 22);
      this.button11.TabIndex = 8;
      this.button11.Text = "11";
      this.button11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.toolTip.SetToolTip(this.button11, "Joystick button");
      // 
      // label1
      // 
      this.label1.Location = new System.Drawing.Point(12, 41);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(94, 20);
      this.label1.TabIndex = 2;
      this.label1.Text = "PTZControlURL";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // label2
      // 
      this.label2.Location = new System.Drawing.Point(12, 67);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(94, 20);
      this.label2.TabIndex = 2;
      this.label2.Text = "Username";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // label3
      // 
      this.label3.Location = new System.Drawing.Point(12, 93);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(94, 20);
      this.label3.TabIndex = 2;
      this.label3.Text = "Password:";
      this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // panel1
      // 
      this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.panel1.BackColor = System.Drawing.SystemColors.ButtonShadow;
      this.panel1.Controls.Add(this.button8);
      this.panel1.Controls.Add(this.button11);
      this.panel1.Controls.Add(this.button0);
      this.panel1.Controls.Add(this.button10);
      this.panel1.Controls.Add(this.button1);
      this.panel1.Controls.Add(this.button9);
      this.panel1.Controls.Add(this.button2);
      this.panel1.Controls.Add(this.button3);
      this.panel1.Controls.Add(this.button7);
      this.panel1.Controls.Add(this.button4);
      this.panel1.Controls.Add(this.button6);
      this.panel1.Controls.Add(this.button5);
      this.panel1.Location = new System.Drawing.Point(93, 414);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(316, 24);
      this.panel1.TabIndex = 9;
      // 
      // messageBox
      // 
      this.messageBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                  | System.Windows.Forms.AnchorStyles.Right)));
      this.messageBox.BackColor = System.Drawing.SystemColors.Info;
      this.messageBox.Cursor = System.Windows.Forms.Cursors.Default;
      this.messageBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.messageBox.ForeColor = System.Drawing.Color.DarkRed;
      this.messageBox.Location = new System.Drawing.Point(93, 444);
      this.messageBox.Multiline = true;
      this.messageBox.Name = "messageBox";
      this.messageBox.ReadOnly = true;
      this.messageBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.messageBox.Size = new System.Drawing.Size(316, 45);
      this.messageBox.TabIndex = 10;
      // 
      // MainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(421, 498);
      this.Controls.Add(this.messageBox);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.panelJoystick);
      this.Controls.Add(this.configureButton);
      this.Controls.Add(this.focusCheck);
      this.Controls.Add(this.passBox);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.userBox);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.urlBox);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.nbrLabel);
      this.Controls.Add(this.listPanel);
      this.Controls.Add(this.enumerateButton);
      this.MinimumSize = new System.Drawing.Size(429, 400);
      this.Name = "MainForm";
      this.Text = "AxisJoystick Test";
      this.Load += new System.EventHandler(this.MainForm_Load);
      this.panelJoystick.ResumeLayout(false);
      this.panelRing.ResumeLayout(false);
      this.panel1.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button enumerateButton;
    private System.Windows.Forms.Panel listPanel;
    private System.Windows.Forms.Label nbrLabel;
    private System.Windows.Forms.ToolTip toolTip;
    private System.Windows.Forms.TextBox urlBox;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox userBox;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox passBox;
    private System.Windows.Forms.CheckBox focusCheck;
    private System.Windows.Forms.Button configureButton;
    private System.Windows.Forms.Panel panelJoystick;
    private System.Windows.Forms.Panel panelRight;
    private System.Windows.Forms.Panel panelLeft;
    private System.Windows.Forms.Panel panelDown;
    private System.Windows.Forms.Panel panelUp;
    private System.Windows.Forms.Panel panelMiddle;
    private System.Windows.Forms.Panel panelRing;
    private System.Windows.Forms.Label button0;
    private System.Windows.Forms.Label button1;
    private System.Windows.Forms.Label button2;
    private System.Windows.Forms.Label button3;
    private System.Windows.Forms.Label button4;
    private System.Windows.Forms.Label button5;
    private System.Windows.Forms.Label button6;
    private System.Windows.Forms.Label button7;
    private System.Windows.Forms.Label button8;
    private System.Windows.Forms.Label button9;
    private System.Windows.Forms.Label button10;
    private System.Windows.Forms.Label button11;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.TextBox messageBox;
  }
}

