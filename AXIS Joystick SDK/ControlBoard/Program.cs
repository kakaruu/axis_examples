using System;
using System.Text;
using AxisJoystickModuleLib;

namespace ControlBoardApp
{
  class Program
  {
      private static AxisJogDialHandlerClass aJogDialHandler = new AxisJogDialHandlerClass();
      private static AxisKeyPadHandlerClass aKeyPadHandler = new AxisKeyPadHandlerClass();
      // This simple program does the minimum required to control an Axis PTZ camera
    // using a joystick. It is assumed that no username or password is required.
    // (It would require the Username and Password properties of the AxisJoystick
    // to be set.)
    static void Main(string[] args)
    {
        AxisJogDial aJogDial = null;

      int aNbrJogDials = aJogDialHandler.EnumerateJogDials();
      if (aNbrJogDials > 0)
      {
        aJogDial = aJogDialHandler.GetJogDial(0);
        if (aJogDial != null)
        {
            aJogDial.Activate();

            aJogDial.ButtonDown  += AxisJogDial_ButtonDown;
            aJogDial.ButtonUp    += AxisJogDial_ButtonUp;
            aJogDial.JogMove     += AxisJogDial_JogMove;
            aJogDial.ShuttleMove += AxisJogDial_ShuttleMove;        
        }
      }

      AxisKeyPad aKeyPad = null;

      int aNbrKeyPads = aKeyPadHandler.EnumerateKeyPads();
      if (aNbrKeyPads > 0)
      {
          aKeyPad = aKeyPadHandler.GetKeyPad(0);
        if (aKeyPad != null)
        {
          aKeyPad.Activate();

          aKeyPad.ButtonDown += AxisKeyPad_ButtonDown;
          aKeyPad.ButtonUp += AxisKeyPad_ButtonUp;

          
          }
      }


      System.Console.WriteLine("Press <RETURN> to exit");
      System.Console.ReadLine();



      if (aNbrJogDials > 0 && aJogDial != null)
      {
        aJogDial.ButtonDown  -= AxisJogDial_ButtonDown;
        aJogDial.ButtonUp    -= AxisJogDial_ButtonUp;
        aJogDial.JogMove     -= AxisJogDial_JogMove;
        aJogDial.ShuttleMove -= AxisJogDial_ShuttleMove;

        aJogDial.DeActivate();
      }

      if (aNbrKeyPads > 0 && aKeyPad != null)
      {
        aKeyPad.ButtonDown -= AxisKeyPad_ButtonDown;
        aKeyPad.ButtonUp -= AxisKeyPad_ButtonUp;

        aKeyPad.DeActivate();
      }
        
    }

    private static void AxisJogDial_ButtonDown(int joystickId, int buttonIndex)
    {
      System.Console.WriteLine(joystickId + ":JogDial Button " + buttonIndex + " DOWN");
    }

    private static void AxisJogDial_ButtonUp(int joystickId, int buttonIndex)
    {
      System.Console.WriteLine(joystickId + ":JogDial Button " + buttonIndex + " UP");
    }
  
    private static void AxisJogDial_JogMove(int joystickId, int moveValue)
    {
      System.Console.WriteLine("MOVE Jog " + joystickId + ": MOVE " + moveValue + " Value");
    }

    private static void AxisJogDial_ShuttleMove(int joystickId, int moveValue)
    {
      System.Console.WriteLine("MOVE Shuttle " + joystickId + ": MOVE " + moveValue + " Value");
    }

    private static void AxisKeyPad_ButtonDown(int joystickId, int buttonIndex)
    {
      System.Console.WriteLine(joystickId + ":KeyPad Button " + buttonIndex + " DOWN");
    }

    private static void AxisKeyPad_ButtonUp(int joystickId, int buttonIndex)
    {
      System.Console.WriteLine(joystickId + ":KeyPad Button " + buttonIndex + " UP");
    }

  }
}
