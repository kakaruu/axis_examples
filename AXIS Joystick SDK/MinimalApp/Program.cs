using System;
using System.Text;
using AxisJoystickModuleLib;

namespace MinimalApp
{
  class Program
  {
    // This simple program does the minimum required to control an Axis PTZ camera
    // using a joystick. It is assumed that no username or password is required.
    // (It would require the Username and Password properties of the AxisJoystick
    // to be set.)
    static void Main(string[] args)
    {
      // Create a AxisJoystickHandler and register all attached joysticks.
      AxisJoystickHandler aHandler = new AxisJoystickHandler();
      int aNbrJoysticks = aHandler.EnumerateJoysticks();
      if (aNbrJoysticks > 0)
      {
        // Get the first found joystick and set the control URL based on
        // the ip of the camera.
        AxisJoystick aJoystick = aHandler.GetJoystick(0);
        
        System.Console.Write("Enter ip address of camera: ");
        aJoystick.PTZControlURL = "http://" + System.Console.ReadLine() + "/axis-cgi/com/ptz.cgi";

        // Activate the joystick so that it will start sending commands to
        // the camera.
        aJoystick.Activate();
        System.Console.WriteLine("Activating joystick. Press <RETURN> to exit");
        System.Console.ReadLine();

        // Make sure camera is stopped before exiting.
        aJoystick.DeActivate();
      }
      else
      {
        System.Console.WriteLine("No joysticks attached. Please attach a joystick and restart the application.");
        System.Console.ReadLine();
      }
    }
  }
}
