// VideoRenderer.cpp : implementation file
//

#include "stdafx.h"
#include "VideoRenderer.h"

#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

CVideoRenderer::CVideoRenderer()
  : m_window(NULL)
  , m_pixelShader(NULL)
  , m_pD3D9(NULL)
  , m_videoFormat(D3DFMT_UNKNOWN)
  , m_videoPitch(0)
  , m_outputWidth(0)
  , m_outputHeight(0)
  , m_composition(single)
{
  for (int i = 0; i < 4; i++)
  {
    m_quadPixelShaders[i] = NULL;
  }
}

CVideoRenderer::~CVideoRenderer()
{
  UnloadDirect3D();
}

void CVideoRenderer::SetTargetWindow(HWND theWindow)
{
  AutoLock lock(m_mutex);
  UnloadDirect3D();
  m_window = theWindow;
}

void CVideoRenderer::SetOutputSize(DWORD theWidth, DWORD theHeight)
{
  AutoLock lock(m_mutex);
  UnloadDirect3D();
  m_outputWidth = theWidth;
  m_outputHeight = theHeight;
}

void CVideoRenderer::SetComposition(Composition theComposition)
{
  m_composition = theComposition;
}

HRESULT CVideoRenderer::SetPixelShader(IShader* thePixelShader)
{
  AutoLock lock(m_mutex);
  m_pixelShader = thePixelShader;

  if(m_pixelShader != NULL && m_pDevice != NULL)
  {
    HR(m_pixelShader->Load(m_pDevice));
  }

  return S_OK;
}

HRESULT CVideoRenderer::SetQuadPixelShader(IShader* thePixelShader, int theIndex)
{
  if(0 <= theIndex && theIndex < 4)
  {
    m_quadPixelShaders[theIndex] = thePixelShader;
    if(thePixelShader != NULL && m_pDevice != NULL)
    {
      HR(m_quadPixelShaders[theIndex]->Load(m_pDevice));
    }

    return S_OK;
  }

  return E_INVALIDARG;
}

HRESULT CVideoRenderer::RenderVideoSample(BYTE* theVideoData, DWORD theDataSize,
  DWORD theWidth, DWORD theHeight, D3DFORMAT theFormat, UINT64 theTimestamp)
{
  // TODO: Scheduling frames for rendering (not required when using AMV)

  AutoLock lock(m_mutex);

  if(D3DFMT_YUY2 != theFormat &&
     D3DFMT_A8R8G8B8 != theFormat)
  {
    return E_FAIL;
  }

  if(m_pD3D9 == NULL ||
     theFormat != m_videoFormat ||
     theWidth != m_videoWidth ||
     theHeight != m_videoHeight ||
     theDataSize / theHeight != m_videoPitch)
  {
    // we need to (re)initialize Direct3D
    m_videoFormat = theFormat;
    m_videoWidth = theWidth;
    m_videoHeight = theHeight;
    m_videoPitch = theDataSize / theHeight;
    
    HR(InitDirect3D());
  }
  
  HR(m_pDevice->TestCooperativeLevel());

  HR(CopyVideoDataToOffscreenSurface(theVideoData, theFormat));
  HR(m_pDevice->ColorFill(m_pTextureSurface, NULL, D3DCOLOR_ARGB(0xFF, 0, 0x0, 0)));
  HR(m_pDevice->StretchRect(m_pOffscreenSurface, NULL, m_pTextureSurface, NULL, D3DTEXF_LINEAR));
  HR(CreateScene());

  if(m_window != NULL)
  {
    HRESULT hr;
    if(FAILED(hr = m_pDevice->Present(NULL, NULL, NULL, NULL)))
    {
      //_ASSERT(false);
      return S_FALSE;
    }
  }

  return S_OK;
}

HRESULT CVideoRenderer::GetOutputImage(BYTE* theBuffer, DWORD* theWidth,
  DWORD* theHeight, DWORD* theStride)
{
  CComPtr<IDirect3DSurface9> pTargetSurface;	
  CComPtr<IDirect3DSurface9> pTempSurface;

  AutoLock lock(m_mutex);
  
  if(m_pD3D9 == NULL)
  {
    return E_FAIL;
  }

  HR(m_pDevice->GetRenderTarget(0, &pTargetSurface));	

  D3DSURFACE_DESC desc;		
  HR(pTargetSurface->GetDesc(&desc));	

  if(!theBuffer)
  {
    *theWidth = desc.Width;
    *theHeight = desc.Height;
    *theStride = desc.Width * 4; // Always ARGB32
    return S_OK;
  }

  HR(m_pDevice->CreateOffscreenPlainSurface(desc.Width, desc.Height, desc.Format, D3DPOOL_SYSTEMMEM, &pTempSurface, NULL));				
  HR(m_pDevice->GetRenderTargetData(pTargetSurface, pTempSurface));					

  D3DLOCKED_RECT d3drect;
  HR(pTempSurface->LockRect(&d3drect, NULL, D3DLOCK_NO_DIRTY_UPDATE | D3DLOCK_NOSYSLOCK | D3DLOCK_READONLY));		

  BYTE* pFrame = (BYTE*)d3drect.pBits;
  BYTE* pBuf = theBuffer;

  memcpy(pBuf, pFrame, desc.Height * d3drect.Pitch);

  return pTempSurface->UnlockRect();
}

HRESULT CVideoRenderer::InitDirect3D()
{
  UnloadDirect3D();

  m_pD3D9 = ::Direct3DCreate9( D3D_SDK_VERSION );
  if(!m_pD3D9)
  {
    return E_FAIL;
  }

  HR(m_pD3D9->GetAdapterDisplayMode(D3DADAPTER_DEFAULT, &m_displayMode));

  D3DCAPS9 deviceCaps;
  HR(m_pD3D9->GetDeviceCaps(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &deviceCaps));

  DWORD dwBehaviorFlags = D3DCREATE_MULTITHREADED;

  if(deviceCaps.VertexProcessingCaps != 0)
  {
    dwBehaviorFlags |= D3DCREATE_HARDWARE_VERTEXPROCESSING;
  }
  else
  {
    dwBehaviorFlags |= D3DCREATE_SOFTWARE_VERTEXPROCESSING;
  }

  D3DPRESENT_PARAMETERS presentParams = {0};
  presentParams.Flags                  = D3DPRESENTFLAG_VIDEO;
  presentParams.Windowed               = TRUE;
  presentParams.hDeviceWindow          = m_window;
  presentParams.BackBufferWidth        = m_outputWidth;
  presentParams.BackBufferHeight       = m_outputHeight;
  presentParams.SwapEffect             = D3DSWAPEFFECT_COPY;
  presentParams.MultiSampleType        = D3DMULTISAMPLE_NONE;
  presentParams.PresentationInterval   = D3DPRESENT_INTERVAL_DEFAULT;
  presentParams.BackBufferFormat       = m_displayMode.Format;
  presentParams.BackBufferCount        = 1;
  presentParams.EnableAutoDepthStencil = FALSE;

  HR(m_pD3D9->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, m_window, dwBehaviorFlags, &presentParams, &m_pDevice));

  HR(m_pDevice->SetRenderState( D3DRS_LIGHTING, FALSE));

  // enable output interpolation
  HR(m_pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR /*D3DTEXF_ANISOTROPIC*/));
  HR(m_pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_ANISOTROPIC));

  // create render target (texture)
  HR(m_pDevice->CreateTexture(m_videoWidth, m_videoHeight, 1, D3DUSAGE_RENDERTARGET, m_displayMode.Format, D3DPOOL_DEFAULT, &m_pTexture, NULL));
  HR(m_pTexture->GetSurfaceLevel(0, &m_pTextureSurface));

  VERTEX vertexArray[] =
  {
    // Singel view
    { D3DXVECTOR3(0, 0, 0),          D3DCOLOR_ARGB(255, 255, 255, 255), D3DXVECTOR2(0, 0) },  // top left
    { D3DXVECTOR3((FLOAT)m_videoWidth, 0, 0),      D3DCOLOR_ARGB(255, 255, 255, 255), D3DXVECTOR2(1, 0) },  // top right
    { D3DXVECTOR3((FLOAT)m_videoWidth, (FLOAT)m_videoHeight, 0), D3DCOLOR_ARGB(255, 255, 255, 255), D3DXVECTOR2(1, 1) },  // bottom right
    { D3DXVECTOR3(0, (FLOAT)m_videoHeight, 0),     D3DCOLOR_ARGB(255, 255, 255, 255), D3DXVECTOR2(0, 1) },  // bottom left

    // Quad views
    { D3DXVECTOR3(0, 0, 0),          D3DCOLOR_ARGB(255, 255, 255, 255), D3DXVECTOR2(0, 0) },  // top left
    { D3DXVECTOR3(m_videoWidth/2.0f, 0, 0),      D3DCOLOR_ARGB(255, 255, 255, 255), D3DXVECTOR2(1, 0) },  // top right
    { D3DXVECTOR3(m_videoWidth/2.0f, m_videoHeight/2.0f, 0), D3DCOLOR_ARGB(255, 255, 255, 255), D3DXVECTOR2(1, 1) },  // bottom right
    { D3DXVECTOR3(0, m_videoHeight/2.0f, 0),     D3DCOLOR_ARGB(255, 255, 255, 255), D3DXVECTOR2(0, 1) },  // bottom left

    { D3DXVECTOR3(m_videoWidth/2.0f, 0, 0),          D3DCOLOR_ARGB(255, 255, 255, 255), D3DXVECTOR2(0, 0) },  // top left
    { D3DXVECTOR3((FLOAT)m_videoWidth, 0, 0),      D3DCOLOR_ARGB(255, 255, 255, 255), D3DXVECTOR2(1, 0) },  // top right
    { D3DXVECTOR3((FLOAT)m_videoWidth, m_videoHeight/2.0f, 0), D3DCOLOR_ARGB(255, 255, 255, 255), D3DXVECTOR2(1, 1) },  // bottom right
    { D3DXVECTOR3(m_videoWidth/2.0f, m_videoHeight/2.0f, 0),     D3DCOLOR_ARGB(255, 255, 255, 255), D3DXVECTOR2(0, 1) },  // bottom left

    { D3DXVECTOR3(0, m_videoHeight/2.0f, 0),          D3DCOLOR_ARGB(255, 255, 255, 255), D3DXVECTOR2(0, 0) },  // top left
    { D3DXVECTOR3(m_videoWidth/2.0f, m_videoHeight/2.0f, 0),      D3DCOLOR_ARGB(255, 255, 255, 255), D3DXVECTOR2(1, 0) },  // top right
    { D3DXVECTOR3(m_videoWidth/2.0f, (FLOAT)m_videoHeight, 0), D3DCOLOR_ARGB(255, 255, 255, 255), D3DXVECTOR2(1, 1) },  // bottom right
    { D3DXVECTOR3(0, (FLOAT)m_videoHeight, 0),     D3DCOLOR_ARGB(255, 255, 255, 255), D3DXVECTOR2(0, 1) },  // bottom left

    { D3DXVECTOR3(m_videoWidth/2.0f, m_videoHeight/2.0f, 0),          D3DCOLOR_ARGB(255, 255, 255, 255), D3DXVECTOR2(0, 0) },  // top left
    { D3DXVECTOR3((FLOAT)m_videoWidth, m_videoHeight/2.0f, 0),      D3DCOLOR_ARGB(255, 255, 255, 255), D3DXVECTOR2(1, 0) },  // top right
    { D3DXVECTOR3((FLOAT)m_videoWidth, (FLOAT)m_videoHeight, 0), D3DCOLOR_ARGB(255, 255, 255, 255), D3DXVECTOR2(1, 1) },  // bottom right
    { D3DXVECTOR3(m_videoWidth/2.0f, (FLOAT)m_videoHeight, 0),     D3DCOLOR_ARGB(255, 255, 255, 255), D3DXVECTOR2(0, 1) },  // bottom left
  };

  // create rectangle that will receive texture
  HR(m_pDevice->CreateVertexBuffer(sizeof(vertexArray), D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY, D3DFVF_CUSTOMVERTEX, D3DPOOL_DEFAULT, &m_pVertexBuffer, NULL));

  VERTEX *vertices;
  HR(m_pVertexBuffer->Lock(0, 0, (void**)&vertices, D3DLOCK_DISCARD));

  memcpy(vertices, vertexArray, sizeof(vertexArray));

  HR(m_pVertexBuffer->Unlock());

  // setup transform matrices
  D3DXMATRIX matOrtho; 
  D3DXMATRIX matIdentity;

  D3DXMatrixOrthoOffCenterLH(&matOrtho, 0, (FLOAT)m_videoWidth, (FLOAT)m_videoHeight, 0, 0.0f, 1.0f);
  D3DXMatrixIdentity(&matIdentity);

  HR(m_pDevice->SetTransform(D3DTS_PROJECTION, &matOrtho));
  HR(m_pDevice->SetTransform(D3DTS_WORLD, &matIdentity));
  HR(m_pDevice->SetTransform(D3DTS_VIEW, &matIdentity));

  HR(m_pD3D9->CheckDeviceFormat(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, m_displayMode.Format, 0, D3DRTYPE_SURFACE, m_videoFormat));
  HR(m_pD3D9->CheckDeviceFormatConversion(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, m_videoFormat, m_displayMode.Format));

  HR(m_pDevice->CreateOffscreenPlainSurface(m_videoWidth, m_videoHeight, m_videoFormat , D3DPOOL_DEFAULT, &m_pOffscreenSurface, NULL));
  HR(m_pDevice->ColorFill(m_pOffscreenSurface, NULL, D3DCOLOR_ARGB(0xFF, 0, 0, 0)));

  if(m_pixelShader != NULL)
  {
    HR(m_pixelShader->Load(m_pDevice));
  }

  for(int i = 0; i < 4; i++)
  {
    if(m_quadPixelShaders[i] != NULL)
    {
      HR(m_quadPixelShaders[i]->Load(m_pDevice));
    }
  }

  return S_OK;
}

void CVideoRenderer::UnloadDirect3D()
{
  SafeRelease(m_pOffscreenSurface);
  SafeRelease(m_pVertexBuffer);
  SafeRelease(m_pTextureSurface);
  SafeRelease(m_pTexture);
  SafeRelease(m_pDevice);

  if(m_pD3D9 != NULL)
  {
    m_pD3D9->Release();
    m_pD3D9 = NULL;
  }
}

HRESULT CVideoRenderer::CreateScene(void)
{
  HRESULT hr;

  HR(m_pDevice->Clear(D3DADAPTER_DEFAULT, NULL, D3DCLEAR_TARGET, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0));

  HR(m_pDevice->BeginScene());

  hr = m_pDevice->SetFVF(D3DFVF_CUSTOMVERTEX);
  
  if(SUCCEEDED(hr))
  {
    hr = m_pDevice->SetStreamSource(0, m_pVertexBuffer, 0, sizeof(VERTEX));
  }
  
  if(SUCCEEDED(hr))
  {
    hr = m_pDevice->SetTexture(0, m_pTexture);
  }

  // draw primitives
  int aVertexIndex = 0;
  if(single == m_composition)
  {
    hr = m_pDevice->SetPixelShader(m_pixelShader != NULL ? m_pixelShader->GetShaderHandle() : NULL);
    if(SUCCEEDED(hr))
    {
      hr = m_pDevice->DrawPrimitive(D3DPT_TRIANGLEFAN, aVertexIndex, 2);
    }
  }
  else if(quad == m_composition)
  {
    aVertexIndex = 4; // offset to first quad definition
    for(int i = 0; i < 4; i++)
    {

      hr = m_pDevice->SetPixelShader(m_quadPixelShaders[i] != NULL ?
        m_quadPixelShaders[i]->GetShaderHandle() : NULL);

      if(SUCCEEDED(hr))
      {
        hr = m_pDevice->DrawPrimitive(D3DPT_TRIANGLEFAN, aVertexIndex, 2);
        aVertexIndex += 4;
      }

      if(FAILED(hr))
      {
        break;
      }
    }
  }

  if(FAILED(hr))
  {
    m_pDevice->EndScene();
    return hr;
  }

  return m_pDevice->EndScene();
}

HRESULT CVideoRenderer::CopyVideoDataToOffscreenSurface(BYTE* theVideoData, D3DFORMAT theFormat)
{
  D3DLOCKED_RECT d3drect;
  HR(m_pOffscreenSurface->LockRect(&d3drect, NULL, 0));

  BYTE* destBuffer = (BYTE*)d3drect.pBits;
  DWORD inputOffset, outputOffset;
  DWORD scanLineLength = min(m_videoPitch, (DWORD)d3drect.Pitch);
  // assume bottom-up orientation for RGB input
  bool isInputBottomUpOrientation = (D3DFMT_A8R8G8B8 == theFormat);
  for(DWORD y = 0; y < m_videoHeight; y++)
  {
    inputOffset = isInputBottomUpOrientation ? (m_videoHeight - y - 1) * m_videoPitch :
      y * m_videoPitch;
  outputOffset = y * d3drect.Pitch;
  memcpy(destBuffer + outputOffset, theVideoData + inputOffset, scanLineLength);
  }

  HR(m_pOffscreenSurface->UnlockRect());

  return S_OK;
}
