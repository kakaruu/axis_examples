// VideoDlg.h : header file
//
// Our viewer video dialog which displays the video media content
//

#pragma once

// Import AxisVideoTransformLib type library definitions from DLL 
// Only required for point mapping (mouse interaction)
#import "../../Redist/AxisVideoTransform.dll"

#include <afxmt.h>
#include <afxdialogex.h>

#include "AxisMediaViewer.h"
#include "AxisMediaViewerEvents.h"
#include "VideoRenderer.h"
#include "DewarpPixelShader.h"

enum ViewType
{
  single,
  quad,
  panorama
};


/// CVideoDlg dialog
class CVideoDlg : public CDialogEx
{
private:

  DECLARE_DYNAMIC(CVideoDlg)

  // Our class member variables
  HICON m_hIcon;
  CAxisMediaViewer m_viewer;
  DWORD m_dwCallbackCookie;
  CEvent m_ThreadEvent;
  BOOL m_bAbort;
  HANDLE m_hFile;
  CString m_fileName;
  LensProfileData m_lensProfileData;
  TiltOrientation m_tiltOrientation;
  LONG m_mediaDataFilePosition;
  bool doDewarp;
  bool doRepeatPlayback;

  ViewType m_viewType;

  CSize m_videoSize;

  CVideoRenderer m_videoRenderer;
  CDewarpPixelShader m_dewarpPixelShader;
  CDewarpPixelShader m_quadPixelShaders[4];
  CDewarpPixelShader m_panoramaCeilingPixelShader;
  CDewarpPixelShader m_panoramaDeskPixelShader;
  CDewarpPixelShader m_panoramaWallPixelShader;

  /// AXIS Dewarping Transform used for point mapping (mouse interaction)
  AxisVideoTransformLib::IDewarpingTransformPtr m_dewarpingTransform;

  HRESULT Setup();
  HRESULT ConfigureDewarpEffect(bool enable);
  HRESULT TakeSnapshot();

  bool IsOverview();
  void Zoom(float zoomFactor);

  /// Update dewarping transform state according to the current state
  HRESULT SynchronizeDewarpingTransformState();

  static UINT ThreadInit(LPVOID pParam);
  UINT Thread();

  // IAxisMediaViewerEvents
  void OnDecodedImage(UINT64 dw64StartTime, SHORT nColorSpace, VARIANT &SampleArray);

public:

  CVideoDlg(LPCTSTR theFileName,
    const LensProfileData& theLensProfileData,
    const TiltOrientation theTiltOrientation,
    const ViewType theViewType);
  virtual ~CVideoDlg();

  // Dialog Data
  enum { IDD = IDD_VIDEO_DIALOG };

protected:

  DECLARE_MESSAGE_MAP()
  DECLARE_DISPATCH_MAP()
  DECLARE_INTERFACE_MAP()

public:

  virtual BOOL OnInitDialog();
  afx_msg void OnPaint();
  afx_msg void OnSize(UINT nType, int cx, int cy);
  afx_msg void OnDestroy();
  afx_msg UINT OnGetDlgCode();
  afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
  afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
  afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);

};
