// DewarpPixelShader.cpp : implementation file
//

#include "stdafx.h"
#include <stdio.h>
#include <fstream>
#include "DewarpPixelShader.h"

#define FOV_MIN_DEG 2
#define FOV_MAX_DEG 45

#define DegToRad(deg) (((deg) * PI)/180.0F)

const FLOAT PI = 3.1415926535897932384626433832795F;

CDewarpPixelShader::CDewarpPixelShader()
: m_shaderData(NULL)
, m_videoWidth(2592)
, m_videoHeight(1944)
, m_tiltOrientation(undef)
{
}

CDewarpPixelShader::~CDewarpPixelShader()
{
  Unload();
}

HRESULT CDewarpPixelShader::Load(IDirect3DDevice9* theDevice)
{
  Unload();

  if(m_shaderData == NULL)
  {
    return E_FAIL;
  }

  m_device = theDevice;

  HR(D3DXGetShaderConstantTable((DWORD*)m_shaderData, &m_constTable));
  HR(m_device->CreatePixelShader((DWORD*)m_shaderData, &m_pixelShader));

  // "size" denotes video input frame size
  FLOAT size[2] = {m_videoWidth, m_videoHeight};
  HR(m_constTable->SetValue(m_device, "size", size, 2 * sizeof(FLOAT)));

  // "imageCenter" is typically the center of the video frame
  FLOAT imageCenter[2] = {m_videoWidth/2.0f, m_videoHeight/2.0f};
  HR(m_constTable->SetValue(m_device, "imageCenter", imageCenter, 2 * sizeof(FLOAT)));

  // "LensProfile" describes the lens characteristics and is dependent on the input frame size
  HR(m_constTable->SetValue(m_device, "LensProfile", m_scaledLensProfileData, sizeof(LensProfileData)));

  UpdatePTZPos();

  return S_OK;
}

void CDewarpPixelShader::Unload()
{
  SafeRelease(m_pixelShader);
  SafeRelease(m_constTable);
  SafeRelease(m_device);
}

IDirect3DPixelShader9* CDewarpPixelShader::GetShaderHandle()
{
  // make sure constant table is update-to-date with this shader
  UpdatePTZPos();
  return m_pixelShader;
}

HRESULT CDewarpPixelShader::SetShaderResource(TCHAR* theFileName)
{
  std::ifstream file(theFileName, std::ios::binary);
  if (file) 
  {
    file.seekg(0,std::ios::end);
    std::streampos length = file.tellg();
    file.seekg(0,std::ios::beg);

    m_shaderData = new char[(unsigned int)length];
    file.read(m_shaderData, length);
    file.close();
    return S_OK;
  }

  return E_FAIL;
}

HRESULT CDewarpPixelShader::SetShaderResource(DWORD theResourceId, LPCTSTR theResourceType)
{
  HRSRC hRes = FindResource(NULL,  MAKEINTRESOURCE(theResourceId), theResourceType);
  if(hRes != NULL)
  {
    HGLOBAL hResourceLoaded = LoadResource(NULL, hRes);
    if(hResourceLoaded != NULL)
    {
      char* lpResLock = (char *) LockResource(hResourceLoaded);
      if(lpResLock != NULL)
      {
        DWORD dwSizeRes = SizeofResource(NULL, hRes);
        if(dwSizeRes > 0)
        {
          m_shaderData = lpResLock;
          return S_OK;
        }
      }
    }
  }

  return E_FAIL;
}

void CDewarpPixelShader::SetVideoProperties(int theWidth, int theHeight,
  const LensProfileData& theLensProfile, const TiltOrientation theTiltOrientation)
{
  m_videoWidth = (FLOAT)theWidth;
  m_videoHeight = (FLOAT)theHeight;

  // lens profile must be scaled with video frame height
  m_scaledLensProfileData[0] = m_videoHeight * theLensProfile[0];
  m_scaledLensProfileData[1] = m_videoHeight * theLensProfile[1];
  m_scaledLensProfileData[2] = m_videoHeight * theLensProfile[2];

  if(theTiltOrientation != m_tiltOrientation)
  {
    m_tiltOrientation = theTiltOrientation;
    GotoDefaultPosition();
  }
}

void CDewarpPixelShader::SetAbsolutePosition(float pan, float tilt, float fieldOfView)
{
  m_pan = pan;
  m_tilt = tilt;
  m_fieldOfView = fieldOfView;
  UpdatePTZPos();
}

void CDewarpPixelShader::SetRelativePosition( float relPan, float relTilt, float relFieldOfView )
{
  m_pan += relPan;
  m_tilt += relTilt;
  m_fieldOfView += relFieldOfView;

  if(m_tilt < m_tiltMin)
  {
    m_tilt = m_tiltMin;
  }
  else if(m_tilt > m_tiltMax)
  {
    m_tilt = m_tiltMax;
  }

  if(m_fieldOfView < FOV_MIN_DEG)
  {
    m_fieldOfView = FOV_MIN_DEG;
  }
  else if(m_fieldOfView > FOV_MAX_DEG)
  {
    m_fieldOfView = FOV_MAX_DEG;
  }

  UpdatePTZPos();
}

void CDewarpPixelShader::GotoDefaultPosition()
{
  switch (m_tiltOrientation)
  {
  case wall:
    m_tiltMin = -90.0f;
    m_tiltMax = 90.0f;
    SetAbsolutePosition(0.0f, 0.0f, 30.0f);
    break;
  case desk:
    m_tiltMin = 0.0f;
    m_tiltMax = 90.0f;
    SetAbsolutePosition(0.0f, 30.0f, 30.0f);
    break;
  case ceiling:
    m_tiltMin = -90.0f;
    m_tiltMax = 0.0f;
    SetAbsolutePosition(0.0f, -30.0f, 30.0f);
    break;
  default:
    m_tiltMin = -90.0f;
    m_tiltMax = 90.0f;
    SetAbsolutePosition(0.0f, 0.0f, 30.0f);
    break;
  }
}

void CDewarpPixelShader::UpdatePTZPos()
{
  if(m_device == NULL)
  {
    return;
  }

  FLOAT lambdaOffset;
  FLOAT rotateData[3];
  FLOAT tangentOfFieldOfView = tan(DegToRad(m_fieldOfView));

  switch (m_tiltOrientation)
  {
  case wall:
    lambdaOffset = PI;
    rotateData[0] = DegToRad(-m_tilt);
    rotateData[1] = DegToRad(m_pan);
    rotateData[2] = 0.0f;
    break;
  case desk:
    lambdaOffset = PI/2;
    rotateData[0] = 0.0f;
    rotateData[1] = DegToRad(-(m_tilt - 90));
    rotateData[2] = DegToRad(-m_pan - 90);
    break;
  case ceiling:
    lambdaOffset = PI/2;
    rotateData[0] = 0.0f;
    rotateData[1] = DegToRad(-(m_tilt + 90));
    rotateData[2] = DegToRad(m_pan + 90);
    break;
  default:
    lambdaOffset = PI/2;
    rotateData[0] = 0.0f;
    rotateData[1] = DegToRad(-m_tilt);
    rotateData[2] = DegToRad(-m_pan);
    break;
  }

  // "lambdaOffset" defines axes orientation (PI/2 = ceiling or desk, PI = wall) 
  m_constTable->SetValue(m_device, "lambdaOffset", &lambdaOffset, sizeof(FLOAT));

  // "rotateData" defines pan-tilt angles (in radians)
  m_constTable->SetValue(m_device, "rotateData", rotateData, 3 * sizeof(FLOAT));

  // "tangentOfFieldOfView" defines tangent of field-of-view, e.g. zoom factor
  m_constTable->SetValue(m_device, "tangentOfFieldOfView", &tangentOfFieldOfView, sizeof(FLOAT));
}
