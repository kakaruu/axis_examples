// VideoDlg.cpp : implementation file
//

#include "stdafx.h"
#include <afxctl.h>
#include <atlsafe.h>
#include <fstream>
#include "DewarpDirect3D.h"
#include "VideoDlg.h"

#define INPUT_VIDEO_FORMAT AMV_CS_YUY2 //AMV_CS_RGB32
#define CLIENT_AREA_WIDTH 1024

#define LOAD_SHADER_FROM_RESOURCE

// CVideoDlg dialog

IMPLEMENT_DYNAMIC(CVideoDlg, CDialogEx)

  CVideoDlg::CVideoDlg(LPCTSTR theFileName,
  const LensProfileData& theLensProfileData,
  const TiltOrientation theTiltOrientation,
  const ViewType theViewType)
  : CDialogEx(CVideoDlg::IDD, NULL)
  , m_fileName(theFileName)
  , m_tiltOrientation(theTiltOrientation)
  , m_viewType(theViewType)
  , doDewarp(true)
  , doRepeatPlayback(true)

{
  // Enable OLE automation
  EnableAutomation();

  m_lensProfileData[0] = theLensProfileData[0];
  m_lensProfileData[1] = theLensProfileData[1];
  m_lensProfileData[2] = theLensProfileData[2];

  // Initialize all class member variables
  m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
  m_hFile = NULL;
  m_bAbort = false;


#ifdef LOAD_SHADER_FROM_RESOURCE
  // load shader from resource
  m_dewarpPixelShader.SetShaderResource(IDR_PS_DEWARP, _T("PS"));
  m_panoramaCeilingPixelShader.SetShaderResource(IDR_PS_PANORAMA_CEILING, _T("PS"));
  m_panoramaDeskPixelShader.SetShaderResource(IDR_PS_PANORAMA_DESK, _T("PS"));
  m_panoramaWallPixelShader.SetShaderResource(IDR_PS_PANORAMA_WALL, _T("PS"));
  for (int i = 0; i < 4; i++)
  {
    m_quadPixelShaders[i].SetShaderResource(IDR_PS_DEWARP, _T("PS"));
  }
#else
  // load shader from file
  m_dewarpPixelShader.SetShaderResource(L"AxisD3D9DewarpPS_2_a.h");
  m_panoramaCeilingPixelShader.SetShaderResource(L"AxisD3D9PanoramaCeilingPS_2_a.h");
  m_panoramaDeskPixelShader.SetShaderResource(L"AxisD3D9PanoramaDeskPS_2_a.h");
  m_panoramaWallPixelShader.SetShaderResource(L"AxisD3D9PanoramaWallPS_2_a.h");
  for (int i = 0; i < 4; i++)
  {
    m_quadPixelShaders[i].SetShaderResource(L"AxisD3D9DewarpPS_2_a.h");
  }
#endif

  // AXIS Dewarping Transform module for coordinate translation (see OnLButtonDown, OnMouseWheel)
  m_dewarpingTransform.CreateInstance(__uuidof(AxisVideoTransformLib::DewarpingTransform));
  if(m_dewarpingTransform.GetInterfacePtr() != NULL)
  {
    CComSafeArray<DOUBLE> aLensProfileArray;
    aLensProfileArray.Add(theLensProfileData[0]);
    aLensProfileArray.Add(theLensProfileData[1]);
    aLensProfileArray.Add(theLensProfileData[2]);

    m_dewarpingTransform->LensProfile = aLensProfileArray;
    m_dewarpingTransform->TiltOrientation = theTiltOrientation;

    // Point mapping only supported in single view type
    m_dewarpingTransform->ViewType = AxisVideoTransformLib::single;
  }
}

CVideoDlg::~CVideoDlg()
{
}

BEGIN_MESSAGE_MAP(CVideoDlg, CDialogEx)
  ON_WM_PAINT()
  ON_WM_SIZE()
  ON_WM_DESTROY()
  ON_WM_GETDLGCODE()
  ON_WM_KEYDOWN()
  ON_WM_LBUTTONDOWN()
  ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CVideoDlg, CDialogEx)     
  DISP_FUNCTION_ID(CVideoDlg,"OnDecodedImage",0x64,OnDecodedImage,VT_EMPTY,VTS_UI8 VTS_I2 VTS_VARIANT)
END_DISPATCH_MAP()

BEGIN_INTERFACE_MAP(CVideoDlg, CDialogEx)
  INTERFACE_PART(CVideoDlg, __uuidof(IAxisMediaViewerEvents), Dispatch)
END_INTERFACE_MAP()

UINT CVideoDlg::ThreadInit(LPVOID pParam)
{
  CVideoDlg* dlg = reinterpret_cast<CVideoDlg*>(pParam);
  dlg->Thread();

  return 0;
}

/// Thread() will read all frame data from input file and send them to the viewer to be rendered.
UINT CVideoDlg::Thread()
{
  // Define local variables
  COleSafeArray SafeArray;
  VARIANT varBuffer;
  DWORD dwSampleType;
  DWORD dwFlags;
  UINT64 dw64StartTime;
  UINT64 dw64StopTime;
  DWORD dwBufferSize;
  BYTE* pBuffer;
  DWORD dwReturn;

  // Setup initial buffer for our MFC helper class
  SafeArray.CreateOneDim(VT_UI1, 1);

  // Initialize COM for this thread
  ::CoInitializeEx(NULL, COINIT_MULTITHREADED);

  // Loop through all frames found in our input file
  try
  {
    while (!m_bAbort)
    {
      // Read frame data from in file
      ReadFile(m_hFile, &dwSampleType, 4, &dwReturn, NULL);
      if (dwReturn != 4)
      {
        // end-of-file
        if(m_viewer.Flush())
        {
          if(doRepeatPlayback)
          {
            m_viewer.Stop();
            SetFilePointer(m_hFile, m_mediaDataFilePosition, 0, FILE_BEGIN);
            m_viewer.Start();
            continue;
          }
        }

        break;
      }

      // Read headers from file
      ReadFile(m_hFile, &dwFlags, 4, &dwReturn, NULL);
      ReadFile(m_hFile, &dw64StartTime, 8, &dwReturn, NULL);
      ReadFile(m_hFile, &dw64StopTime, 8, &dwReturn, NULL);
      ReadFile(m_hFile, &dwBufferSize, 4, &dwReturn, NULL);

      // Render video as fast as possible
      //dw64StartTime = 0;

      // Setup our MFC helper class and read frame data from file
      SafeArray.ResizeOneDim(dwBufferSize);
      SafeArray.AccessData((void**) &pBuffer);
      ReadFile(m_hFile, pBuffer, dwBufferSize, &dwReturn, NULL);
      SafeArray.UnaccessData();

      // Render video and audio samples retrieved from input file
      varBuffer = SafeArray.Detach();

      switch (dwSampleType)
      {
      case AMV_VST_MPEG4_VIDEO_CONFIG:
      case AMV_VST_MPEG4_VIDEO_IVOP:
      case AMV_VST_MPEG4_VIDEO_PVOP:
      case AMV_VST_MPEG4_VIDEO_BVOP:
      case AMV_VST_MPEG4_VIDEO_FRAGMENT:
      case AMV_VST_MJPEG_VIDEO:
      case AMV_VST_H264_VIDEO_CONFIG:
      case AMV_VST_H264_VIDEO_IDR:
      case AMV_VST_H264_VIDEO_NON_IDR:
      case AMV_VST_H264_VIDEO_SEI:
        if ((m_viewer.GetPlayOptions() & AMV_PO_VIDEO) > 0)
        {
          m_viewer.RenderVideoSample(dwFlags, dw64StartTime, dw64StopTime, varBuffer);
        }
        break;
      case AMV_VST_MPEG4_AUDIO:
        if ((m_viewer.GetPlayOptions() & AMV_PO_AUDIO) > 0)
        {
          m_viewer.RenderAudioSample(dwFlags, dw64StartTime, dw64StopTime, varBuffer);
        }
        break;
      default:
        AfxMessageBox(_T("Unsupported sample type"));
        break;
      }

      SafeArray.Attach(varBuffer);
    }
  }
  catch (COleDispatchException* e)
  {
    AfxMessageBox(e->m_strDescription);
    e->Delete();
  }

  // Uninitialize COM for this thread
  ::CoUninitialize();

  // Release all waiting threads
  m_ThreadEvent.SetEvent();

  return 0;
}

HRESULT CVideoDlg::Setup()
{
  // Define local variables
  COleSafeArray SafeArray;
  VARIANT varMediaType;
  DWORD dwReturn;
  DWORD dwBufferSize;
  BYTE* pBuffer;
  CRect rcWnd;
  CRect rcClient;

  // Create the AXIS Media Viewer COM object
  if (!m_viewer.CreateDispatch(_T("AxisMediaViewerLib.AxisMediaViewer")))
  {
    // Install AXIS Media Parser SDK: http://www.axis.com/partner_pages/media_parser.php
    AfxMessageBox(_T("Failed to create dispatch for AxisMediaViewer!"));
    return false;
  }

  // Get a pointer to IUnknown for our event handler class
  IUnknown* pUnkCallback = this->GetIDispatch(false);

  // Establish a connection between viewer and event handlers
  AfxConnectionAdvise(m_viewer, __uuidof(IAxisMediaViewerEvents), pUnkCallback, false, &m_dwCallbackCookie);

  // Open input file containing data stream
  m_hFile = CreateFile(m_fileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
  if (m_hFile == INVALID_HANDLE_VALUE)
  {
    AfxMessageBox(_T("Failed to open file!"));
    return false;
  }

  // Read media type data describing data stream
  ReadFile(m_hFile, &dwBufferSize, 4, &dwReturn, NULL);
  SafeArray.CreateOneDim(VT_UI1, dwBufferSize);
  SafeArray.AccessData((void**) &pBuffer);
  ReadFile(m_hFile, pBuffer, dwBufferSize, &dwReturn, NULL);
  SafeArray.UnaccessData();
  m_mediaDataFilePosition = 4 + dwBufferSize;

  try
  {
    // Setup our COM object properties
    m_viewer.SetPlayOptions(AMV_PO_VIDEO);
    m_viewer.SetColorSpace(INPUT_VIDEO_FORMAT);

    // Initialize our viewer _without_ internal rendering.
    // Decoded image data will be delivered from OnDecodedImage event.
    varMediaType = SafeArray.Detach();
    m_viewer.Init(0, varMediaType, NULL /*no internal rendering*/);
    SafeArray.Attach(varMediaType);

    // Get video size in pixels from our viewer
    m_viewer.GetVideoSize(&m_videoSize.cx, &m_videoSize.cy);
    DWORD aClientAreaHeight = (DWORD)(CLIENT_AREA_WIDTH * m_videoSize.cy/(FLOAT)m_videoSize.cx);

    if(panorama == m_viewType && wall == m_tiltOrientation)
    {
      // Wall orientation only produce a single panorama (180�)
      aClientAreaHeight /= 2;
    }

    // Adjust size of dialog to fit video size and center it on screen
    GetWindowRect(&rcWnd);
    GetClientRect(&rcClient);
    MoveWindow(0, 0,
      rcWnd.Width() - rcClient.Width() + CLIENT_AREA_WIDTH,
      rcWnd.Height() - rcClient.Height() + aClientAreaHeight, FALSE);
    CenterWindow();

    // Set render window (optional)
    m_videoRenderer.SetTargetWindow(m_hWnd);

    // Uncomment this if a custom output size is required.
    // Note that snapshots (GetCurrentImage) will be captured in this size.
    //m_videoRenderer.SetOutputSize(m_videoSize.cx, m_videoSize.cy);

    ConfigureDewarpEffect(doDewarp);

    // Start our viewer
    m_viewer.Start();
  }
  catch (COleDispatchException* e)
  {
    AfxMessageBox(e->m_strDescription);
    e->Delete();
    return false;
  }

  return true;
}

HRESULT CVideoDlg::ConfigureDewarpEffect(bool enable)
{
  doDewarp = enable;

  if(doDewarp)
  {
    // setup shaders
    m_dewarpPixelShader.SetVideoProperties(m_videoSize.cx, m_videoSize.cy,
      m_lensProfileData, m_tiltOrientation);
    m_panoramaCeilingPixelShader.SetVideoProperties(m_videoSize.cx, m_videoSize.cy,
      m_lensProfileData, m_tiltOrientation);
    m_panoramaDeskPixelShader.SetVideoProperties(m_videoSize.cx, m_videoSize.cy,
      m_lensProfileData, m_tiltOrientation);
    m_panoramaWallPixelShader.SetVideoProperties(m_videoSize.cx, m_videoSize.cy,
      m_lensProfileData, m_tiltOrientation);
    for(int i = 0; i < 4; i++)
    {
      m_quadPixelShaders[i].SetVideoProperties(m_videoSize.cx, m_videoSize.cy,
        m_lensProfileData, m_tiltOrientation);
    }

    // configure renderer
    if(quad == m_viewType)
    {
      m_videoRenderer.SetComposition(CVideoRenderer::quad);
      for(int i = 0; i < 4; i++)
      {
        // offset pan 90 degrees in each view
        m_quadPixelShaders[i].GotoDefaultPosition();
        m_quadPixelShaders[i].SetRelativePosition(90.0f * i - 45.0f, 0, 0);
        m_videoRenderer.SetQuadPixelShader(&m_quadPixelShaders[i], i);
      }
    }
    else if(panorama == m_viewType)
    {
      m_videoRenderer.SetComposition(CVideoRenderer::single);
      switch(m_tiltOrientation)
      {
      case ceiling:
        m_videoRenderer.SetPixelShader(&m_panoramaCeilingPixelShader);
        break;
      case desk:
        m_videoRenderer.SetPixelShader(&m_panoramaDeskPixelShader);
        break;
      case wall:
        m_videoRenderer.SetPixelShader(&m_panoramaWallPixelShader);
        break;
      }
    }
    else // singel
    {
      m_videoRenderer.SetComposition(CVideoRenderer::single);
      m_videoRenderer.SetPixelShader(&m_dewarpPixelShader);
    }
  }
  else // !doDewarp
  {
    m_videoRenderer.SetPixelShader(NULL);
    m_videoRenderer.SetComposition(CVideoRenderer::single);
  }
  
  return S_OK;
}

HRESULT CVideoDlg::TakeSnapshot() 
{
  DWORD width, height, stride;
  HR(m_videoRenderer.GetOutputImage(NULL, &width, &height, &stride));

  BITMAPINFOHEADER info_header;
  info_header.biSize = sizeof(BITMAPINFOHEADER);
  info_header.biWidth = width;
  info_header.biHeight = -(LONG)height; // negative = top-down orientation
  info_header.biPlanes = 1;
  info_header.biBitCount = 32; // ARGB
  info_header.biCompression = BI_RGB;
  info_header.biSizeImage = stride*height;
  info_header.biXPelsPerMeter = 1;
  info_header.biYPelsPerMeter = 1;
  info_header.biClrUsed = 0;
  info_header.biClrImportant = 0;

  BITMAPFILEHEADER file_header;
  file_header.bfType = 0x4D42; // BM
  file_header.bfSize = sizeof(BITMAPFILEHEADER);
  file_header.bfReserved1 = 0;
  file_header.bfReserved2 = 0;
  file_header.bfOffBits = sizeof(BITMAPINFOHEADER) + sizeof(BITMAPFILEHEADER);


  BYTE* imageBuffer = new BYTE[info_header.biSizeImage];
  HR(m_videoRenderer.GetOutputImage(imageBuffer, &width, &height, &stride));

  std::ofstream file("snapshot.bmp", std::ios::binary);
  if (file) 
  {
    file.write((const char*)&file_header, sizeof(BITMAPFILEHEADER));
    file.write((const char*)&info_header, sizeof(BITMAPINFOHEADER));
    file.write((const char*)imageBuffer, info_header.biSizeImage);
    file.close();
  }

  delete [] imageBuffer;

  return S_OK;
}

bool CVideoDlg::IsOverview()
{
  return !doDewarp;
}

void CVideoDlg::Zoom(float zoomFactor)
{
  if (IsOverview() && zoomFactor > 0)
  {
    // Goto dewarped view
    ConfigureDewarpEffect(true); // dewarped view
    float aPan = m_dewarpPixelShader.GetPan();
    float aTilt = m_dewarpPixelShader.GetTilt();
    m_dewarpPixelShader.SetAbsolutePosition(aPan, aTilt, 45);
  }
  else if (m_dewarpPixelShader.GetFieldOfView() - zoomFactor > 45)
  {
    // Goto overview
    ConfigureDewarpEffect(false); // overview
  }
  else
  {
    m_dewarpPixelShader.SetRelativePosition(0, 0, -zoomFactor);
  }
}

HRESULT CVideoDlg::SynchronizeDewarpingTransformState()
{
  _ASSERT(m_dewarpingTransform.GetInterfacePtr() != NULL);

  m_dewarpingTransform->Bypass = !doDewarp;
  m_dewarpingTransform->Pan = m_dewarpPixelShader.GetPan();
  m_dewarpingTransform->Tilt = m_dewarpPixelShader.GetTilt();
  m_dewarpingTransform->FieldOfView = m_dewarpPixelShader.GetFieldOfView();

  return S_OK;
}

BOOL CVideoDlg::OnInitDialog()
{
  CDialogEx::OnInitDialog();

  SetIcon(m_hIcon, TRUE);			// Set big icon
  SetIcon(m_hIcon, FALSE);		// Set small icon

  // Setup our viewer
  if (FAILED(Setup()))
  {
    m_ThreadEvent.SetEvent();
    EndDialog(IDCANCEL);
    return true;
  }

  // Start thread to render frames
  AfxBeginThread(ThreadInit, this);

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void CVideoDlg::OnPaint()
{
  CPaintDC dc(this); // device context for painting
}

void CVideoDlg::OnSize(UINT nType, int cx, int cy)
{
  // Optionally change render output size to match new client area
  RECT aClientRect;
  ::GetClientRect(m_hWnd, &aClientRect);
  m_videoRenderer.SetOutputSize(aClientRect.right - aClientRect.left,
    aClientRect.bottom - aClientRect.top);

  CDialogEx::OnSize(nType, cx, cy);
}

void CVideoDlg::OnDestroy()
{
  // Tell render thread to abort
  m_bAbort = true;

  // Tell viewer to stop and release render call
  try
  {
    if (m_viewer.GetStatus() & AMV_STATUS_RUNNING)
    {
      m_viewer.Stop();
    }
  }
  catch (COleDispatchException* e)
  {
    AfxMessageBox(e->m_strDescription);
    e->Delete();
  }

  // Wait up to 500 ms for render thread to terminate
  WaitForSingleObject(m_ThreadEvent, 500);

  // Close input file
  if (m_hFile)
  {
    CloseHandle(m_hFile);
  }

  // Unregister event sink
  IUnknown* pUnkCallback = this->GetIDispatch(FALSE);
  AfxConnectionUnadvise(m_viewer, __uuidof(IAxisMediaViewerEvents),
    pUnkCallback, FALSE, m_dwCallbackCookie);

  // Release COM object
  m_viewer.ReleaseDispatch();

  CDialogEx::OnDestroy();
}

UINT CVideoDlg::OnGetDlgCode()
{
  // capture arrow keys
  return DLGC_WANTALLKEYS;
}

void CVideoDlg::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
  // global key commands
  switch (nChar)
  {
  case 'D':
    // Enable/disable dewarp effect
    ConfigureDewarpEffect(!doDewarp);
    break;
  case 'S':
    (void)TakeSnapshot();
    break;
  }

  if(m_viewType == single)
  {
    switch(nChar)
    {
    case VK_LEFT:
      m_dewarpPixelShader.SetRelativePosition(-1.0, 0.0f, 0.0f);
      break;
    case VK_RIGHT:
      m_dewarpPixelShader.SetRelativePosition(1.0, 0.0f, 0.0f);
      break;
    case VK_UP:
      m_dewarpPixelShader.SetRelativePosition(0.0f, 1.0, 0.0f);
      break;
    case VK_DOWN:
      m_dewarpPixelShader.SetRelativePosition(0.0f, -1.0, 0.0f);
      break;
    case VK_ADD:
      Zoom(1.0);
      break;
    case VK_SUBTRACT:
      Zoom(-1.0);
      break;
    }
  }
  else if(m_viewType == quad)
  {
    // only expose pan
    float aRelPan = 0.0f;
    switch(nChar)
    {
    case VK_LEFT:
      aRelPan = -1.0;
      break;
    case VK_RIGHT:
      aRelPan = 1.0;
      break;
    }

    for (int i = 0; i < 4; i++)
    {
      m_quadPixelShaders[i].SetRelativePosition(aRelPan, 0.0f, 0.0f);
    }
  }
}

void CVideoDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
  if(m_viewType != single)
  {
    // Point mapping is currently not supported for quad and panorama views
    return;
  }

  if(m_dewarpingTransform.GetInterfacePtr() == NULL)
  {
    // Mouse control requires AXIS Dewarping Transform to be registered on the system
    _ASSERT(false);
    return;
  }

  RECT aClientRect;
  this->GetClientRect(&aClientRect);
  double aCalcPan, aCalcTilt;
  
  // Updated the dewarping transform with the current PTZ state
  SynchronizeDewarpingTransformState();

  // Map image point to pan/tilt (use normalized coordinates)
  double aNormX = point.x / (double)aClientRect.right;
  double aNormY = point.y / (double)aClientRect.bottom;
  m_dewarpingTransform->MapPoint(aNormX, aNormY, &aCalcPan, &aCalcTilt, 0);

  // Updated the pixel shader with the calculated pan/tilt (keep FOV)
  m_dewarpPixelShader.SetAbsolutePosition((float)aCalcPan, (float)aCalcTilt, m_dewarpPixelShader.GetFieldOfView());
}

BOOL CVideoDlg::OnMouseWheel(UINT nFlags, short zDelta, CPoint point)
{
  if(m_viewType != single)
  {
    // Point mapping is currently not supported for quad and panorama views
    return TRUE;
  }

  if(m_dewarpingTransform.GetInterfacePtr() == NULL)
  {
    // Mouse control requires AXIS Dewarping Transform to be registered on the system
    _ASSERT(false);
    return TRUE;
  }

  RECT aClientRect;
  this->GetClientRect(&aClientRect);
  this->ScreenToClient(&point);

  // Updated the dewarping transform with the current PTZ state
  SynchronizeDewarpingTransformState();

  float aZoomFactor = zDelta / 100.0f;
  double aNormX = point.x / (double)aClientRect.right;
  double aNormY = point.y / (double)aClientRect.bottom;

  // calculate pan/tilt for zoom point
  double aZoomPointPan, aZoomPointTilt;
  m_dewarpingTransform->MapPoint(aNormX, aNormY, &aZoomPointPan, &aZoomPointTilt, 0);

  if (IsOverview() && (aZoomFactor > 0))
  {
    // go from overview to dewarped view at cursor position
    Zoom(aZoomFactor);
    m_dewarpPixelShader.SetAbsolutePosition(
      (float)aZoomPointPan, (float)aZoomPointTilt, m_dewarpPixelShader.GetFieldOfView());
    
    // compensate pan/tilt so that cursor stays over the desired position
    double aNewZoomPointPan, aNewZoomPointTilt;
    SynchronizeDewarpingTransformState();
    m_dewarpingTransform->MapPoint(aNormX, aNormY,
      &aNewZoomPointPan, &aNewZoomPointTilt, 0);

    m_dewarpPixelShader.SetRelativePosition(
      (float)(aZoomPointPan - aNewZoomPointPan),
      (float)(aZoomPointTilt - aNewZoomPointTilt),
      0.0f);
  }
  else
  {
    // zoom in/out at current position
    Zoom(aZoomFactor);

    // compensate pan/tilt error due to zoom operation in order to keep zoom-point position
    double aNewZoomPointPan, aNewZoomPointTilt;
    SynchronizeDewarpingTransformState();
    m_dewarpingTransform->MapPoint(aNormX, aNormY, &aNewZoomPointPan, &aNewZoomPointTilt, 0);

    m_dewarpPixelShader.SetRelativePosition(
      (float)(aZoomPointPan - aNewZoomPointPan),
      (float)(aZoomPointTilt - aNewZoomPointTilt),
      0.0f);
  }

  return TRUE;
}

void CVideoDlg::OnDecodedImage(UINT64 dw64StartTime, SHORT nColorSpace, VARIANT &SampleArray)
{
  // NOTE: this event is not received on the GUI thread

  D3DFORMAT aD3DVideoFormat;
  BYTE* aVideoSampleBuffer;
  DWORD aVideoSampleBufferLength;

  if (m_bAbort)
  {
    return;
  }

  switch (nColorSpace)
  {
  case AMV_CS_YUY2:
    aD3DVideoFormat = D3DFMT_YUY2;
    break;
  case AMV_CS_RGB32:
    aD3DVideoFormat = D3DFMT_A8R8G8B8;
    break;
  default:
    // unsupported format
    _ASSERT(false);
    return;
  }

  COleSafeArray SafeArray;

  // Attach image buffer to our MFC helper class
  SafeArray.Attach(SampleArray);
  SafeArray.AccessData((void**) &aVideoSampleBuffer);
  aVideoSampleBufferLength = SafeArray.GetOneDimSize();

  if(nColorSpace == AMV_CS_RGB32 || nColorSpace == AMV_CS_RGB24)
  {
    BITMAPINFOHEADER* aHeader = (BITMAPINFOHEADER*) aVideoSampleBuffer;
    aVideoSampleBuffer += sizeof(BITMAPINFOHEADER);
    aVideoSampleBufferLength -= sizeof(BITMAPINFOHEADER);
  }
  
  HRESULT hr = m_videoRenderer.RenderVideoSample(aVideoSampleBuffer, aVideoSampleBufferLength, 
    m_videoSize.cx, m_videoSize.cy,
    aD3DVideoFormat, dw64StartTime);

  _ASSERT(SUCCEEDED(hr));
  
  // Release buffer from our MFC helper class
  SafeArray.UnaccessData();
  SampleArray = SafeArray.Detach();
}
