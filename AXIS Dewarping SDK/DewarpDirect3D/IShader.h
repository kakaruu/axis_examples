// IShader.h : header file
//
// Interface shader functionality
//

#pragma once

#include "d3dx9.h"

struct IShader
{  
  virtual HRESULT Load(IDirect3DDevice9* theDevice) PURE;
  virtual void Unload() PURE;
  virtual IDirect3DPixelShader9* GetShaderHandle() PURE;
};