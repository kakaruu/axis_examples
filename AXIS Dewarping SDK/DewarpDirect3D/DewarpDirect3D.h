// DewarpDirect3D.h : main header file for the DewarpDirect3D application
//

#pragma once

#ifndef __AFXWIN_H__
#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols
#include "VideoDlg.h"

struct InputMedia
{
  LPCTSTR fileName;
  const TiltOrientation tiltOrientation;
  const LensProfileData lensProfileData;
};


// CDewarpDirect3DApp:
// See DewarpDirect3D.cpp for the implementation of this class
//
class CDewarpDirect3DApp : public CWinApp
{
public:
  CDewarpDirect3DApp();

  // Overrides
public:
  virtual BOOL InitInstance();

  // Implementation

  DECLARE_MESSAGE_MAP()
};

extern CDewarpDirect3DApp theApp;