// DewarpPixelShader.h : header file
//
// Encapsulate dewarping pixel shader functionality
//

#pragma once

#include "IShader.h"

/// LensProfileData describes the lens characteristics
typedef FLOAT LensProfileData[3];

/// TiltOrientation describes how the camera is mounted.
/// This parameter determines how the virtual PTZ axes are orientated
/// for the most natural PTZ control.
enum TiltOrientation
{
  undef,
  wall,
  ceiling,
  desk
};

class CDewarpPixelShader : public IShader
{
public:
  CDewarpPixelShader();
  virtual ~CDewarpPixelShader();
  
  // IShader implementation
  HRESULT Load(IDirect3DDevice9* theDevice);
  void Unload();
  IDirect3DPixelShader9* GetShaderHandle();

  HRESULT SetShaderResource(TCHAR* theFileName);
  HRESULT SetShaderResource(DWORD theResourceId, LPCTSTR theResourceType);
  
  /// Sets video properties for the current input
  void SetVideoProperties(int theWidth, int theHeight,
    const LensProfileData& theLensProfile, const TiltOrientation theTiltOrientation);

  /// Sets absolute PTZ position
  void SetAbsolutePosition(float pan, float tilt, float fieldOfView);

  /// Sets relative translation from current PTZ position
  void SetRelativePosition(float relPan, float relTilt, float relFieldOfView);

  // Restore to initial PTZ coordinates
  void GotoDefaultPosition();

  float GetPan() { return m_pan; }
  float GetTilt() { return m_tilt; }
  float GetFieldOfView() { return m_fieldOfView; }

private:

  void UpdatePTZPos();

  char* m_shaderData;

  CComPtr<IDirect3DDevice9> m_device;
  CComPtr<IDirect3DPixelShader9> m_pixelShader;
  CComPtr<ID3DXConstantTable> m_constTable;

  FLOAT m_videoWidth, m_videoHeight;
  LensProfileData m_scaledLensProfileData;
  TiltOrientation m_tiltOrientation;

  FLOAT m_pan;
  FLOAT m_tilt;
  FLOAT m_fieldOfView;

  FLOAT m_tiltMin;
  FLOAT m_tiltMax;
};