// VideoRenderer.h : header file
//
// Render video frames using Direct3D 9
//

#pragma once

// Microsoft DirectX SDK 9 (June 2010)
// http://www.microsoft.com/en-us/download/details.aspx?id=6812
#include <d3dx9.h>

#include <afxmt.h>
#include "IShader.h"

struct VERTEX
{
  D3DXVECTOR3 pos;        // vertex untransformed position
  D3DCOLOR    color;      // diffuse color
  D3DXVECTOR2 texPos;     // texture relative coordinates
};

#define D3DFVF_CUSTOMVERTEX ( D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1 )

class AutoLock
{
public:
  AutoLock(CMutex& m): m_mutex(m)  { m_mutex.Lock(); }
  ~AutoLock()                     { m_mutex.Unlock(); }
private:
  CMutex&   m_mutex;
};

// CVideoRenderer dialog

class CVideoRenderer
{
public:
  CVideoRenderer();   // standard constructor
  virtual ~CVideoRenderer();

  enum Composition
  {
    single,
    quad
  };

  void SetTargetWindow(HWND theWindow);
  void SetOutputSize(DWORD theWidth, DWORD theHeight);
  void SetComposition(Composition theComposition);
  HRESULT SetPixelShader(IShader* thePixelShader);
  HRESULT SetQuadPixelShader(IShader* thePixelShader, int theIndex);

  HRESULT RenderVideoSample(BYTE* theVideoData, DWORD theDataSize,
                            DWORD theWidth, DWORD theHeight,
                            D3DFORMAT theFormat, UINT64 theTimestamp);

  HRESULT GetOutputImage(BYTE* theBuffer, DWORD* theWidth, DWORD* theHeight, DWORD* theStride);

private:

  HRESULT InitDirect3D();
  void UnloadDirect3D();

  HRESULT CreateScene();
  HRESULT CopyVideoDataToOffscreenSurface(BYTE* theVideoData, D3DFORMAT theFormat);

  CMutex m_mutex;

  HWND m_window;

  IShader* m_pixelShader;
  IShader* m_quadPixelShaders[4];

  IDirect3D9* m_pD3D9;

  CComPtr<IDirect3DDevice9>       m_pDevice;
  CComPtr<IDirect3DSurface9>      m_pOffscreenSurface;
  CComPtr<IDirect3DVertexBuffer9> m_pVertexBuffer;
  CComPtr<IDirect3DSurface9>      m_pTextureSurface;
  CComPtr<IDirect3DTexture9>      m_pTexture;

  D3DDISPLAYMODE m_displayMode;

  Composition m_composition;

  D3DFORMAT m_videoFormat;
  DWORD m_videoWidth;
  DWORD m_videoHeight;
  DWORD m_videoPitch;

  DWORD m_outputWidth;
  DWORD m_outputHeight;
};