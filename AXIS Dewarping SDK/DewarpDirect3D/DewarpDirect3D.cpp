// DewarpDirect3D.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "DewarpDirect3D.h"

// Sample media files with different camera mount configurations
InputMedia Input = {L"sample_overview_desk.H264.bin", desk, {-0.022618674f, 0.014994084f, 0.36817500f}};
//InputMedia Input = {L"sample_overview_wall.H264.bin", wall, {-0.022618674f, 0.014994084f, 0.36817500f}};
//InputMedia Input = {L"sample_overview_ceiling.H264.bin", ceiling, {-0.022618674f, 0.014994084f, 0.36817500f}};

// Select view type
ViewType View = single;
//ViewType View = panorama;
//ViewType View = quad;

BEGIN_MESSAGE_MAP(CDewarpDirect3DApp, CWinApp)
END_MESSAGE_MAP()

CDewarpDirect3DApp::CDewarpDirect3DApp()
{
}


// The one and only CDewarpDirect3DApp object
CDewarpDirect3DApp theApp;


BOOL CDewarpDirect3DApp::InitInstance()
{
  // InitCommonControlsEx() is required on Windows XP if an application
  // manifest specifies use of ComCtl32.dll version 6 or later to enable
  // visual styles.  Otherwise, any window creation will fail.
  INITCOMMONCONTROLSEX InitCtrls;
  InitCtrls.dwSize = sizeof(InitCtrls);

  // Set this to include all the common control classes you want to use
  // in your application.
  InitCtrls.dwICC = ICC_WIN95_CLASSES;
  InitCommonControlsEx(&InitCtrls);

  CWinApp::InitInstance();

  // Initialize COM for main thread
  CoInitializeEx(NULL, COINIT_MULTITHREADED);

  {
    // Set file name to video dialog
    CVideoDlg videoDialog(Input.fileName, Input.lensProfileData, Input.tiltOrientation, View);

    // Display video dialog and start viewer
    m_pMainWnd = &videoDialog;
    INT_PTR nResponse = videoDialog.DoModal();
    if (nResponse == IDOK)
    {
      // TODO: Place code here to handle when the dialog is
      //  dismissed with OK
    }
    else if (nResponse == IDCANCEL)
    {
      // TODO: Place code here to handle when the dialog is
      //  dismissed with Cancel
    }
  }

  // Uninitialize COM for main thread
  ::CoUninitialize();

  // Since the dialog has been closed, return FALSE so that we exit the
  //  application, rather than start the application's message pump.
  return FALSE;
}
