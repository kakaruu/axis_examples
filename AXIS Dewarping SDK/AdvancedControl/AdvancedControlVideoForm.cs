using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using AxisMediaViewerLib;
using AxisVideoTransformLib;
using DirectShowLib;
using DirectShowLib.DMO;
using System.Diagnostics;
using AxisMediaParserLib;

namespace DewarpDirectShow
{
	/// <summary>
	/// 
	/// This sample shows how to implement very smooth and user-friendly interaction.
	/// - Click-and-drag to move
	/// - Rotate overview circle
	/// - Smooth transition between overview and dewarped viewing
	/// - Optimized for smooth control (using Accelerator Filter)
	/// 
	/// This sample uses AXIS Dewarping Transform DMO as a DirectShow filter.
	/// For more info about this look at the DewarpDirectShowCSharp sample.
	/// 
	/// Dewarping control keys:
	/// D = Dewarping ON/OFF
	/// Arrow-keys = Pan/Tilt
	/// +/- = Zoom Level
	/// 
	/// </summary>
	class AdvancedControlVideoForm : Form
	{
		private const int outputWidth = 1024;
		private const int outputHeight = 768;

		private const int maxFieldOfView = 50;
		private const int defaultTiltAngle = 40;
		private const double dampingFactor = 1.2;

		// Sample video clips (change to test different camera mount configurations)

		/// <summary>
		/// Desk mount
		/// </summary>
		//private const TiltOrientation tiltOrientation = TiltOrientation.desk;
		//private const string filePath = "sample_overview_desk.H264.bin";
		//private double[] LensProfile = new double[] { -0.022618674, 0.014994084, 0.368175 };

		/// <summary>
		/// Wall mount
		/// </summary>
		private const TiltOrientation tiltOrientation = TiltOrientation.wall;
		private const string filePath = "sample_overview_wall.H264.bin";
		private double[] LensProfile = new double[] { -0.022618674, 0.014994084, 0.368175 };
		
		/// <summary>
		/// Ceiling mount
		/// </summary>
		//private const TiltOrientation tiltOrientation = TiltOrientation.ceiling;
		//private const string filePath = "sample_overview_ceiling.H264.bin";
		//private double[] LensProfile = new double[] { -0.022618674, 0.014994084, 0.368175 };

		//private const TiltOrientation tiltOrientation = TiltOrientation.ceiling;
		//private const string filePath = "M3048_overview_ceiling.H264.bin";
		//private double[] LensProfile = new double[] { -0.00076086, -0.00042091, 0.30745344 };


		private AxisMediaParser parser;
		private AxisMediaViewer viewer;

		private IDewarpingTransform dewarpingTransform;

		// reference transform (only used for point mapping)
		private IDewarpingTransform dewarpingTransform2;

		private bool isMouseButtonPressed = false;		
		private Point mouseAnchorPoint;

		private bool doExitRenderThread = false;

		// Use panel derivative to receive arrow key events
		private SelectablePanel videoPanel;

		private Label panTiltLabel;

		// timer for animation effects
		private System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();

		private double lastRelPan;
		private double lastRelTilt;
		private double panSpeed = 0.0;
		private double tiltSpeed = 0.0;
		private double zoomSpeed = 0.0;


		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new AdvancedControlVideoForm());
		}


		public AdvancedControlVideoForm()
		{
			InitializeComponent();
        }

		private void SetupViewer(IntPtr windowHandle)
		{
			// Create the AXIS Media Viewer COM object in a MTA apartment to allow multiple
			// threads (UI and render thread) to access COM object (by default tasks use MTA threads).
			int outputWidth = videoPanel.Width;
			int outputHeight = videoPanel.Height;
			Task.Factory.StartNew(() =>
			{
				try
				{
                    parser = new AxisMediaParser();
                    viewer = new AxisMediaViewer();

                    parser.OnVideoSample += OnVideoSample;

                    parser.ShowLoginDialog = true;
                    parser.MediaURL = "axrtsphttp://192.168.0.247/axis-media/media.amp?videocodec=h264&events=on&eventtopic=//.";
                    parser.MediaUsername = "root";
                    parser.MediaPassword = "!Q2w3e4r5t6y";
                    
					//viewer.EnableOnDecodedImage = false;

					// Set some media properties
					viewer.VideoRenderer = (int)AMV_RENDER_OPTIONS.AMV_VIDEO_RENDERER_VMR9;
					viewer.LiveMode = true;
					//viewer.H264VideoDecodingMode |= (int)AMV_DECODING_MODE.AMV_VIDEO_DECODING_QUALITY_DYNAMIC;
#if WIN64
					viewer.H264EnableHWDecoding = true;
#endif

                    int cookieID;
                    int numberOfStreams;
                    object mediaTypeBuffer;
                    parser.Connect(out cookieID, out numberOfStreams, out mediaTypeBuffer);

                    // Init the Viewer with media type buffer and a win32 hWnd handle to the window. 
                    viewer.Init(0, mediaTypeBuffer, windowHandle.ToInt64());
				}
				catch (System.Exception ex)
				{
					// Install AXIS Media Parser SDK: http://www.axis.com/partner_pages/media_parser.php
					MessageBox.Show("Failed to initialize AXIS Media Viewer:\n" + ex.Message);
					return;
				}

				// Make Media Viewer to inject AXIS Dewarping Transform filter in media graph
				try
				{
					InitDewarpingTransform(outputWidth, outputHeight);
					//viewer.AddFilter(dewarpingTransform, "Dewarping", 0);
				}
				catch (System.Exception ex)
				{
					MessageBox.Show("Failed to add AXIS Dewarping Transform (AxisVideoTransform.dll):\n" +
						ex.Message);
				}

				// Inject Accelerator Filter
				// - Parallelize H.264 decoding and dewarping work on different threads/cores
				// - Compensate for missing frames to avoid stuttering
				try
				{
					//AcceleratorFilter acceleratorFilter;
					//dewarpingTransform.InjectAcceleratorFilter(out acceleratorFilter);
				}
				catch (System.Exception ex)
				{
					MessageBox.Show("Failed to inject Accelerator Filter:\n" + ex.Message);
				}

				viewer.SetVideoPosition(0, 0, videoPanel.Width, videoPanel.Height);

			}).Wait();
		}

        private void OnVideoSample(int cookieID, int sampleType,
        int sampleFlags, ulong startTime, ulong stopTime, object SampleArray)
        {
            BeginInvoke((Action)(() =>
            {
                Console.Write(startTime);
            }));

            // Let viewer render video sample

            // Add LiveTimeOffset to original timestamp for optimal latency when rendering
            long renderStartTime = 0;
            long renderStopTime = 1;
            if ((long)startTime + parser.LiveTimeOffset > 0)
            {
                renderStartTime = (long)startTime + parser.LiveTimeOffset;
                renderStopTime = (long)stopTime + parser.LiveTimeOffset;
            }

            viewer.RenderVideoSample(sampleFlags, (ulong)renderStartTime, (ulong)renderStopTime, SampleArray);
        }

        private void InitDewarpingTransform(int outputWidth, int outputHeight)
		{
			// Use Microsofts DMO wrapper filter to utilize dewarping DMO in DirectShow
			IDMOWrapperFilter wrapperFilter = new DMOWrapperFilter() as IDMOWrapperFilter;
			if (wrapperFilter == null)
			{
				throw new Exception("Could not load DMO wrapper");
			}

			int hr = wrapperFilter.Init(typeof(DewarpingTransformClass).GUID, DMOCategory.VideoEffect);
			DsError.ThrowExceptionForHR(hr);

			dewarpingTransform2 = new DewarpingTransform();

			// Get the dewarping control interface
			dewarpingTransform = (IDewarpingTransform)wrapperFilter;

			// Set lens profile and tilt orientation
			// Default lens profile for AXIS M3007/27
			dewarpingTransform.LensProfile = LensProfile;
			dewarpingTransform2.LensProfile = LensProfile;
			dewarpingTransform.TiltOrientation = (int)tiltOrientation;
			dewarpingTransform2.TiltOrientation = (int)tiltOrientation;

			// Set output video resolution to window size (see VideoForm_Load())
			dewarpingTransform.OutputWidth = outputWidth;
			dewarpingTransform.OutputHeight = outputHeight;

			// Set OutputFrameRate to 60 FPS to get really smooth control
			dewarpingTransform.OutputFrameRate = 60;

			// start in overview
			dewarpingTransform.EffectLevel = 0.0;
			dewarpingTransform.FieldOfView = maxFieldOfView;
		}

		private void DestroyViewer()
		{
			if (viewer != null)
			{
				if (viewer.Status == (int)AMV_STATUS.AMV_STATUS_RUNNING)
				{
					viewer.Stop();
				}

				// Inform the GC that COM object no longer will be used
				Marshal.FinalReleaseComObject(viewer);
				viewer = null;
			}
		}

		private void Play()
		{
			viewer.Start();
            parser.Start();
		}

		private void Zoom(double zoomFactor)
		{
			if (dewarpingTransform.EffectLevel < 1.0) // not fully dewarped
			{
				dewarpingTransform.EffectLevel += 0.1 * zoomFactor;
			}
			else // dewarped mode
			{
				dewarpingTransform.FieldOfView -= 2 * zoomFactor;
				if (dewarpingTransform.FieldOfView >= maxFieldOfView)
				{
					// start transition to overview
					dewarpingTransform.FieldOfView = maxFieldOfView;
					dewarpingTransform.EffectLevel = 0.99;
				}
			}

			// toggle overview
			if (IsOverview() && dewarpingTransform.ViewType == (int)ViewType.singleOverview)
			{
				// do not show overview when completely zoomed-out
				dewarpingTransform.ViewType = (int)ViewType.single;
			}
			else if (!IsOverview() &&	dewarpingTransform.ViewType == (int)ViewType.single)
			{
				// show overview when zoomed-in
				dewarpingTransform.ViewType = (int)ViewType.singleOverview;
			}
		}

		private bool IsOverview()
		{
			return dewarpingTransform.EffectLevel == 0;
		}

		private bool CanMapPoint()
		{
			// Point mapping is currently not supported for quad and panorama views
			return dewarpingTransform.ViewType == (int)ViewType.single ||
				 dewarpingTransform.ViewType == (int)ViewType.singleOverview;
		}

		/// <summary>
		/// This event handler is called after the dialog is shown, since we need the
		/// win32 window handle as a parameter to the Viewer
		/// </summary>
		private void VideoForm_Load(object sender, EventArgs e)
		{
			int xDiff = Width - videoPanel.Width;
			int yDiff = Height - videoPanel.Height;
			this.Width = outputWidth + xDiff;
			this.Height = outputHeight + yDiff;

			this.SetupViewer(this.videoPanel.Handle);

            timer.Interval = 25;
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();

            // Auto start
            Play();
		}

		private void VideoForm_FormClosing(object sender, FormClosingEventArgs e)
		{
            parser.Stop();
			DestroyViewer();
		}

		/// <summary>
		/// Event called when the dialog is resized, the viewer is informed about the new size
		/// </summary>
		private void VideoForm_Resize(object sender, EventArgs e)
		{
			if (viewer != null)
			{
				viewer.SetVideoPosition(0, 0, videoPanel.Width, videoPanel.Height);
			}
		}		

		private void videoPanel_Paint(object sender, PaintEventArgs e)
		{
			if (viewer != null)
			{
				viewer.RepaintVideo();
			}
		}

		private void videoPanel_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
		{
			// Global commands
			switch (e.KeyCode)
			{
				case Keys.D:
                {
                    //dewarpingTransform.Bypass = !dewarpingTransform.Bypass;

                    parser.Stop();
                    parser.Disconnect();
                    viewer.Stop();
                    viewer.Flush();
                    

                    //parser.MediaURL = "axrtsphttp://192.168.0.194/axis-media/media.amp?videocodec=h264&events=on&eventtopic=//.";
                    //parser.MediaUsername = "root";
                    //parser.MediaPassword = "owell";

                    int cookieID;
                    int numberOfStreams;
                    object mediaTypeBuffer;
                    parser.Connect(out cookieID, out numberOfStreams, out mediaTypeBuffer);

                    viewer.Init(numberOfStreams, mediaTypeBuffer, this.videoPanel.Handle.ToInt64());
                    try
                    {
                        InitDewarpingTransform(outputWidth, outputHeight);
                        viewer.AddFilter(dewarpingTransform, "Dewarping", 0);
                    }
                    catch (System.Exception ex)
                    {
                        MessageBox.Show("Failed to add AXIS Dewarping Transform (AxisVideoTransform.dll):\n" +
                            ex.Message);
                    }
                    break;
                }
                case Keys.S:
                {
                    viewer.Start();
                    parser.Start();
                    break;
                }
			}

			switch (e.KeyCode)
			{
				case Keys.Left:
					dewarpingTransform.Pan -= 1.0;
					panSpeed = -1;
					break;
				case Keys.Right:
					dewarpingTransform.Pan += 1.0;
					panSpeed = 1;
					break;
				case Keys.Up:
					dewarpingTransform.Tilt += 1.0;
					tiltSpeed = 1;
					break;
				case Keys.Down:
					dewarpingTransform.Tilt -= 1.0;
					tiltSpeed = -1;
					break;

				case Keys.Add:
					Zoom(0.25);
					zoomSpeed = 0.25;
					break;
				case Keys.Subtract:
					Zoom(-0.25);
					zoomSpeed = -0.25;
					break;
			}
		}

		/// <summary>
		/// Center view at the click position
		/// </summary>
		private void videoPanel_MouseDown(object sender, MouseEventArgs e)
		{
			isMouseButtonPressed = true;

			// dewarpingTransform2 will represent the state at which the mouse button was pressed
			dewarpingTransform2.EffectLevel = dewarpingTransform.EffectLevel;
			dewarpingTransform2.Pan = dewarpingTransform.Pan;
			dewarpingTransform2.Tilt = dewarpingTransform.Tilt;
			dewarpingTransform2.FieldOfView = dewarpingTransform.FieldOfView;

			mouseAnchorPoint.X = e.X;
			mouseAnchorPoint.Y = e.Y;
		}

		private void videoPanel_MouseMove(object sender, MouseEventArgs e)
		{

			if (!CanMapPoint()) return;

			UpdatePanTiltLabel(e.X, e.Y);

			if (!isMouseButtonPressed) return;

			double relPan = 0;
			double relTilt = 0;
			if (dewarpingTransform.EffectLevel == 0.0) // overview
			{
				// get anchor point pan/tilt
				double mouseAnchorPan, mouseAnchorTilt;
				double normX = mouseAnchorPoint.X / (double)videoPanel.Width;
				double normY = mouseAnchorPoint.Y / (double)videoPanel.Height;
				dewarpingTransform2.MapPoint(normX, normY, out mouseAnchorPan, out mouseAnchorTilt, 0);

				// do not move if anchor point is near tilt -90 or 90 where pan is undefined
				if (Math.Abs(Math.Abs(mouseAnchorTilt) - 90.0) > 5)
				{
					// get current point pan/tilt
					double pan, tilt;
					normX = e.X / (double)videoPanel.Width;
					normY = e.Y / (double)videoPanel.Height;
					dewarpingTransform2.MapPoint(normX, normY, out pan, out tilt, 0);

					// calculate the difference in pan degrees between current point and start point
					relPan = mouseAnchorPan - pan;
				}
			}
			else // dewarped mode or transition
			{
				// estimate pan/tilt scale for the current view (dewarpingTransform2)
				double panLeft, panRight, tiltTop, tiltBottom, dummy;
				switch ((TiltOrientation)dewarpingTransform.TiltOrientation)
				{
					case TiltOrientation.ceiling:
						dewarpingTransform2.MapPoint(0, 0, out panLeft, out dummy, 0);
						dewarpingTransform2.MapPoint(1, 0, out panRight, out dummy, 0);
						dewarpingTransform2.MapPoint(0.5, 0, out dummy, out tiltTop, 0);
						dewarpingTransform2.MapPoint(0.5, 1, out dummy, out tiltBottom, 0);
						break;
					case TiltOrientation.desk:
						dewarpingTransform2.MapPoint(0, 1, out panLeft, out dummy, 0);
						dewarpingTransform2.MapPoint(1, 1, out panRight, out dummy, 0);
						dewarpingTransform2.MapPoint(0.5, 0, out dummy, out tiltTop, 0);
						dewarpingTransform2.MapPoint(0.5, 1, out dummy, out tiltBottom, 0);
						break;
					case TiltOrientation.wall:
						dewarpingTransform2.MapPoint(0, 0.5, out panLeft, out dummy, 0);
						dewarpingTransform2.MapPoint(1, 0.5, out panRight, out dummy, 0);
						dewarpingTransform2.MapPoint(0.5, 0, out dummy, out tiltTop, 0);
						dewarpingTransform2.MapPoint(0.5, 1, out dummy, out tiltBottom, 0);
						break;
						default:
						panRight = panLeft = tiltBottom = tiltTop = 0;
						break;
				}

				relPan = (mouseAnchorPoint.X - e.X) * (panRight - panLeft) / (double)videoPanel.Width;
				relTilt = (mouseAnchorPoint.Y - e.Y) * (tiltBottom - tiltTop) / (double)videoPanel.Height;
			}

			// apply new pan/tilt
			dewarpingTransform.Pan = dewarpingTransform2.Pan + relPan;
			dewarpingTransform.Tilt = dewarpingTransform2.Tilt + relTilt;

			// for momentum effect
			panSpeed = 4 * (relPan - lastRelPan);
			tiltSpeed = 4 * (relTilt - lastRelTilt);
			lastRelPan = relPan;
			lastRelTilt = relTilt;
		}

		private void UpdatePanTiltLabel(int x, int y)
		{
			double pan, tilt;
			double normX = x / (double)videoPanel.Width;
			double normY = y / (double)videoPanel.Height;
			dewarpingTransform.MapPoint(normX, normY, out pan, out tilt, 0);
			panTiltLabel.Text = String.Format("Pan: {0:#}, Tilt: {1:#}", pan, tilt);
		}

		private void videoPanel_MouseUp(object sender, MouseEventArgs e)
		{
			if (!CanMapPoint()) return;

			if (isMouseButtonPressed)
			{
				lastRelPan = 0;
				lastRelTilt = 0;
				isMouseButtonPressed = false;

				if (e.Button == MouseButtons.Middle)
				{
					// go to overview
					zoomSpeed = -10;
				}
				else if (e.X == mouseAnchorPoint.X && e.Y == mouseAnchorPoint.Y)
				{
					// single click
					if (panSpeed != 0 || tiltSpeed != 0 || zoomSpeed != 0)
					{
						// moving - stop animation
						panSpeed = 0;
						tiltSpeed = 0;
						zoomSpeed = 0;
					}
					else
					{
						bool isOverview = dewarpingTransform.EffectLevel == 0.0;
						double pan, tilt;
						double normX = mouseAnchorPoint.X / (double)videoPanel.Width;
						double normY = mouseAnchorPoint.Y / (double)videoPanel.Height;
						if (isOverview)
						{
							// zoom in
							dewarpingTransform.MapPoint(normX, normY, out pan, out tilt, 0);
							dewarpingTransform.Pan = pan;
							dewarpingTransform.Tilt = tilt;
							zoomSpeed = 4;
						}
						else
						{
							// go to position
							dewarpingTransform.MapPoint(normX, normY, out pan, out tilt, 0);
							dewarpingTransform.SetPTZPosition(pan, tilt, dewarpingTransform.FieldOfView, false);
						}
					}
				}
			}
		}

		/// <summary>
		/// Perform zoom operation when user scrolls the mouse wheel over image
		/// </summary>
		void videoPanel_MouseWheel(object sender, MouseEventArgs e)
		{
			if (e.Delta > 0 && dewarpingTransform.EffectLevel == 0)
			{
				// started zooming in from overview
				// fixed angle for ceiling/desk, use cursor position for wall
				switch ((TiltOrientation)dewarpingTransform.TiltOrientation)
				{
					case TiltOrientation.ceiling:
						dewarpingTransform.Tilt = -defaultTiltAngle;
						break;

					case TiltOrientation.desk:
						dewarpingTransform.Tilt = defaultTiltAngle;
						break;

					case TiltOrientation.wall:
						double zoomPan;
						double zoomTilt;
						double normX = e.X / (double)videoPanel.Width;
						double normY = e.Y / (double)videoPanel.Height;
					  dewarpingTransform.MapPoint(normX, normY, out zoomPan, out zoomTilt, 0);
						dewarpingTransform.Pan = zoomPan;
						dewarpingTransform.Tilt = zoomTilt;
						break;
				}
			}

			double zoomFactor = e.Delta / 100.0;
			zoomFactor *= 0.5;
			Zoom(zoomFactor);
			zoomSpeed = 0.5 * zoomFactor;
		}

		void timer_Tick(object sender, EventArgs e)
		{
			if (!isMouseButtonPressed)
			{
				// Momentum effect
				if (panSpeed != 0 || tiltSpeed != 0 || zoomSpeed != 0)
				{
					dewarpingTransform.Pan += panSpeed;
					dewarpingTransform.Tilt += tiltSpeed;
					Zoom(zoomSpeed);
					panSpeed /= dampingFactor;
					tiltSpeed /= dampingFactor;
					zoomSpeed /= dampingFactor;
					if (Math.Abs(panSpeed) < 0.1) panSpeed = 0.0;
					if (Math.Abs(tiltSpeed) < 0.1) tiltSpeed = 0.0;
					if (Math.Abs(zoomSpeed) < 0.1) zoomSpeed = 0.0;
					if (IsOverview() && zoomSpeed < 0) zoomSpeed = 0.0;
				}
			}
		}


		#region Windows Form Designer generated code

		// Required designer variable.
		private System.ComponentModel.IContainer components = null;

		// Clean up any resources being used.
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}


		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.videoPanel = new DewarpDirectShow.SelectablePanel();
			this.panTiltLabel = new System.Windows.Forms.Label();
			this.videoPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// videoPanel
			// 
			this.videoPanel.Controls.Add(this.panTiltLabel);
			this.videoPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.videoPanel.Location = new System.Drawing.Point(0, 0);
			this.videoPanel.Name = "videoPanel";
			this.videoPanel.Size = new System.Drawing.Size(577, 243);
			this.videoPanel.TabIndex = 23;
			this.videoPanel.TabStop = true;
			this.videoPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.videoPanel_Paint);
			this.videoPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.videoPanel_MouseDown);
			this.videoPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.videoPanel_MouseMove);
			this.videoPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.videoPanel_MouseUp);
			this.videoPanel.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.videoPanel_MouseWheel);
			this.videoPanel.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.videoPanel_PreviewKeyDown);
			// 
			// panTiltLabel
			// 
			this.panTiltLabel.AutoSize = true;
			this.panTiltLabel.Location = new System.Drawing.Point(4, 4);
			this.panTiltLabel.Name = "panTiltLabel";
			this.panTiltLabel.Size = new System.Drawing.Size(0, 13);
			this.panTiltLabel.TabIndex = 0;
			// 
			// DewarpingVideoForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(577, 243);
			this.Controls.Add(this.videoPanel);
			this.Name = "DewarpingVideoForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Dewarping Video Form (D=Dewarping ON/OFF;  Arrow-keys=Pan/Tilt; +/-=Zoom Level)";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VideoForm_FormClosing);
			this.Load += new System.EventHandler(this.VideoForm_Load);
			this.Resize += new System.EventHandler(this.VideoForm_Resize);
			this.videoPanel.ResumeLayout(false);
			this.videoPanel.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion


	}
}