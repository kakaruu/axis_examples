using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using AxisMediaViewerLib;
using AxisVideoTransformLib;
using DirectShowLib;
using DirectShowLib.DMO;

namespace DewarpDirectShow
{
	/// <summary>
	/// 
	/// This sample shows how to use the AXIS Dewarping Transform DMO as a DirectShow filter 
	/// by using the Windows DMO Wrapper Filter.
	/// For simplicity AXIS Media Viewer is used to abstract the construction
	/// of the actual DirectShow graph, but the sample code will show how to 
	/// wrap the DMO and control it.
	/// 
	/// Dewarping control keys:
	/// D = Dewarping ON/OFF
	/// 1-5 = View type
	/// Arrow-keys = Pan/Tilt
	/// +/- = Zoom Level
	/// SHIFT + right/left = Panorama field-of-view
	/// 
	/// </summary>
	class DewarpingVideoForm : Form
	{
		// Sample video clips (change to test different camera mount configurations)

		/// <summary>
		/// Desk mount
		/// </summary>
		//private const TiltOrientation tiltOrientation = TiltOrientation.desk;
		//private const string filePath = "sample_overview_desk.H264.bin";

		/// <summary>
		/// Wall mount
		/// </summary>
		//private const TiltOrientation tiltOrientation = TiltOrientation.wall;
		//private const string filePath = "sample_overview_wall.H264.bin";

		/// <summary>
		/// Ceiling mount
		/// </summary>
		private const TiltOrientation tiltOrientation = TiltOrientation.ceiling;
		private const string filePath = "sample_overview_ceiling.H264.bin";


		private AmpFileParser fileParser;
		private AxisMediaViewer viewer;

		private IDewarpingTransform dewarpingTransform;

		// The render thread that parses the file and displays each frame
		private Thread renderThread;

		bool repeat = true;
		private bool doExitRenderThread = false;

		// Use panel derivative to receive arrow key events
		private SelectablePanel videoPanel;


		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new DewarpingVideoForm());
		}


		public DewarpingVideoForm()
		{
			InitializeComponent();
			fileParser = new AmpFileParser(filePath);
			this.SetupViewer(this.videoPanel.Handle);
		}

		private void SetupViewer(IntPtr windowHandle)
		{
			// Create the AXIS Media Viewer COM object in a MTA apartment to allow multiple
			// threads (UI and render thread) to access COM object (by default tasks use MTA threads).
			Task.Factory.StartNew(() =>
			{
				try
				{
					viewer = new AxisMediaViewer();

					// Set some media properties
					viewer.VMR9 = true;
					viewer.PlaybackRate = 1.0;
					viewer.LiveMode = false;
					viewer.EnableOnDecodedImage = false;

					// Init the Viewer with media type buffer and a win32 hWnd handle to the window. 
					viewer.Init(0, fileParser.MediaTypeBuffer, windowHandle.ToInt64());
				}
				catch (System.Exception ex)
				{
					// Install AXIS Media Parser SDK: http://www.axis.com/partner_pages/media_parser.php
					MessageBox.Show("Failed to initialize AXIS Media Viewer:\n" + ex.Message);
					return;
				}

				// Make Media Viewer to inject AXIS Dewarping Transform filter in media graph
				try
				{
					InitDewarpingTransform();
					viewer.AddFilter(dewarpingTransform, "Dewarping", 0);
				}
				catch (System.Exception ex)
				{
					MessageBox.Show("Failed to add AXIS Dewarping Transform (AxisVideoTransform.dll):\n" +
						ex.Message);
				}
			}).Wait();
		}

		private void InitDewarpingTransform()
		{
			// Use Microsofts DMO wrapper filter to utilize dewarping DMO in DirectShow
			IDMOWrapperFilter wrapperFilter = new DMOWrapperFilter() as IDMOWrapperFilter;
			if (wrapperFilter == null)
			{
				throw new Exception("Could not load DMO wrapper");
			}

			int hr = wrapperFilter.Init(typeof(DewarpingTransformClass).GUID, DMOCategory.VideoEffect);
			DsError.ThrowExceptionForHR(hr);

			// Get the dewarping control interface
			dewarpingTransform = (IDewarpingTransform)wrapperFilter;

			// Set lens profile and tilt orientation
			// Default lens profile for AXIS M3007/27
			//dewarpingTransform.LensProfile = new double[] { -0.022618674, 0.014994084, 0.368175 };
			dewarpingTransform.TiltOrientation = (int)tiltOrientation;

			// Start in overview mode
			// dewarpingTransform.Bypass = true;  

			// Set custom output video resolution (default is 1024x768)
			dewarpingTransform.OutputWidth = 1280;

			// Initial view type - determines output aspect ratio if not explicit
			//dewarpingTransform.PanoramaFieldOfView = 360;
			//dewarpingTransform.ViewType = (int)ViewType.panoramaSingle;
		}

		private void DestroyViewer()
		{
			if (viewer != null)
			{
				// Inform the GC that COM object no longer will be used
				Marshal.FinalReleaseComObject(viewer);
				viewer = null;
			}
		}

		private void Play()
		{
			viewer.Start();
			StartRenderThread();
		}

		private void StartRenderThread()
		{
			if (renderThread == null)
			{
				// Create the render thread that will read file and render frames
				renderThread = new Thread(new ThreadStart(RenderThread));
				renderThread.Start();
			}
		}

		private void StopRenderThread()
		{
			if (renderThread != null)
			{
				doExitRenderThread = true;
				renderThread.Join();
				doExitRenderThread = false;
				renderThread = null;
			}
		}

		/// <summary>
		/// A thread that reads a binary media file and renders the frames in the viewer object.
		/// </summary>
		private void RenderThread()
		{
			try
			{
				// While there are frames in the in file, read each frame and let the viewer render it
				while (!doExitRenderThread)
				{
					AmpFileSample sample = fileParser.ReadSample();
					if (sample == null)
					{
						// end-of-file reached - wait for all samples to render before stopping viewer
						if (viewer.Flush())
						{
							if (repeat)
							{
								viewer.Stop();
								long pos = 0;
								fileParser.Seek(ref pos);
								viewer.Start();
								continue;
							}
							else
							{
								break;
							}
						}
					}
					else
					{
						switch (sample.SampleType)
						{
							case (int)AMV_VIDEO_SAMPLE_TYPE.AMV_VST_MPEG4_VIDEO_CONFIG:
							case (int)AMV_VIDEO_SAMPLE_TYPE.AMV_VST_MPEG4_VIDEO_IVOP:
							case (int)AMV_VIDEO_SAMPLE_TYPE.AMV_VST_MPEG4_VIDEO_PVOP:
							case (int)AMV_VIDEO_SAMPLE_TYPE.AMV_VST_MPEG4_VIDEO_BVOP:
							case (int)AMV_VIDEO_SAMPLE_TYPE.AMV_VST_MPEG4_VIDEO_FRAGMENT:
							case (int)AMV_VIDEO_SAMPLE_TYPE.AMV_VST_MJPEG_VIDEO:
							case (int)AMV_VIDEO_SAMPLE_TYPE.AMV_VST_H264_VIDEO_CONFIG:
							case (int)AMV_VIDEO_SAMPLE_TYPE.AMV_VST_H264_VIDEO_IDR:
							case (int)AMV_VIDEO_SAMPLE_TYPE.AMV_VST_H264_VIDEO_NON_IDR:
							case (int)AMV_VIDEO_SAMPLE_TYPE.AMV_VST_H264_VIDEO_SEI:
								if ((viewer.PlayOptions & (int)AMV_PLAY_OPTIONS.AMV_PO_VIDEO) > 0)
								{
									if (viewer.PlaybackRate >= 32.0 &&
										((int)AMV_SAMPLE.AMV_S_SYNCPOINT & sample.SampleFlags) == 0)
									{
										// skip non-sync point video if playback rate is x32 or higher
										continue;
									}

									viewer.RenderVideoSample(sample.SampleFlags, sample.StartTime,
										sample.StopTime, sample.Buffer);
								}
								break;
							case (int)AMV_VIDEO_SAMPLE_TYPE.AMV_VST_MPEG4_AUDIO:
								if ((viewer.PlayOptions & (int)AMV_PLAY_OPTIONS.AMV_PO_AUDIO) > 0)
								{
									viewer.RenderAudioSample(sample.SampleFlags, sample.StartTime,
										sample.StopTime, sample.Buffer);
								}
								break;
							default:
								throw new Exception("Unknown sample type");
						}
					}
				}
			}
			catch (COMException e)
			{
				MessageBox.Show(string.Format("Exception for {0}, {1}", filePath, e.Message));
			}
		}

		private void TakeSnapshot()
		{
			object imageData;
			long timestamp;

			// Returns BITMAPINFOHEADER + pixel data
			dewarpingTransform.GetCurrentImage(out imageData, out timestamp, 0);
			byte[] imageBuffer = (byte[])imageData;

			using(MemoryStream stream = new MemoryStream(imageBuffer.Length + 14))
			using (BinaryWriter writer = new BinaryWriter(stream))
			{
				// Add BITMAPFILEHEADER (14 bytes)
				writer.Write('B'); // bfType
				writer.Write('M');
				writer.Write(imageBuffer.Length + 14); // bfSize
				writer.Write((short)0); // bfReserved1
				writer.Write((short)0); // bfReserved2
				writer.Write(imageBuffer[0] + 14); // bfOffBits (imageBuffer[0] = BITMAPINFOHEADER::biSize)
				
				// Write image data
				writer.Write(imageBuffer, 0, imageBuffer.Length);

				Bitmap bitmap = (Bitmap)Image.FromStream(stream);
				bitmap.Save("Snapshot.jpg", ImageFormat.Jpeg);
			}
		}

		private bool IsOverview()
		{
			return dewarpingTransform.Bypass;
		}

		private void Zoom(double zoomFactor)
		{
			if (IsOverview() && zoomFactor > 0)
			{
				// Goto dewarped view
				dewarpingTransform.Bypass = false; // dewarped view
				dewarpingTransform.FieldOfView = 45;
			}
			else if (dewarpingTransform.FieldOfView - zoomFactor > 45)
			{
				// Goto overview
				dewarpingTransform.Bypass = true; // overview
			}
			else
			{
				dewarpingTransform.FieldOfView -= zoomFactor;
			}
		}

		private bool CanMapPoint()
		{
			// Point mapping is currently not supported for quad and panorama views
			return dewarpingTransform.ViewType == (int)ViewType.single ||
				 dewarpingTransform.ViewType == (int)ViewType.singleOverview;
		}

		/// <summary>
		/// This event handler is called after the dialog is shown, since we need the
		/// win32 window handle as a parameter to the Viewer
		/// </summary>
		private void VideoForm_Load(object sender, EventArgs e)
		{
			// Get the dewarped video output frame size and resize the form to fit the video stream
			ClientSize = new System.Drawing.Size(
				dewarpingTransform.OutputWidth + ClientSize.Width - videoPanel.Width,
				dewarpingTransform.OutputHeight + ClientSize.Height - videoPanel.Height);

			// Auto start
			Play();
		}

		private void VideoForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			StopRenderThread();
			DestroyViewer();
			fileParser.Close();
		}

		/// <summary>
		/// Event called when the dialog is resized, the viewer is informed about the new size
		/// </summary>
		private void VideoForm_Resize(object sender, EventArgs e)
		{
			if (viewer != null)
			{
				viewer.SetVideoPosition(0, 0, videoPanel.Width, videoPanel.Height);
			}
		}		

		private void videoPanel_Paint(object sender, PaintEventArgs e)
		{
			if (viewer != null)
			{
				viewer.RepaintVideo();
			}
		}

		private void videoPanel_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
		{
			// Global commands
			switch (e.KeyCode)
			{
				case Keys.D:
					dewarpingTransform.Bypass = !dewarpingTransform.Bypass;
					break;

				// 1-5 views
				case Keys.D1:
				case Keys.NumPad1:
					dewarpingTransform.ViewType = (int)ViewType.single;
					dewarpingTransform.SelectedView = (int)AxisVideoTransformLib.View.all;
					break;
				case Keys.D2:
				case Keys.NumPad2:
					dewarpingTransform.ViewType = (int)ViewType.quad;
					dewarpingTransform.Bypass = false;
					// views in quad can be controlled individually by setting SelectedView (click on them)
					dewarpingTransform.SelectedView = (int)AxisVideoTransformLib.View.northWest;
					//dewarpingTransform.SelectedView = (int)AxisVideoTransformLib.View.all;
					break;
				case Keys.D3:
				case Keys.NumPad3:
					dewarpingTransform.ViewType = (int)ViewType.panoramaSingle;
					dewarpingTransform.Bypass = false;
					break;
				case Keys.D4:
				case Keys.NumPad4:
					dewarpingTransform.ViewType = (int)ViewType.panoramaDouble;
					dewarpingTransform.Bypass = false;
					break;
				case Keys.D5:
				case Keys.NumPad5:
					dewarpingTransform.ViewType = (int)ViewType.singleOverview;
					dewarpingTransform.SelectedView = (int)AxisVideoTransformLib.View.all;
					break;
				case Keys.S:
					TakeSnapshot();
					break;
			}

			// PTZ control for single and quad view
			if (dewarpingTransform.ViewType == (int)ViewType.single ||
					dewarpingTransform.ViewType == (int)ViewType.singleOverview ||
					dewarpingTransform.ViewType == (int)ViewType.quad)
			{
				switch (e.KeyCode)
				{
					case Keys.Left:
						dewarpingTransform.Pan -= 1.0;
						break;
					case Keys.Right:
						dewarpingTransform.Pan += 1.0;
						break;
					case Keys.Up:
						dewarpingTransform.Tilt += 1.0;
						break;
					case Keys.Down:
						dewarpingTransform.Tilt -= 1.0;
						break;

					case Keys.Add:
						Zoom(1.0);
						break;
					case Keys.Subtract:
						Zoom(-1.0);
						break;
				}

			}
			else if(dewarpingTransform.ViewType == (int)ViewType.panoramaSingle ||
							dewarpingTransform.ViewType == (int)ViewType.panoramaDouble)
			{
				switch (e.KeyCode)
				{
					case Keys.Left:
						if (e.Shift)
						{
							dewarpingTransform.PanoramaFieldOfView -= 1.0;
						}
						else
						{
							dewarpingTransform.PanoramaPanOffset -= 1.0;
						}

						break;
					case Keys.Right:
						if (e.Shift)
						{
							dewarpingTransform.PanoramaFieldOfView += 1.0;
						}
						else
						{
							dewarpingTransform.PanoramaPanOffset += 1.0;
						}

						break;
				}
			}
		}

		/// <summary>
		/// Center view at the click position
		/// </summary>
		private void videoPanel_MouseDown(object sender, MouseEventArgs e)
		{
			double normX = e.X / (double)videoPanel.Width;
			double normY = e.Y / (double)videoPanel.Height;
			if (!CanMapPoint())
			{
				if (normY < 0.5)
				{
					if (normX < 0.5)
					{
						dewarpingTransform.SelectedView = (int)AxisVideoTransformLib.View.northWest;
					}
					else
					{
						dewarpingTransform.SelectedView = (int)AxisVideoTransformLib.View.northEast;
					}
				}
				else
				{
					if (normX < 0.5)
					{
						dewarpingTransform.SelectedView = (int)AxisVideoTransformLib.View.southWest;
					}
					else
					{
						dewarpingTransform.SelectedView = (int)AxisVideoTransformLib.View.southEast;
					}
				}

				return;
			}

			double pan, tilt;
			dewarpingTransform.MapPoint(normX, normY, out pan, out tilt, 0);

			dewarpingTransform.Pan = pan;
			dewarpingTransform.Tilt = tilt;
		}
		
		/// <summary>
		/// Perform zoom operation when user scrolls the mouse wheel over image
		/// </summary>
		void videoPanel_MouseWheel(object sender, MouseEventArgs e)
		{
			if (!CanMapPoint()) return;

			double zoomFactor = e.Delta / 100.0;
			double normX = e.X / (double)videoPanel.Width;
			double normY = e.Y / (double)videoPanel.Height;

			// calculate pan/tilt for zoom point
			double zoomPointPan, zoomPointTilt;
			dewarpingTransform.MapPoint(normX, normY, out zoomPointPan, out zoomPointTilt, 0);
			
			if (IsOverview() && (zoomFactor > 0))
			{
				// go from overview to dewarped view at cursor position
				dewarpingTransform.Pan = zoomPointPan;
				dewarpingTransform.Tilt = zoomPointTilt;
				Zoom(zoomFactor);

				// compensate pan/tilt so that cursor stays over the desired position
				double newZoomPointPan, newZoomPointTilt;
				dewarpingTransform.MapPoint(normX, normY,
					out newZoomPointPan, out newZoomPointTilt,
					(int)PointMapFlag.ignoreOverviewMap); // map to points behind  overview map (view mode #5)
				
				dewarpingTransform.Pan += (zoomPointPan - newZoomPointPan);
				dewarpingTransform.Tilt += (zoomPointTilt - newZoomPointTilt);
			}
			else
			{
				// zoom in/out at current position
				Zoom(zoomFactor);

				// compensate pan/tilt error due to zoom operation in order to keep zoom-point position
				double newZoomPointPan, newZoomPointTilt;
				dewarpingTransform.MapPoint(normX, normY, out newZoomPointPan, out newZoomPointTilt, 0);

				dewarpingTransform.Pan += (zoomPointPan - newZoomPointPan);
				dewarpingTransform.Tilt += (zoomPointTilt - newZoomPointTilt);
			}
		}


		#region Windows Form Designer generated code

		// Required designer variable.
		private System.ComponentModel.IContainer components = null;

		// Clean up any resources being used.
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}


		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.videoPanel = new DewarpDirectShow.SelectablePanel();
			this.SuspendLayout();
			// 
			// videoPanel
			// 
			this.videoPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.videoPanel.Location = new System.Drawing.Point(0, 0);
			this.videoPanel.Name = "videoPanel";
			this.videoPanel.Size = new System.Drawing.Size(634, 452);
			this.videoPanel.TabIndex = 23;
			this.videoPanel.TabStop = true;
			this.videoPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.videoPanel_Paint);
			this.videoPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.videoPanel_MouseDown);
			this.videoPanel.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.videoPanel_MouseWheel);
			this.videoPanel.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.videoPanel_PreviewKeyDown);
			// 
			// DewarpingVideoForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(634, 452);
			this.Controls.Add(this.videoPanel);
			this.Name = "DewarpingVideoForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Dewarping Video Form (D=Dewarping ON/OFF; S=Snapshot; 1-5=View type; Arrow-keys=Pan/Tilt; +/-" +
    "=Zoom Level)";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.VideoForm_FormClosing);
			this.Load += new System.EventHandler(this.VideoForm_Load);
			this.Resize += new System.EventHandler(this.VideoForm_Resize);
			this.ResumeLayout(false);

		}

		#endregion


	}
}