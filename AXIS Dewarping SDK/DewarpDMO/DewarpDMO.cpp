// DewarpDMO.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "DewarpDMO.h"

// Sample media files with different camera mount configurations
//InputMedia Input = {L"sample_overview_desk.H264.bin", AxisVideoTransformLib::desk, {-0.022618674f, 0.014994084f, 0.36817500f}};
//InputMedia Input = {L"sample_overview_wall.H264.bin", AxisVideoTransformLib::wall, {-0.022618674f, 0.014994084f, 0.36817500f}};
InputMedia Input = {L"sample_overview_ceiling.H264.bin", AxisVideoTransformLib::ceiling, {-0.022618674f, 0.014994084f, 0.36817500f}};


BEGIN_MESSAGE_MAP(CDewarpDMOApp, CWinApp)
END_MESSAGE_MAP()

CDewarpDMOApp::CDewarpDMOApp()
{
}


// The one and only CDewarpDMOApp object
CDewarpDMOApp theApp;


BOOL CDewarpDMOApp::InitInstance()
{
  // InitCommonControlsEx() is required on Windows XP if an application
  // manifest specifies use of ComCtl32.dll version 6 or later to enable
  // visual styles.  Otherwise, any window creation will fail.
  INITCOMMONCONTROLSEX InitCtrls;
  InitCtrls.dwSize = sizeof(InitCtrls);

  // Set this to include all the common control classes you want to use
  // in your application.
  InitCtrls.dwICC = ICC_WIN95_CLASSES;
  InitCommonControlsEx(&InitCtrls);

  CWinApp::InitInstance();

  // Initialize COM for main thread
  CoInitializeEx(NULL, COINIT_MULTITHREADED);

  {
    // Set file name to video dialog
    CDewarpDMODlg videoDialog(Input.fileName, Input.lensProfileData, Input.tiltOrientation);

    // Display video dialog and start viewer
    m_pMainWnd = &videoDialog;
    INT_PTR nResponse = videoDialog.DoModal();
    if (nResponse == IDOK)
    {
      // TODO: Place code here to handle when the dialog is
      //  dismissed with OK
    }
    else if (nResponse == IDCANCEL)
    {
      // TODO: Place code here to handle when the dialog is
      //  dismissed with Cancel
    }
  }

  // Uninitialize COM for main thread
  ::CoUninitialize();

  // Since the dialog has been closed, return FALSE so that we exit the
  //  application, rather than start the application's message pump.
  return FALSE;
}
