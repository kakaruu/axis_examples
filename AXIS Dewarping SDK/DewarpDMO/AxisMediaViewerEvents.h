// Machine generated IDispatch wrapper class(es) created with Add Class from Typelib Wizard


#import "AxisMediaViewer.dll" no_namespace


// CAxisMediaViewerEvents wrapper class

class CAxisMediaViewerEvents : public COleDispatchDriver
{
public:
  CAxisMediaViewerEvents(){} // Calls COleDispatchDriver default constructor
  CAxisMediaViewerEvents(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
  CAxisMediaViewerEvents(const CAxisMediaViewerEvents& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

  // Attributes
public:

  // Operations
public:


  // IAxisMediaViewerEvents methods
public:
  void OnDecodedImage(unsigned __int64 StartTime, short ColorSpace, VARIANT& SampleArray)
  {
    static BYTE parms[] = VTS_UI8 VTS_I2 VTS_VARIANT ;
    InvokeHelper(0x64, DISPATCH_METHOD, VT_EMPTY, NULL, parms, StartTime, ColorSpace, &SampleArray);
  }
  void OnDecodedImage32(long StartTimeLow, long StartTimeHigh, short ColorSpace, VARIANT& SampleArray)
  {
    static BYTE parms[] = VTS_I4 VTS_I4 VTS_I2 VTS_VARIANT ;
    InvokeHelper(0x65, DISPATCH_METHOD, VT_EMPTY, NULL, parms, StartTimeLow, StartTimeHigh, ColorSpace, &SampleArray);
  }
  void OnMediaPosition(__int64 CurrentPosition)
  {
    static BYTE parms[] = VTS_I8 ;
    InvokeHelper(0x66, DISPATCH_METHOD, VT_EMPTY, NULL, parms, CurrentPosition);
  }
  void OnMediaPosition32(long CurrentPositionLow, long CurrentPositionHigh)
  {
    static BYTE parms[] = VTS_I4 VTS_I4 ;
    InvokeHelper(0x67, DISPATCH_METHOD, VT_EMPTY, NULL, parms, CurrentPositionLow, CurrentPositionHigh);
  }

  // IAxisMediaViewerEvents properties
public:

};
