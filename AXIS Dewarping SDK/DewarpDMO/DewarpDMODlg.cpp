// DewarpDMODlg.cpp : implementation file
//

#include "stdafx.h"
#include <afxctl.h>
#include <fstream>
#include <uuids.h>
#include <Amvideo.h>
#include <Dmo.h>
#include "DewarpDMO.h"
#include "DewarpDMODlg.h"

// DMO libs
#pragma comment(lib, "strmiids.lib")
#pragma comment(lib, "dmoguids.lib")
#pragma comment(lib, "msdmo.lib")

// Input/output formats
// NOTE: AMV_CS_YV12 requires H.264 decoder 4.2.0.0 or higher
#define INPUT_VIDEO_FORMAT AMV_CS_YUY2 //AMV_CS_RGB32 //AMV_CS_YV12
#define OUTPUT_VIDEO_FORMAT MEDIASUBTYPE_RGB32 //MEDIASUBTYPE_YV12

const DOUBLE PI = 3.1415926535897932384626433832795F;
#define DegToRad(deg) (((deg) * PI)/180.0F)
#define RadToDeg(rad) (((rad) * 180.0F)/PI)
#define IsInRange(val, low, high) ((val) >= (low) && (val) <= (high))

// CDewarpDMODlg dialog

IMPLEMENT_DYNAMIC(CDewarpDMODlg, CDialogEx)

  CDewarpDMODlg::CDewarpDMODlg(LPCTSTR theFileName,
  const LensProfileData& theLensProfileData,
  const AxisVideoTransformLib::TiltOrientation theTiltOrientation)
  : CDialogEx(CDewarpDMODlg::IDD, NULL)
  , m_fileName(theFileName)
  , doRepeatPlayback(true)
  , m_inputColorSpace(AMV_CS_NULL)
  , m_dewarpingTransform(NULL)
  , m_mediaObject(NULL)
  , m_outputBuffer(NULL)
{
  if(FAILED(InitDewarpingDMO(theLensProfileData, theTiltOrientation)))
  {
    AfxMessageBox(_T("Failed to create Dewarping Transform DMO!"));
  }

  // Enable OLE automation
  EnableAutomation();

  // Initialize all class member variables
  m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
  m_hFile = NULL;
  m_bAbort = false;
}

CDewarpDMODlg::~CDewarpDMODlg()
{
}

BEGIN_MESSAGE_MAP(CDewarpDMODlg, CDialogEx)
  ON_WM_PAINT()
  ON_WM_SIZE()
  ON_WM_DESTROY()
  ON_WM_GETDLGCODE()
  ON_WM_KEYDOWN()
  ON_WM_LBUTTONDOWN()
  ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CDewarpDMODlg, CDialogEx)     
  DISP_FUNCTION_ID(CDewarpDMODlg,"OnDecodedImage",0x64,OnDecodedImage,VT_EMPTY,VTS_UI8 VTS_I2 VTS_VARIANT)
END_DISPATCH_MAP()

BEGIN_INTERFACE_MAP(CDewarpDMODlg, CDialogEx)
  INTERFACE_PART(CDewarpDMODlg, __uuidof(IAxisMediaViewerEvents), Dispatch)
END_INTERFACE_MAP()

UINT CDewarpDMODlg::ThreadInit(LPVOID pParam)
{
  CDewarpDMODlg* dlg = reinterpret_cast<CDewarpDMODlg*>(pParam);
  dlg->Thread();

  return 0;
}

/// Thread() will read all frame data from input file and send them to the viewer to be rendered.
UINT CDewarpDMODlg::Thread()
{
  // Define local variables
  COleSafeArray SafeArray;
  VARIANT varBuffer;
  DWORD dwSampleType;
  DWORD dwFlags;
  UINT64 dw64StartTime;
  UINT64 dw64StopTime;
  DWORD dwBufferSize;
  BYTE* pBuffer;
  DWORD dwReturn;

  // Setup initial buffer for our MFC helper class
  SafeArray.CreateOneDim(VT_UI1, 1);

  // Initialize COM for this thread
  ::CoInitializeEx(NULL, COINIT_MULTITHREADED);

  // Loop through all frames found in our input file
  try
  {
    while (!m_bAbort)
    {
      // Read frame data from in file
      ReadFile(m_hFile, &dwSampleType, 4, &dwReturn, NULL);
      if (dwReturn != 4)
      {
        // end-of-file
        if(m_viewer.Flush())
        {
          if(doRepeatPlayback)
          {
            m_viewer.Stop();
            SetFilePointer(m_hFile, m_mediaDataFilePosition, 0, FILE_BEGIN);
            m_viewer.Start();
            continue;
          }
        }

        break;
      }

      // Read headers from file
      ReadFile(m_hFile, &dwFlags, 4, &dwReturn, NULL);
      ReadFile(m_hFile, &dw64StartTime, 8, &dwReturn, NULL);
      ReadFile(m_hFile, &dw64StopTime, 8, &dwReturn, NULL);
      ReadFile(m_hFile, &dwBufferSize, 4, &dwReturn, NULL);

      // Render video as fast as possible
      //dw64StartTime = 0;

      // Setup our MFC helper class and read frame data from file
      SafeArray.ResizeOneDim(dwBufferSize);
      SafeArray.AccessData((void**) &pBuffer);
      ReadFile(m_hFile, pBuffer, dwBufferSize, &dwReturn, NULL);
      SafeArray.UnaccessData();

      // Render video and audio samples retrieved from input file
      varBuffer = SafeArray.Detach();

      switch (dwSampleType)
      {
      case AMV_VST_MPEG4_VIDEO_CONFIG:
      case AMV_VST_MPEG4_VIDEO_IVOP:
      case AMV_VST_MPEG4_VIDEO_PVOP:
      case AMV_VST_MPEG4_VIDEO_BVOP:
      case AMV_VST_MPEG4_VIDEO_FRAGMENT:
      case AMV_VST_MJPEG_VIDEO:
      case AMV_VST_H264_VIDEO_CONFIG:
      case AMV_VST_H264_VIDEO_IDR:
      case AMV_VST_H264_VIDEO_NON_IDR:
      case AMV_VST_H264_VIDEO_SEI:
        if ((m_viewer.GetPlayOptions() & AMV_PO_VIDEO) > 0)
        {
          m_viewer.RenderVideoSample(dwFlags, dw64StartTime, dw64StopTime, varBuffer);
        }
        break;
      case AMV_VST_MPEG4_AUDIO:
        if ((m_viewer.GetPlayOptions() & AMV_PO_AUDIO) > 0)
        {
          m_viewer.RenderAudioSample(dwFlags, dw64StartTime, dw64StopTime, varBuffer);
        }
        break;
      default:
        AfxMessageBox(_T("Unsupported sample type"));
        break;
      }

      SafeArray.Attach(varBuffer);
    }
  }
  catch (COleDispatchException* e)
  {
    AfxMessageBox(e->m_strDescription);
    e->Delete();
  }

  // Uninitialize COM for this thread
  ::CoUninitialize();

  // Release all waiting threads
  m_ThreadEvent.SetEvent();

  return 0;
}

HRESULT CDewarpDMODlg::Setup()
{
  // Define local variables
  COleSafeArray SafeArray;
  VARIANT varMediaType;
  DWORD dwReturn;
  DWORD dwBufferSize;
  BYTE* pBuffer;
  CRect rcWnd;
  CRect rcClient;

  // Create the AXIS Media Viewer COM object
  if (!m_viewer.CreateDispatch(_T("AxisMediaViewerLib.AxisMediaViewer")))
  {
    // Install AXIS Media Parser SDK: http://www.axis.com/partner_pages/media_parser.php
    AfxMessageBox(_T("Failed to create dispatch for AxisMediaViewer!"));
    return E_FAIL;
  }

  // Get a pointer to IUnknown for our event handler class
  IUnknown* pUnkCallback = this->GetIDispatch(false);

  // Establish a connection between viewer and event handlers
  AfxConnectionAdvise(m_viewer, __uuidof(IAxisMediaViewerEvents), pUnkCallback, false, &m_dwCallbackCookie);

  // Open input file containing data stream
  m_hFile = CreateFile(m_fileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
  if (m_hFile == INVALID_HANDLE_VALUE)
  {
    AfxMessageBox(_T("Failed to open file!"));
    return E_FAIL;
  }

  // Read media type data describing data stream
  ReadFile(m_hFile, &dwBufferSize, 4, &dwReturn, NULL);
  SafeArray.CreateOneDim(VT_UI1, dwBufferSize);
  SafeArray.AccessData((void**) &pBuffer);
  ReadFile(m_hFile, pBuffer, dwBufferSize, &dwReturn, NULL);
  SafeArray.UnaccessData();
  m_mediaDataFilePosition = 4 + dwBufferSize;

  try
  {
    // Setup our COM object properties
    m_viewer.SetPlayOptions(AMV_PO_VIDEO);
    m_viewer.SetColorSpace(INPUT_VIDEO_FORMAT);

    // Initialize our viewer _without_ internal rendering.
    // Decoded image data will be delivered from OnDecodedImage event.
    varMediaType = SafeArray.Detach();
    m_viewer.Init(0, varMediaType, NULL /*no internal rendering*/);
    SafeArray.Attach(varMediaType);

    // Get video size in pixels from our viewer
    m_viewer.GetVideoSize(&m_inputVideoWidth, &m_inputVideoHeight);

    // Start our viewer
    m_viewer.Start();
  }
  catch (COleDispatchException* e)
  {
    AfxMessageBox(e->m_strDescription);
    e->Delete();
    return E_FAIL;
  }

  return S_OK;
}

void CDewarpDMODlg::SetWindowSize()
{
  CRect rcWnd;
  CRect rcClient;

  if(m_dewarpingTransform->OutputWidth > 0)
  {
    // Adjust size of dialog to fit video size and center it on screen
    GetWindowRect(&rcWnd);
    GetClientRect(&rcClient);
    MoveWindow(0, 0,
      rcWnd.Width() - rcClient.Width() + m_dewarpingTransform->OutputWidth,
      rcWnd.Height() - rcClient.Height() + m_dewarpingTransform->OutputHeight, FALSE);
    CenterWindow();
  }
}

BOOL CDewarpDMODlg::OnInitDialog()
{
  CDialogEx::OnInitDialog();

  SetIcon(m_hIcon, TRUE);			// Set big icon
  SetIcon(m_hIcon, FALSE);		// Set small icon

  // Setup our viewer
  if (FAILED(Setup()))
  {
    m_ThreadEvent.SetEvent();
    EndDialog(IDCANCEL);
    return TRUE;
  }

  if(m_dewarpingTransform.GetInterfacePtr() == NULL)
  {
    m_ThreadEvent.SetEvent();
    EndDialog(IDCANCEL);
    return TRUE;
  }

  // Start thread to render frames
  AfxBeginThread(ThreadInit, this);

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void CDewarpDMODlg::OnPaint()
{
  CPaintDC dc(this); // device context for painting
}

void CDewarpDMODlg::OnSize(UINT nType, int cx, int cy)
{
  // Optionally change render output size to match new client area
  RECT aClientRect;
  ::GetClientRect(m_hWnd, &aClientRect);

  CDialogEx::OnSize(nType, cx, cy);
}

void CDewarpDMODlg::OnDestroy()
{
  // Tell render thread to abort
  m_bAbort = true;

  // Tell viewer to stop and release render call
  try
  {
    if (m_viewer.GetStatus() & AMV_STATUS_RUNNING)
    {
      m_viewer.Stop();
    }
  }
  catch (COleDispatchException* e)
  {
    AfxMessageBox(e->m_strDescription);
    e->Delete();
  }

  // Wait up to 500 ms for render thread to terminate
  WaitForSingleObject(m_ThreadEvent, 500);

  // Close input file
  if (m_hFile)
  {
    CloseHandle(m_hFile);
  }

  // Unregister event sink
  IUnknown* pUnkCallback = this->GetIDispatch(FALSE);
  AfxConnectionUnadvise(m_viewer, __uuidof(IAxisMediaViewerEvents),
    pUnkCallback, FALSE, m_dwCallbackCookie);

  // Release COM object
  m_viewer.ReleaseDispatch();

  CDialogEx::OnDestroy();
}

UINT CDewarpDMODlg::OnGetDlgCode()
{
  // capture arrow keys
  return DLGC_WANTALLKEYS;
}

void CDewarpDMODlg::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
  // Global commands
  switch (nChar)
  {
  case 'D':
    // Enable/disable dewarp effect
    m_dewarpingTransform->Bypass = !m_dewarpingTransform->Bypass;
    break;
  case '1':
  case VK_NUMPAD1:
    m_dewarpingTransform->ViewType = AxisVideoTransformLib::single;
    break;
  case '2':
  case VK_NUMPAD2:
    m_dewarpingTransform->ViewType = AxisVideoTransformLib::quad;
    break;
  case '3':
  case VK_NUMPAD3:
    m_dewarpingTransform->ViewType = AxisVideoTransformLib::panoramaSingle;
    break;
  case '4':
  case VK_NUMPAD4:
    m_dewarpingTransform->ViewType = AxisVideoTransformLib::panoramaDouble;
    break;
  case '5':
  case VK_NUMPAD5:
    m_dewarpingTransform->ViewType = AxisVideoTransformLib::singleOverview;
    break;
  }

  // PTZ control for single and quad view
  if(m_dewarpingTransform->ViewType == AxisVideoTransformLib::single ||
    m_dewarpingTransform->ViewType == AxisVideoTransformLib::singleOverview)
  {
    switch (nChar)
    {
    case VK_LEFT:
      m_dewarpingTransform->Pan -= 1.0;
      break;
    case VK_RIGHT:
      m_dewarpingTransform->Pan += 1.0;
      break;
    case VK_UP:
      m_dewarpingTransform->Tilt += 1.0;
      break;
    case VK_DOWN:
      m_dewarpingTransform->Tilt -= 1.0;
      break;
    case VK_ADD:
      Zoom(1.0);
      break;
    case VK_SUBTRACT:
      Zoom(-1.0);
      break;
    }
  }
  else if(m_dewarpingTransform->ViewType == AxisVideoTransformLib::quad)
  {
    // Quad view: This implementation restricts control to only pan
    switch (nChar)
    {
    case VK_LEFT:
      m_dewarpingTransform->Pan -= 1.0;
      break;
    case VK_RIGHT:
      m_dewarpingTransform->Pan += 1.0;
      break;
    }
  }
  else if(m_dewarpingTransform->ViewType == AxisVideoTransformLib::panoramaSingle ||
          m_dewarpingTransform->ViewType == AxisVideoTransformLib::panoramaDouble)
  {
    short nVirtKey = GetKeyState(VK_SHIFT);
    switch (nChar)
    {
    case VK_LEFT:
      if (nVirtKey & 0x8000)
      {
        m_dewarpingTransform->PanoramaFieldOfView -= 1.0;
      }
      else
      {
        m_dewarpingTransform->PanoramaPanOffset -= 1.0;
      }

      break;
    case VK_RIGHT:
      if (nVirtKey & 0x8000)
      {
        m_dewarpingTransform->PanoramaFieldOfView += 1.0;
      }
      else
      {
        m_dewarpingTransform->PanoramaPanOffset += 1.0;
      }

      break;
    }
  }
}

void CDewarpDMODlg::OnLButtonDown(UINT nFlags, CPoint point)
{
  if(!CanPointMap()) return;

  RECT aClientRect;
  this->GetClientRect(&aClientRect);
  double aCalcPan, aCalcTilt;

  // Map image point to pan/tilt (use normalized coordinates)
  double aNormX = point.x / (double)aClientRect.right;
  double aNormY = point.y / (double)aClientRect.bottom;
  m_dewarpingTransform->MapPoint(aNormX, aNormY, &aCalcPan, &aCalcTilt, 0);

  m_dewarpingTransform->Pan = aCalcPan;
  m_dewarpingTransform->Tilt = aCalcTilt;
}

BOOL CDewarpDMODlg::OnMouseWheel(UINT nFlags, short zDelta, CPoint point)
{
  if(!CanPointMap()) return TRUE;

  RECT aClientRect;
  this->GetClientRect(&aClientRect);
  this->ScreenToClient(&point);

  float aZoomFactor = zDelta / 100.0f;
  double aNormX = point.x / (double)aClientRect.right;
  double aNormY = point.y / (double)aClientRect.bottom;

  // calculate pan/tilt for zoom point
  double aZoomPointPan, aZoomPointTilt;
  m_dewarpingTransform->MapPoint(aNormX, aNormY, &aZoomPointPan, &aZoomPointTilt, 0);

  if (IsOverview() && (aZoomFactor > 0))
  {
    // go from overview to dewarped view at cursor position
    Zoom(aZoomFactor);
    m_dewarpingTransform->Pan = aZoomPointPan;
    m_dewarpingTransform->Tilt = aZoomPointTilt;

    // compensate pan/tilt so that cursor stays over the desired position
    double aNewZoomPointPan, aNewZoomPointTilt;
    m_dewarpingTransform->MapPoint(aNormX, aNormY, &aNewZoomPointPan, &aNewZoomPointTilt, 0);

    m_dewarpingTransform->Pan += (aZoomPointPan - aNewZoomPointPan);
    m_dewarpingTransform->Tilt += (aZoomPointTilt - aNewZoomPointTilt);
  }
  else
  {
    // zoom in/out at current position
    Zoom(aZoomFactor);

    // compensate pan/tilt error due to zoom operation in order to keep zoom-point position
    double aNewZoomPointPan, aNewZoomPointTilt;
    m_dewarpingTransform->MapPoint(aNormX, aNormY, &aNewZoomPointPan, &aNewZoomPointTilt, 0);

    m_dewarpingTransform->Pan += (aZoomPointPan - aNewZoomPointPan);
    m_dewarpingTransform->Tilt += (aZoomPointTilt - aNewZoomPointTilt);
  }

  return TRUE;
}

void CDewarpDMODlg::OnDecodedImage(UINT64 dw64StartTime, SHORT nColorSpace, VARIANT &SampleArray)
{
  // NOTE: this event is not received on the GUI thread

  HRESULT hr = S_OK;
  BYTE* aVideoSampleBuffer;
  DWORD aVideoSampleBufferLength;

  if (m_bAbort)
  {
    return;
  }

  COleSafeArray SafeArray;

  // Attach image buffer to our MFC helper class
  SafeArray.Attach(SampleArray);
  SafeArray.AccessData((void**) &aVideoSampleBuffer);
  aVideoSampleBufferLength = SafeArray.GetOneDimSize();

  if(nColorSpace == AMV_CS_RGB32 || nColorSpace == AMV_CS_RGB24)
  {
    BITMAPINFOHEADER* aHeader = (BITMAPINFOHEADER*) aVideoSampleBuffer;
    aVideoSampleBuffer += sizeof(BITMAPINFOHEADER);
    aVideoSampleBufferLength -= sizeof(BITMAPINFOHEADER);
  }


  // DEWARPING ROUTINE

  // Initially, configure input/output video format
  if(AMV_CS_NULL == m_inputColorSpace)
  {
    m_inputColorSpace = (AMV_COLOR_SPACE)nColorSpace;
    m_inputFrameSize = aVideoSampleBufferLength;
    hr = ConfigureDewarpingDMO();
    if(SUCCEEDED(hr))
    {
      SetWindowSize();
    }

  }

  // Deliver warped input frame to transform
  if(SUCCEEDED(hr))
  {
    IMediaBufferPtr anInputBuffer;
    hr = CMediaBuffer::Create(aVideoSampleBuffer, m_inputFrameSize, &anInputBuffer);
    if(SUCCEEDED(hr))
    {
      hr = m_mediaObject->ProcessInput(0, anInputBuffer, 0, 0, 0);
    }
  }

  // Perform dewarping of current input
  // Can be called multiple times to generate different views for same input frame
  if(SUCCEEDED(hr))
  {
    DWORD anOutputStatus;
    DMO_OUTPUT_DATA_BUFFER aOutputBuffer;
    aOutputBuffer.pBuffer = m_outputBuffer;
    m_outputBuffer->SetLength(0);
    hr = m_mediaObject->ProcessOutput(0, 1, &aOutputBuffer, &anOutputStatus);

    if(S_OK == hr)
    {
      hr = RenderFrame();
    }
  }

  _ASSERT(SUCCEEDED(hr));
  
  // Release buffer from our MFC helper class
  SafeArray.UnaccessData();
  SampleArray = SafeArray.Detach();
}

HRESULT CDewarpDMODlg::InitDewarpingDMO(const LensProfileData& theLensProfileData,
  const AxisVideoTransformLib::TiltOrientation theTiltOrientation)
{
  m_dewarpingTransform.CreateInstance(__uuidof(AxisVideoTransformLib::DewarpingTransform));
  m_dewarpingTransform.QueryInterface(IID_IMediaObject, &m_mediaObject);

  if(m_dewarpingTransform.GetInterfacePtr() == NULL)
  {
    return E_FAIL;
  }

  CComSafeArray<DOUBLE> aLensProfileArray;
  aLensProfileArray.Add(theLensProfileData[0]);
  aLensProfileArray.Add(theLensProfileData[1]);
  aLensProfileArray.Add(theLensProfileData[2]);

  m_dewarpingTransform->LensProfile = aLensProfileArray;
  m_dewarpingTransform->TiltOrientation = theTiltOrientation; 

  // start in overview mode
  //m_dewarpingTransform->Bypass = VARIANT_TRUE;  

  // set custom output video resolution (default is 1024x768)
  m_dewarpingTransform->OutputWidth = 1280;

  // initial view type - determines output resolution if not explicit)
  //m_dewarpingTransform->PanoramaFieldOfView = 360;
  //m_dewarpingTransform->ViewType = AxisVideoTransformLib::panoramaSingle;

  return S_OK;
}

HRESULT CDewarpDMODlg::ConfigureDewarpingDMO()
{
  HRESULT hr;
  DMO_MEDIA_TYPE anInputMediaType;
  DMO_MEDIA_TYPE anOutputMediaType;

  if(m_mediaObject.GetInterfacePtr() == NULL)
  {
    // DMO not initialized
    return E_FAIL;
  }

  // Construct input media type structure that reflect the current input video
  MoInitMediaType(&anInputMediaType, sizeof(VIDEOINFOHEADER));
  anInputMediaType.majortype = MEDIATYPE_Video;
  anInputMediaType.formattype = FORMAT_VideoInfo;

  VIDEOINFOHEADER* aVideoInfo = (VIDEOINFOHEADER*) anInputMediaType.pbFormat;

  switch (m_inputColorSpace)
  {
  case AMV_CS_YV12:
    anInputMediaType.subtype = MEDIASUBTYPE_YV12;
    aVideoInfo->bmiHeader.biBitCount = 12;
    break;
  case AMV_CS_YUY2:
    anInputMediaType.subtype = MEDIASUBTYPE_YUY2;
    aVideoInfo->bmiHeader.biBitCount = 16;
    break;
  case AMV_CS_RGB32:
    anInputMediaType.subtype = MEDIASUBTYPE_RGB32;
    aVideoInfo->bmiHeader.biBitCount = 32;
    break;
  default:
    // unsupported format
    _ASSERT(false);
    return E_NOTIMPL;
  }

  aVideoInfo->AvgTimePerFrame = 0;
  aVideoInfo->bmiHeader.biWidth = m_inputVideoWidth;
  aVideoInfo->bmiHeader.biHeight = m_inputVideoHeight;
  aVideoInfo->bmiHeader.biSizeImage = m_inputFrameSize;
  aVideoInfo->bmiHeader.biCompression = anInputMediaType.subtype.Data1;

  hr = m_mediaObject->SetInputType(0, &anInputMediaType, 0);
  
  // Enumerate the output media types and select a type that match the desired format
  if(SUCCEEDED(hr))
  {
    hr = E_FAIL;
    int aTypeIndex = 0;
    while(S_OK == m_mediaObject->GetOutputType(0, aTypeIndex++, &anOutputMediaType))
    {
      if(anOutputMediaType.subtype == OUTPUT_VIDEO_FORMAT &&
         anOutputMediaType.formattype == FORMAT_VideoInfo)
      {
        VIDEOINFOHEADER* aHeader = (VIDEOINFOHEADER*) anOutputMediaType.pbFormat;

        if(MEDIASUBTYPE_RGB32 == OUTPUT_VIDEO_FORMAT)
        {
          // force top-bottom bitmap orientation on output by setting negative height 
          aHeader->bmiHeader.biHeight = -abs(aHeader->bmiHeader.biHeight);
        }

        hr = m_mediaObject->SetOutputType(0, &anOutputMediaType, 0);
        if(SUCCEEDED(hr))
        {
          CopyMemory(&m_outputBitmapInfoHeader, &aHeader->bmiHeader, sizeof(BITMAPINFOHEADER));
          hr = S_OK;
        }

        break;
      }
    }
  }

  if(SUCCEEDED(hr))
  {
    // create output buffer according to required size
    DWORD aBufferSize, aBufferAlignment;
    hr = m_mediaObject->GetOutputSizeInfo(0, &aBufferSize, &aBufferAlignment);
    if(SUCCEEDED(hr))
    {
      hr = CMediaBuffer::Create(aBufferSize, &m_outputBuffer);
    }
  }

  MoFreeMediaType(&anInputMediaType);

  return hr;
}

HRESULT CDewarpDMODlg::RenderFrame()
{
  HRESULT hr;
  BOOL aSuccess;
  CDC* aDC;
  CDC aMemDC;
  CBitmap aBitmap;
  BYTE* anImageData;
  DWORD aDataLength;

  if(MEDIASUBTYPE_RGB32 != OUTPUT_VIDEO_FORMAT)
  {
    // Sample code does only implement RGB rendering
    _ASSERT(false);
    return S_FALSE;
  }

  hr = m_outputBuffer->GetBufferAndLength(&anImageData, &aDataLength);

  if(SUCCEEDED(hr))
  {
    // This code will render the bitmap as top-bottom orientation
    aDC = this->GetDC();

    LONG anOutputVideoWidth = m_outputBitmapInfoHeader.biWidth;
    LONG anOutputVideoHeight = abs(m_outputBitmapInfoHeader.biHeight);
    LONG anOutputVideoStride =  m_outputBitmapInfoHeader.biSizeImage / anOutputVideoHeight;
    aSuccess = aBitmap.CreateBitmap(anOutputVideoWidth, anOutputVideoHeight, 1, 32, anImageData);

    if(aSuccess)
    {
      aSuccess = aMemDC.CreateCompatibleDC(aDC);
      aMemDC.SelectObject(&aBitmap);
    }
        
    if(aSuccess)
    {
      CRect aClientRect;
      this->GetClientRect(&aClientRect);
      aDC->SetStretchBltMode(HALFTONE);
      aSuccess = aDC->StretchBlt(0, 0, aClientRect.right, aClientRect.bottom, &aMemDC,
                                 0, 0, anOutputVideoWidth, anOutputVideoHeight, SRCCOPY);

      // Show the current view region when overview circle is shown
      // Test this by go to AxisVideoTransformLib::singleOverview mode ('5' key)
      // or switch to overview mode ('D' key)
      if(IsOverview())
      {
        long letterBoxWidth = (aClientRect.Width() - aClientRect.Height()) / 2;
        CRect aCircleRect = aClientRect;
        aCircleRect.left += letterBoxWidth;
        aCircleRect.right -= letterBoxWidth;

        DrawViewRegion(aDC, aCircleRect, true);
      }
      else if(m_dewarpingTransform->ViewType == AxisVideoTransformLib::singleOverview)
      {
        // overview circle has a radius of half the image height and is placed at lower right corner 
        CRect aCircleRect;
        double outputImageAspectRatio = 
          m_dewarpingTransform->OutputWidth / (double)m_dewarpingTransform->OutputHeight;
        aCircleRect.left = (long)(aClientRect.right - aClientRect.Width() / 2 / outputImageAspectRatio);
        aCircleRect.right = aClientRect.right;
        aCircleRect.top = aClientRect.bottom - aClientRect.Height() / 2;
        aCircleRect.bottom = aClientRect.bottom;

        DrawViewRegion(aDC, aCircleRect, false);
      }
    }

    aBitmap.DeleteObject();
    aMemDC.DeleteDC();
    aDC->DeleteDC();
    this->ReleaseDC(aDC);

    hr = aSuccess ? S_OK : E_FAIL;
  }

  return hr;
}

bool CDewarpDMODlg::CanPointMap()
{
  // Point mapping is currently not supported for quad and panorama views
  return m_dewarpingTransform->ViewType == AxisVideoTransformLib::single ||
    m_dewarpingTransform->ViewType == AxisVideoTransformLib::singleOverview;
}


bool CDewarpDMODlg::IsOverview()
{
  return (m_dewarpingTransform->Bypass != VARIANT_FALSE) ? true : false;
}

void CDewarpDMODlg::Zoom(float zoomFactor)
{
  if (IsOverview() && zoomFactor > 0)
  {
    // Goto dewarped view
    m_dewarpingTransform->Bypass = false; // dewarped view
    m_dewarpingTransform->FieldOfView = 45;
  }
  else if (m_dewarpingTransform->FieldOfView - zoomFactor > 45)
  {
    // Goto overview
    m_dewarpingTransform->Bypass = true; // overview
  }
  else
  {
    m_dewarpingTransform->FieldOfView -= zoomFactor;
  }
}

void CDewarpDMODlg::DrawViewRegion(
  CDC* theDC, CRect theOverviewCircleRect, bool showPointsOutsideCircle)
{
  HRESULT hr;

  if(!CanPointMap()) return;

  HPEN aPen = ::CreatePen(BS_SOLID, (DWORD) 2, RGB(255,0,0));
  HPEN aOldPen = (HPEN) SelectObject(theDC->m_hDC, aPen);
  
  // Initialize view border coordinates
  InitViewCoordinates(0.02f);

  std::vector<POINT> polygonPoints;
  bool isPrevPoint = false;
  double x, y;
  for (UINT i = 0; i < myViewCoords.size(); i++)
  {
    hr = m_dewarpingTransform->MapPointToOverview(myViewCoords[i].x, myViewCoords[i].y, &x, &y, 0);
    // returns coordinates in range 0.0 -> 1.0 representing overview circle rectangle
    // returns S_FALSE if point is outside circle
    if(hr == S_OK || showPointsOutsideCircle && hr == S_FALSE)
    {
      // valid point
      POINT point = {
        theOverviewCircleRect.left + (long)(x * theOverviewCircleRect.Width()),
        theOverviewCircleRect.top + (long)(y * theOverviewCircleRect.Height()) };

      if(isPrevPoint)
      {
        theDC->LineTo(point);
      }
      else
      {
        theDC->MoveTo(point);
      }

      isPrevPoint = true;
    }
    else
    {
      // line disconnected
      isPrevPoint = false;
    }
  }

  // indicate the "up" orientation
  if(S_OK == m_dewarpingTransform->MapPointToOverview(0.5, 0.0, &x, &y, 0))
  {
    // valid point
    POINT point = {
      theOverviewCircleRect.left + (long)(x * theOverviewCircleRect.Width()),
      theOverviewCircleRect.top + (long)(y * theOverviewCircleRect.Height()) };
    theDC->Ellipse(point.x - 5, point.y - 5, point.x + 5, point.y + 5);
  }

  SelectObject(theDC->m_hDC, aOldPen);
}

void CDewarpDMODlg::InitViewCoordinates(float theStepSize)
{
  if(myViewCoords.size() == 0)
  {
    // create normalized view border points ({0,0} -> {1,1})
    for (float x = 0; x < 1; x += theStepSize)
    {
      POINTF point = {x, 0};
      myViewCoords.push_back(point);
    }

    for (float y = 0; y < 1; y += theStepSize)
    {
      POINTF point = {1, y};
      myViewCoords.push_back(point);
    }

    for (float x = 1; x > 0; x -= theStepSize)
    {
      POINTF point = {x, 1};
      myViewCoords.push_back(point);
    }

    for (float y = 1; y > 0; y -= theStepSize)
    {
      POINTF point = {0, y};
      myViewCoords.push_back(point);
    }

    // connecting point
    myViewCoords.push_back(myViewCoords[0]);
  }
}