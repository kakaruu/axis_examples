// DewarpDMO.h : main header file for the DewarpDMO application
//

#pragma once

#ifndef __AFXWIN_H__
#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols
#include "DewarpDMODlg.h"

struct InputMedia
{
  LPCTSTR fileName;
  const AxisVideoTransformLib::TiltOrientation tiltOrientation;
  const LensProfileData lensProfileData;
};


// CDewarpDMOApp:
// See DewarpDMO.cpp for the implementation of this class
//
class CDewarpDMOApp : public CWinApp
{
public:
  CDewarpDMOApp();

  // Overrides
public:
  virtual BOOL InitInstance();

  // Implementation

  DECLARE_MESSAGE_MAP()
};

extern CDewarpDMOApp theApp;