// DewarpDMODlg.h : header file
//
// Our viewer video dialog which displays the video media content
//

#pragma once

#include <afxmt.h>
#include <afxdialogex.h>
#include <atlsafe.h>
#include <vector>

#include "MediaBuffer.h"

#include "AxisMediaViewer.h"
#include "AxisMediaViewerEvents.h"

// Import AxisVideoTransformLib type library definitions from DLL
#import "../../Redist/AxisVideoTransform.dll"

_COM_SMARTPTR_TYPEDEF(IMediaObject, IID_IMediaObject);
_COM_SMARTPTR_TYPEDEF(IMediaBuffer, IID_IMediaBuffer);

typedef DOUBLE LensProfileData[3];

/// CDewarpDMODlg dialog
class CDewarpDMODlg : public CDialogEx
{
private:

  DECLARE_DYNAMIC(CDewarpDMODlg)

  // Our class member variables
  HICON m_hIcon;
  CAxisMediaViewer m_viewer;
  DWORD m_dwCallbackCookie;
  CEvent m_ThreadEvent;
  BOOL m_bAbort;
  HANDLE m_hFile;
  CString m_fileName;
  LONG m_mediaDataFilePosition;
  bool doRepeatPlayback;

  LONG m_inputVideoWidth;
  LONG m_inputVideoHeight;
  LONG m_inputFrameSize;
  AMV_COLOR_SPACE m_inputColorSpace;

  AxisVideoTransformLib::IDewarpingTransformPtr m_dewarpingTransform;

  /// DMO interface
  IMediaObjectPtr m_mediaObject;

  BITMAPINFOHEADER m_outputBitmapInfoHeader;
  IMediaBufferPtr m_outputBuffer;

  std::vector<POINTF> myViewCoords;

  HRESULT Setup();
  void SetWindowSize();

  HRESULT InitDewarpingDMO(const LensProfileData& theLensProfileData,
                           const AxisVideoTransformLib::TiltOrientation theTiltOrientation);
  HRESULT ConfigureDewarpingDMO();
  HRESULT RenderFrame();

  bool CanPointMap();
  bool IsOverview();
  void Zoom(float zoomFactor);

  void DrawViewRegion(CDC* theDC, CRect theOverviewCircleRect, bool showPointsOutsideCircle);
  void InitViewCoordinates(float theStepSize);

  static UINT ThreadInit(LPVOID pParam);
  UINT Thread();

  // IAxisMediaViewerEvents
  void OnDecodedImage(UINT64 dw64StartTime, SHORT nColorSpace, VARIANT &SampleArray);

public:

  CDewarpDMODlg(LPCTSTR theFileName,
    const LensProfileData& theLensProfileData,
    const AxisVideoTransformLib::TiltOrientation theTiltOrientation);
  virtual ~CDewarpDMODlg();

  // Dialog Data
  enum { IDD = IDD_VIDEO_DIALOG };

protected:

  DECLARE_MESSAGE_MAP()
  DECLARE_DISPATCH_MAP()
  DECLARE_INTERFACE_MAP()

public:

  virtual BOOL OnInitDialog();
  afx_msg void OnPaint();
  afx_msg void OnSize(UINT nType, int cx, int cy);
  afx_msg void OnDestroy();
  afx_msg UINT OnGetDlgCode();
  afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
  afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
  afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
};
