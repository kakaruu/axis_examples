﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace CustomTransform.TransformTable
{
	public class ListTableData
	{
		[JsonProperty("items")]
		public List<TableDescription> TableDescriptionList { get; set; }
	}
}
