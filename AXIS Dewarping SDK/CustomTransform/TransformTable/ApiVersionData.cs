﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace CustomTransform.TransformTable
{
	public class ApiVersionData
	{
		[JsonProperty("apiVersions")]
		public List<Version> ApiVersions { get; set; }
	}
}
