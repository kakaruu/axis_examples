﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Newtonsoft.Json;

namespace CustomTransform.TransformTable
{
	public class FileTransformTableService : ITransformTableService
	{
		private ListTableResponse listTableResponse;
		private GetTableResponse getTableResponse;

		public FileTransformTableService()
		{
			listTableResponse = JsonConvert.DeserializeObject<ListTableResponse>(
				File.ReadAllText("list_table_response.json"));
			getTableResponse = JsonConvert.DeserializeObject<GetTableResponse>(
				File.ReadAllText("get_table_response.json"));
		}

		public bool IsSupported()
		{
			return true;
		}

		public bool IsSupportedVersion(Version version)
		{
			return getTableResponse.ApiVersion == version;
		}

		public IEnumerable<TableDescription> ListTables()
		{
			return listTableResponse.Data.TableDescriptionList;
		}

		public byte[] GetTable(TableDescription tableDescription)
		{
			if (tableDescription.Id != getTableResponse.Data.Id)
			{
				throw new Exception("table not found");
			}

			return Convert.FromBase64String(getTableResponse.Data.TableDataBase64);
		}
	}
}
