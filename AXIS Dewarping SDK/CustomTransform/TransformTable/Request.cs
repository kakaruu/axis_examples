﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace CustomTransform.TransformTable
{
	public class Request
	{
		public Request(Version apiVersion, string context, string method)
		{
			ApiVersion = apiVersion.ToString(2);
			Context = context;
			Method = method;
		}

		[JsonProperty("apiVersion")]
		public string ApiVersion { get; set; }

		[JsonProperty("context")]
		public string Context { get; set; }

		[JsonProperty("method")]
		public string Method { get; set; }
	}
}
