﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomTransform.TransformTable
{
	public interface ITransformTableService
	{
		bool IsSupported();

		bool IsSupportedVersion(Version version);

		IEnumerable<TableDescription> ListTables();

		byte[] GetTable(TableDescription tableDescription);
	}
}
