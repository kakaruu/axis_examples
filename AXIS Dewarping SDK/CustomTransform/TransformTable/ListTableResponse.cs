﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace CustomTransform.TransformTable
{
	public class ListTableResponse : Response
	{
		[JsonProperty("data")]
		public ListTableData Data { get; set; }
	}
}
