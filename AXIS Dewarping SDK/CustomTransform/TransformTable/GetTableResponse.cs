﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace CustomTransform.TransformTable
{
	public class GetTableResponse : Response
	{
		[JsonProperty("data")]
		public GetTableData Data { get; set; }
	}
}
