﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace CustomTransform.TransformTable
{
	public class TableDescription
	{
		public TableDescription(string id, string description)
		{
			Id = id;
			Description = description;
		}

		[JsonProperty("id")]
		public string Id { get; private set; }

		[JsonProperty("description")]
		public string Description { get; private set; }
	}
}
