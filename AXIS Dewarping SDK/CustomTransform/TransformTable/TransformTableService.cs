﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

namespace CustomTransform.TransformTable
{
	public class TransformTableService : ITransformTableService
	{
		private readonly string paramCgiPath = "axis-cgi/view/param.cgi";
		private readonly string apiPath = "axis-cgi/transform-table.cgi";
		private readonly string identificationProperty = "Properties.TransformTable.TransformTable";
		private readonly string context = "DewarpingSDK";

		private Version apiVersion;
		private string host;
		private int port;
		private NetworkCredential credentials;

		public TransformTableService(
			Version apiVersion,
			string host,
			int port,
			string userName,
			string password)
		{
			this.apiVersion = apiVersion;
			this.host = host;
			this.port = port;
			credentials = new NetworkCredential(userName, password);
		}

		public bool IsSupported()
		{
			string result = CallGetMethod(paramCgiPath,	"action=list&group=" + identificationProperty);
			return Regex.IsMatch(result, String.Format("{0}=yes", identificationProperty));
		}

		public bool IsSupportedVersion(Version version)
		{
			Request request = new Request(apiVersion, context, "getSupportedVersions");
			string result = CallPostMethod(apiPath, JsonConvert.SerializeObject(request));
			GetSupportedVersionsResponse response =
				JsonConvert.DeserializeObject<GetSupportedVersionsResponse>(result);
			ThrowOnError(response);
			return response.Data.ApiVersions.Any((v) => v == version);
		}

		public IEnumerable<TableDescription> ListTables()
		{
			Request request = new Request(apiVersion, context, "listTables");
			string result = CallPostMethod(apiPath, JsonConvert.SerializeObject(request));
			ListTableResponse response = JsonConvert.DeserializeObject<ListTableResponse>(result);
			ThrowOnError(response);

			// Write response to file
			// File.WriteAllText("list_table_response.json", result);

			return response.Data.TableDescriptionList;
		}

		public byte[] GetTable(TableDescription tableDescription)
		{
			string tableId = tableDescription.Id;
			GetTableRequest request = new GetTableRequest(apiVersion, context, "getTable", tableId);
			string result = CallPostMethod(apiPath, JsonConvert.SerializeObject(request));
			GetTableResponse response = JsonConvert.DeserializeObject<GetTableResponse>(result);
			ThrowOnError(response);

			// Write response to file
			// File.WriteAllText("get_table_response.json", result);

			return Convert.FromBase64String(response.Data.TableDataBase64);
		}

		private void ThrowOnError(Response response)
		{
			if (response.Error != null)
			{
				throw new Exception(String.Format(
					"Device returned an error for method \"{2}\": {0} (code: {1})",
					response.Error.Message, response.Error.Code, response.Method));
			}
		}

		public string CallGetMethod(string path, string query)
		{
			HttpWebRequest httpWebRequest = CreateHttpWebRequest(path, query);
			HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();

			StreamReader streamReader = new StreamReader(response.GetResponseStream());
			return streamReader.ReadToEnd();
		}

		public string CallPostMethod(string path,	string postString)
		{
			HttpWebRequest httpWebRequest = CreateHttpWebRequest(path, "");	
			httpWebRequest.Method = "POST";

			byte[] postData = Encoding.UTF8.GetBytes(postString);
			httpWebRequest.ContentLength = postData.Length;

			Stream reqStream = httpWebRequest.GetRequestStream();
			reqStream.Write(postData, 0, postData.Length);
			reqStream.Close();

			HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();

			StreamReader streamReader = new StreamReader(response.GetResponseStream());
			return streamReader.ReadToEnd();
		}

		private HttpWebRequest CreateHttpWebRequest(string path, string query)
		{
			UriBuilder uriBuilder = new UriBuilder();
			uriBuilder.Host = host;
			uriBuilder.Scheme = "http://";
			uriBuilder.Port = port;
			uriBuilder.Path = path;
			uriBuilder.Query = query;

			HttpWebRequest webClient = (HttpWebRequest)HttpWebRequest.Create(uriBuilder.ToString());
			webClient.AllowAutoRedirect = false;
			webClient.Credentials = credentials;

			return webClient;
		}
	}
}
