﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace CustomTransform.TransformTable
{
	public class GetTableData
	{
		[JsonProperty("id")]
		public string Id { get; private set; }

		[JsonProperty("table")]
		public string TableDataBase64 { get; private set; }
	}
}
