﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace CustomTransform.TransformTable
{
	public class GetTableRequest : Request
	{
		public GetTableRequest(Version apiVersion, string context, string method, string id)
			: base(apiVersion, context, method)
		{
			Params = new GetTableRequestParams();
			Params.Id = id;
		}
		
		[JsonProperty("params")]
		public GetTableRequestParams Params { get; set; }
	}
}
