﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace CustomTransform.TransformTable
{
	public class Response
	{
		[JsonProperty("apiVersion")]
		public Version ApiVersion { get; set; }

		[JsonProperty("method")]
		public string Method { get; set; }

		[JsonProperty("error")]
		public Error Error { get; set; }
	}
}
