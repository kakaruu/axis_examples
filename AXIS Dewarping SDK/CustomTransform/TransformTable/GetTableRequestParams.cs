﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace CustomTransform.TransformTable
{
	public class GetTableRequestParams
	{
		[JsonProperty("id")]
		public string Id { get; set; }
	}
}
