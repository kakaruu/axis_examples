﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace CustomTransform.TransformTable
{
	public class GetSupportedVersionsResponse : Response
	{
		[JsonProperty("data")]
		public ApiVersionData Data { get; set; }
	}
}
