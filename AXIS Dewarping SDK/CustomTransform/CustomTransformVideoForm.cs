using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using AxisMediaViewerLib;
using AxisVideoTransformLib;
using DirectShowLib;
using DirectShowLib.DMO;
using System.Diagnostics;
using CustomTransform.TransformTable;

namespace DewarpDirectShow
{
	/// <summary>
	/// 
	/// This sample code shows how to use AXIS Dewarping Transform DMO to
	/// perform custom transforms. Typically this involves acquire transform
	/// data from the camera and apply it to the video stream. This sample use
	/// video and transform data from file.
	/// 
	/// This sample also shows how to implement digital zoom using the mouse or keyboard.
	/// 
	/// This sample uses AXIS Dewarping Transform DMO as a DirectShow filter.
	/// For more info about this look at the DewarpDirectShowCSharp sample.
	/// 
	/// </summary>
	class CustomTransformVideoForm : Form
	{
		private const int outputWidth = 1024;
		private const int outputHeight = 768;

		// Sample video clips
		private const string videoPath = "P3807.H264.4320x1920@30.bin";

		private AmpFileParser fileParser;
		private AxisMediaViewer viewer;

		private IDewarpingTransform dewarpingTransform;

		// The render thread that parses the file and displays each frame
		private Thread renderThread;

		private bool doExitRenderThread = false;

		// Use panel derivative to receive arrow key events
		private SelectablePanel videoPanel;

		private bool isMouseButtonPressed = false;
		private double viewStartPointLeft;
		private double viewStartPointTop;
		private Point mouseAnchorPoint;

		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new CustomTransformVideoForm());
		}


		public CustomTransformVideoForm()
		{
			InitializeComponent();
			fileParser = new AmpFileParser(videoPath);
		}

		private void SetupViewer(IntPtr windowHandle)
		{
			// Create the AXIS Media Viewer COM object in a MTA apartment to allow multiple
			// threads (UI and render thread) to access COM object (by default tasks use MTA threads).
			int outputWidth = videoPanel.Width;
			int outputHeight = videoPanel.Height;
			Task.Factory.StartNew(() =>
			{
				try
				{
					viewer = new AxisMediaViewer();
					viewer.EnableOnDecodedImage = false;

					// Set some media properties
					viewer.VideoRenderer = (int)AMV_RENDER_OPTIONS.AMV_VIDEO_RENDERER_EVR;
					viewer.LiveMode = false;
					viewer.H264VideoDecodingMode |= 
						(int)(AMV_DECODING_MODE.AMV_VIDEO_DECODING_QUALITY_DYNAMIC | AMV_DECODING_MODE.AMV_VIDEO_NO_FRAME_DROPPING);
#if WIN64
					viewer.H264EnableHWDecoding = true;
#endif

					// Init the Viewer with media type buffer and a win32 hWnd handle to the window. 
					viewer.Init(0, fileParser.MediaTypeBuffer, windowHandle.ToInt64());
				}
				catch (System.Exception ex)
				{
					// Install AXIS Media Parser SDK: http://www.axis.com/partner_pages/media_parser.php
					MessageBox.Show("Failed to initialize AXIS Media Viewer:\n" + ex.Message);
					return;
				}

				// Make Media Viewer to inject AXIS Dewarping Transform filter in media graph
				try
				{
					InitDewarpingTransform(outputWidth, outputHeight);
					viewer.AddFilter(dewarpingTransform, "Dewarping", 0);
				}
				catch (System.Exception ex)
				{
					MessageBox.Show("Failed to add AXIS Dewarping Transform (AxisVideoTransform.dll):\n" +
						ex.Message);
					return;
				}

				// Inject Accelerator Filter
				// - Parallelize H.264 decoding and dewarping work on different threads/cores
				// - Compensate for missing frames to avoid stuttering
				try
				{
					AcceleratorFilter acceleratorFilter;
					dewarpingTransform.InjectAcceleratorFilter(out acceleratorFilter);
				}
				catch (System.Exception ex)
				{
					MessageBox.Show("Failed to inject Accelerator Filter:\n" + ex.Message);
				}

				viewer.SetVideoPosition(0, 0, videoPanel.Width, videoPanel.Height);

			}).Wait();
		}

		private void InitDewarpingTransform(int outputWidth, int outputHeight)
		{
			// Use Microsofts DMO wrapper filter to utilize dewarping DMO in DirectShow
			IDMOWrapperFilter wrapperFilter = new DMOWrapperFilter() as IDMOWrapperFilter;
			if (wrapperFilter == null)
			{
				throw new Exception("Could not load DMO wrapper");
			}

			int hr = wrapperFilter.Init(typeof(DewarpingTransformClass).GUID, DMOCategory.VideoEffect);
			DsError.ThrowExceptionForHR(hr);

			// Get the dewarping control interface
			dewarpingTransform = (IDewarpingTransform)wrapperFilter;

			dewarpingTransform.ViewType = (int)ViewType.custom;

			// Set output video resolution to window height (see CustomTransformVideoForm_Load())
			dewarpingTransform.OutputWidth = outputWidth;
			dewarpingTransform.OutputHeight = outputHeight;

			// for smooth digital zoom support on low FPS streams
			dewarpingTransform.OutputFrameRate = 30;

			// Get custom transform data
			byte[] transformData = null;
			try
			{
				transformData = GetTransformData();
			}
			catch (System.Exception ex)
			{
				MessageBox.Show("Failed to retrieve transform data.\n" + ex.Message);
				return;
			}

			if (transformData == null)
			{
				MessageBox.Show("No transform table available for the current device configuration. " +
					"Please check the available capture modes on the device.");
				return;	
			}

			// Set custom transform data
			try
			{
				dewarpingTransform.SetCustomTransformData(transformData, 0);
			}
			catch (System.ArgumentException)
			{
				MessageBox.Show("Invalid transform data");
			}
		}

		private byte[] GetTransformData()
		{
			// the desired transform table index (typically implemented as combo-box selection)
			int tableIndex = 0;
			
			// load transform from device
			//ITransformTableService transformTableService =
			//	new TransformTableService(new Version(1, 0), "172.25.127.89", 80, "root", "pass");

			// load transform from file
			ITransformTableService transformTableService = new FileTransformTableService();

			if (!transformTableService.IsSupported())
			{
				throw new Exception("Transform Table API is not supported by device");
			}

			if (!transformTableService.IsSupportedVersion(new Version(1, 0)))
			{
				throw new Exception("Unsupported API version");
			}

			var tableDescriptionList = transformTableService.ListTables();
			if (tableDescriptionList.Count() == 0)
			{
				return null;
			}
			else if (tableIndex >= tableDescriptionList.Count())
			{
				throw new Exception(
					"No transform data available for the current table index: " +
					tableIndex);
			}

			TableDescription tableDescription = tableDescriptionList.ToArray()[tableIndex];
			System.Diagnostics.Trace.WriteLine(String.Format("Loading transform table: {0} (id: {1})",
				tableDescription.Description, tableDescription.Id));

			return transformTableService.GetTable(tableDescription);				
		}

		private void DestroyViewer()
		{
			if (viewer != null)
			{
				if (viewer.Status == (int)AMV_STATUS.AMV_STATUS_RUNNING)
				{
					viewer.Stop();
				}

				// Inform the GC that COM object no longer will be used
				Marshal.FinalReleaseComObject(viewer);
				viewer = null;
			}
		}

		private void Play()
		{
			viewer.Start();
			StartRenderThread();
		}

		private void StartRenderThread()
		{
			if (renderThread == null)
			{
				// Create the render thread that will read file and render frames
				renderThread = new Thread(new ThreadStart(RenderThread));
				renderThread.Start();
			}
		}

		private void StopRenderThread()
		{
			if (renderThread != null)
			{
				doExitRenderThread = true;
				renderThread.Join();
				doExitRenderThread = false;
				renderThread = null;
			}
		}

		/// <summary>
		/// A thread that reads a binary media file and renders the frames in the viewer object.
		/// </summary>
		private void RenderThread()
		{
			try
			{
				// While there are frames in the in file, read each frame and let the viewer render it
				while (!doExitRenderThread)
				{
					AmpFileSample sample = fileParser.ReadSample();
					if (sample == null)
					{
						// end-of-file reached - wait for all samples to render before stopping viewer
						if (viewer.Flush())
						{
							viewer.Stop();
							long pos = 0;
							fileParser.Seek(ref pos);
							viewer.Start();
							continue;
						}
					}
					else
					{
						switch (sample.SampleType)
						{
							case (int)AMV_VIDEO_SAMPLE_TYPE.AMV_VST_MPEG4_VIDEO_CONFIG:
							case (int)AMV_VIDEO_SAMPLE_TYPE.AMV_VST_MPEG4_VIDEO_IVOP:
							case (int)AMV_VIDEO_SAMPLE_TYPE.AMV_VST_MPEG4_VIDEO_PVOP:
							case (int)AMV_VIDEO_SAMPLE_TYPE.AMV_VST_MPEG4_VIDEO_BVOP:
							case (int)AMV_VIDEO_SAMPLE_TYPE.AMV_VST_MPEG4_VIDEO_FRAGMENT:
							case (int)AMV_VIDEO_SAMPLE_TYPE.AMV_VST_MJPEG_VIDEO:
							case (int)AMV_VIDEO_SAMPLE_TYPE.AMV_VST_H264_VIDEO_CONFIG:
							case (int)AMV_VIDEO_SAMPLE_TYPE.AMV_VST_H264_VIDEO_IDR:
							case (int)AMV_VIDEO_SAMPLE_TYPE.AMV_VST_H264_VIDEO_NON_IDR:
							case (int)AMV_VIDEO_SAMPLE_TYPE.AMV_VST_H264_VIDEO_SEI:
								if ((viewer.PlayOptions & (int)AMV_PLAY_OPTIONS.AMV_PO_VIDEO) > 0)
								{
									if (viewer.PlaybackRate >= 32.0 &&
										((int)AMV_SAMPLE.AMV_S_SYNCPOINT & sample.SampleFlags) == 0)
									{
										// skip non-sync point video if playback rate is x32 or higher
										continue;
									}

									viewer.RenderVideoSample(sample.SampleFlags, sample.StartTime,
										sample.StopTime, sample.Buffer);
								}
								break;
							case (int)AMV_VIDEO_SAMPLE_TYPE.AMV_VST_MPEG4_AUDIO:
								if ((viewer.PlayOptions & (int)AMV_PLAY_OPTIONS.AMV_PO_AUDIO) > 0)
								{
									viewer.RenderAudioSample(sample.SampleFlags, sample.StartTime,
										sample.StopTime, sample.Buffer);
								}
								break;
							default:
								throw new Exception("Unknown sample type");
						}
					}
				}
			}
			catch (COMException e)
			{
				MessageBox.Show(string.Format("Exception for {0}, {1}", videoPath, e.Message));
			}
		}

		/// <summary>
		/// This event handler is called after the dialog is shown, since we need the
		/// win32 window handle as a parameter to the Viewer
		/// </summary>
		private void CustomTransformVideoForm_Load(object sender, EventArgs e)
		{
			int xDiff = Width - videoPanel.Width;
			int yDiff = Height - videoPanel.Height;
			this.Width = outputWidth + xDiff;
			this.Height = outputHeight + yDiff;

			this.SetupViewer(this.videoPanel.Handle);

			// Auto start
			Play();
		}

		private void CustomTransformVideoForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			StopRenderThread();
			DestroyViewer();
			fileParser.Close();
		}

		/// <summary>
		/// Event called when the dialog is resized, the viewer is informed about the new size
		/// </summary>
		private void CustomTransformVideoForm_Resize(object sender, EventArgs e)
		{
			if (viewer != null)
			{
				viewer.SetVideoPosition(0, 0, videoPanel.Width, videoPanel.Height);
			}
		}		

		private void videoPanel_Paint(object sender, PaintEventArgs e)
		{
			if (viewer != null)
			{
				viewer.RepaintVideo();
			}
		}

		private void videoPanel_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
		{
			// Global commands
			switch (e.KeyCode)
			{
				case Keys.D:
					dewarpingTransform.EffectLevel = dewarpingTransform.EffectLevel == 0.0 ? 1.0 : 0.0;
					break;
				case Keys.Left:
					dewarpingTransform.CustomTransformViewRect.OffsetRect(-0.01, 0.0);
					break;
				case Keys.Right:
					dewarpingTransform.CustomTransformViewRect.OffsetRect(0.01, 0.0);
					break;
				case Keys.Up:
					dewarpingTransform.CustomTransformViewRect.OffsetRect(0.0, -0.01);
					break;
				case Keys.Down:
					dewarpingTransform.CustomTransformViewRect.OffsetRect(0.0, 0.01);
					break;
				case Keys.Add:
					dewarpingTransform.CustomTransformViewRect.DeflateRect(0.01, 0.01);
					break;
				case Keys.Subtract:
					dewarpingTransform.CustomTransformViewRect.InflateRect(0.01, 0.01);
					break;
			}
		}
		
		private void videoPanel_MouseDown(object sender, MouseEventArgs e)
		{
			isMouseButtonPressed = true;
			mouseAnchorPoint.X = e.X;
			mouseAnchorPoint.Y = e.Y;
			viewStartPointLeft = dewarpingTransform.CustomTransformViewRect.Left;
			viewStartPointTop = dewarpingTransform.CustomTransformViewRect.Top;
		}

		private void videoPanel_MouseMove(object sender, MouseEventArgs e)
		{
			if (!isMouseButtonPressed) return;

			double relMouseX = (mouseAnchorPoint.X - e.X) / (double)videoPanel.Width;
			double relMouseY = (mouseAnchorPoint.Y - e.Y) / (double)videoPanel.Height;

			IRectangle view = dewarpingTransform.CustomTransformViewRect;
			view.OffsetRect(
				viewStartPointLeft + relMouseX * view.Width - view.Left,
				viewStartPointTop + relMouseY * view.Height - view.Top);
		}

		private void videoPanel_MouseUp(object sender, MouseEventArgs e)
		{
			if (isMouseButtonPressed)
			{
				if (mouseAnchorPoint == e.Location)
				{
					// click - center view
					double centerOffsetX = (e.X - videoPanel.Width / 2) / (double)videoPanel.Width;
					double centerOffsetY = (e.Y - videoPanel.Height / 2) / (double)videoPanel.Height;
					IRectangle view = dewarpingTransform.CustomTransformViewRect;
					view.OffsetRect(centerOffsetX * view.Width, centerOffsetY * view.Height);
				}

				isMouseButtonPressed = false;
			}
		}

		/// <summary>
		/// Perform zoom operation when user scrolls the mouse wheel over image
		/// </summary>
		void videoPanel_MouseWheel(object sender, MouseEventArgs e)
		{
			double zoomFactor = e.Delta / 120.0;
			zoomFactor /= 10.0; // 10 steps

			IRectangle view = dewarpingTransform.CustomTransformViewRect;
			double zoomPointX = (e.X / (double)videoPanel.Width) * view.Width + view.Left;
			double zoomPointY = (e.Y / (double)videoPanel.Height) * view.Height + view.Top;

			// zoom in/out at current position
			dewarpingTransform.CustomTransformViewRect.DeflateRect(zoomFactor/2, zoomFactor/2);

			double newZoomPointX = (e.X / (double)videoPanel.Width) * view.Width + view.Left;
			double newZoomPointY = (e.Y / (double)videoPanel.Height) * view.Height + view.Top;
			
			// compensate X/Y error due to zoom operation in order to keep zoom-point position
			double errorX = newZoomPointX - zoomPointX;
			double errorY = newZoomPointY - zoomPointY;
			view.OffsetRect(-errorX, -errorY);
		}

		#region Windows Form Designer generated code

		// Required designer variable.
		private System.ComponentModel.IContainer components = null;

		// Clean up any resources being used.
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}


		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.videoPanel = new DewarpDirectShow.SelectablePanel();
			this.SuspendLayout();
			// 
			// videoPanel
			// 
			this.videoPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.videoPanel.Location = new System.Drawing.Point(0, 0);
			this.videoPanel.Name = "videoPanel";
			this.videoPanel.Size = new System.Drawing.Size(577, 243);
			this.videoPanel.TabIndex = 23;
			this.videoPanel.TabStop = true;
			this.videoPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.videoPanel_Paint);
			this.videoPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.videoPanel_MouseDown);
			this.videoPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.videoPanel_MouseMove);
			this.videoPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.videoPanel_MouseUp);
			this.videoPanel.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.videoPanel_MouseWheel);
			this.videoPanel.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.videoPanel_PreviewKeyDown);
			// 
			// CustomTransformVideoForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(577, 243);
			this.Controls.Add(this.videoPanel);
			this.Name = "CustomTransformVideoForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Custom Transform Video Form (D=ON/OFF; +/-=Zoom; Arrows=move, or use mouse to per" +
    "form digital zoom)";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CustomTransformVideoForm_FormClosing);
			this.Load += new System.EventHandler(this.CustomTransformVideoForm_Load);
			this.Resize += new System.EventHandler(this.CustomTransformVideoForm_Resize);
			this.ResumeLayout(false);

		}

		#endregion


	}
}